﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.Pages
{
    public class ContactAndOrganizationManagementPage : PageController
    {
        private const string ID_DASHBOARD_TEMPLATE = "personalPageTemplate";

        public override void Navigate()
        {
            driver.FindElement(By.LinkText("Admin")).Click();
            driver.FindElement(By.PartialLinkText("Contacts")).Click();
            driver.FindElement(By.LinkText("Contact and Organization Management")).Click();
            driver.WaitUntilPageLoaded();
        }

        public string GetDashboardTemplate()
        {
            SelectElement dashboardTemplateDropDown = new SelectElement(driver.FindElement(By.Id(ID_DASHBOARD_TEMPLATE)));
            IWebElement selectedOption = dashboardTemplateDropDown.SelectedOption;
            return selectedOption.Text;
        }
    }
}
