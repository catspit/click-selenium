﻿using ClickPortal;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ClickPortal.Pages;
using ClickPortal.IACUC.ResearchTeam.Pages;
using ClickPortal.PortalDriver;
using Newtonsoft.Json;

namespace ClickPortal.IACUC.ResearchTeam
{
    public class IACUCResearchTeam : ClickEType
    {
        protected static int created = 0;

        [ETypeProperty("ID")]
        public string ID;
        [ETypeProperty("name")]
        public string Name;
        [ETypeProperty("company", RelatedEType = "Company")]
        public string Company; 
        [ETypeProperty("createdBy", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string CreatorUserID;       
        [ETypeProperty("owner", RelatedEType = "Person", RelatedKeyName = "userId")]
        [ETypeProperty("customAttributes.principalInvestigator.customAttributes.studyTeamMember", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string Owner;

        public string ResourceContainerTemplateID = "TMPL8D094BCCE5216EE";
        public string ResourceContainerParent = "CLICK_IACUC_HOME";
        public string CreateView = "VIEW8D09422F110CAEE";

        private ResearchTeamWorkspace workspace;
        protected int researchTeamNumber;

        public IACUCResearchTeam() : base("_ClickResearchTeam")
        {
            this.researchTeamNumber = ++created;
            this.ID = "Test-" + this.researchTeamNumber;
            this.Name = "Test IACUC Research Team " + researchTeamNumber;
            this.Company = "Test Company";
            this.Owner = "administrator";
            this.CreatorUserID = "administrator";
        }

        public IACUCResearchTeam(IACUCResearchTeam source) : this()
        {
            this.OID = source.OID;
            this.ID = source.ID;
            this.Name = source.Name;
            this.Owner = source.Owner;
            this.Workspace = new ResearchTeamWorkspace(source.Workspace.OID);
            this.CreatorUserID = source.CreatorUserID;
        }

        public IACUCResearchTeam(String name) : this()
        {
            this.Name = name;
        }

        public ResearchTeamWorkspace Workspace
        {
            get
            {
                if (workspace == null)
                {
                    //FindWorkspaceFromProtocolList(protocolList);
                }
                return workspace;
            }
            set
            {
                this.workspace = value;
            }
        }

        public override string CreateScript
        {
            get
            {
                string script = @"var etype = wom.createTransientEntity(""_ClickResearchTeam"");
etype.registerEntity();
etype.initialize();";
                string propertiesThatMustBeAddedAtTheEnd = "";
                List<string> optionalParams = new List<string>();

                int relatedCount = 0;
                foreach (FieldInfo field in this.GetType().GetFields())
                {
                    foreach (object attr in field.GetCustomAttributes(true))
                    {
                        if (attr is ETypeProperty && field.GetValue(this) != null)
                        {
                            if (((ETypeProperty)attr).RelatedEType != null)
                            {
                                string fieldname = ((ETypeProperty)attr).RelatedKeyName == null ? "ID" : ((ETypeProperty)attr).RelatedKeyName;
                                switch (((ETypeProperty)attr).PropertyName)
                                {
                                    case "owner":
                                        script += "\nvar entOwnerPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entOwnerPerson == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "company":
                                        script += "\nvar entCompany = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entCompany == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "createdBy":
                                        script += "\nvar entCreatedByPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entCreatedByPerson == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    
                                    default:
                                        optionalParams.Add("\nvar related" + ++relatedCount + " = getResultSet(\""
                                    + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                    + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""");
if (related" + relatedCount + @".count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }" + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + "related" + relatedCount + ".elements()(1)" + ");");
                                        break;
                                }
                            }
                            else
                            {
                                switch (((ETypeProperty)attr).PropertyName)
                                {
                                    case "ID":
                                        propertiesThatMustBeAddedAtTheEnd += "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");";
                                        break;

                                    default:
                                        optionalParams.Add("\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");");
                                        break;
                                }
                            }
                        }
                    }
                }

                script += "\netype.setRequiredFields(entOwnerPerson, entCompany, entCreatedByPerson);";
                script += "\nvar STInfo = ApplicationEntity.getTypeNamed(\"_StudyTeamMemberInfo\");\nvar STInfoSet = STInfo.createEntitySet();\netype.setQualifiedAttribute(\"customAttributes.teamMembers\", STInfoSet);";
                
                foreach (string snip in optionalParams)
                {
                    script += snip;
                }
                script += @"
var rc = ResourceContainer.createFromResource(etype, getElements(""ContainerTemplateForID"", ""ID"", """ + ResourceContainerTemplateID + @""")(1));
rc.setQualifiedAttribute(""parent"", getElements(""ContainerBaseForID"", ""ID"", """ + ResourceContainerParent + @""")(1));
etype.setQualifiedAttribute(""resourceContainer"", rc);
var sch = ShadowSCH.getSCH();
var view = wom.getEntityFromString(""com.webridge.entity.Entity[OID[3A2CF4C1A9565E4E8CCDCBCFE4110B89]]"");
wom.putContext(""currentView"", view, true);
ProjectStateTransition.processProject(sch, etype, etype, null);
" + propertiesThatMustBeAddedAtTheEnd + @"
?etype;";
                return script;
            }
        }

        public override void CreateEntity(PortalDriver.PortalDriver Driver)
        {
            base.CreateEntity(Driver);
            FindWorkspace(Driver);
        }

        public void FindWorkspace(PortalDriver.PortalDriver Driver)
        {
            string getWorkspaceOidScript = @"?wom.getEntityFromString(""" + this.OID + @""").getQualifiedAttribute(""resourceContainer"");";
            string workspaceOid = Driver.AdminDriver.Console().Execute(getWorkspaceOidScript);
            this.Workspace = new ResearchTeamWorkspace(workspaceOid);
        }

        public new String OID
        {
            get
            {
                if (base.OID == default(String))
                {
                    this.Workspace.Navigate();
                    base.OID = this.Workspace.projectOid;
                }
                return base.OID;
            }
            set
            {
                base.OID = value;
            }
        }
    }
}
