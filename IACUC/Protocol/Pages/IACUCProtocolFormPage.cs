﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using ClickPortal;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class IACUCProtocolFormPage : PageController
    {
        protected IACUCProtocol _protocol;

        public IACUCProtocolFormPage(IACUCProtocol protocol)
        {
            _protocol = protocol;
        }

        public bool isEditable()
        {
            return driver.FindElements(By.Id("InkSaveProjectEditor")).Count > 0;
        }

        public IWebElement SaveLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Save"));
            }
        }

        public IWebElement ExitLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Exit"));
            }
        }
    }
}
