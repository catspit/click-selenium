﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.SAFETY.Protocol.Pages
{
    public class SAFETYProtocolBiosafetySummary : SAFETYProtocolFormPage
    {
        public SAFETYProtocolBiosafetySummary(SAFETYProtocol protocol)
            : base(protocol)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[9BA536552826B24A8EBCE8D9631143C0]]");
        }
    }
}
