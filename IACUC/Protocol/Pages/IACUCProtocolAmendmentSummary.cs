﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class IACUCProtocolAmendmentSummary : IACUCProtocolFormPage
    {
        public IACUCProtocolAmendmentSummary(IACUCProtocol protocol) : base(protocol)
        {

        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[9044B1F5DD68904DA1A8F354092EA281]]");
        }
    }
}
