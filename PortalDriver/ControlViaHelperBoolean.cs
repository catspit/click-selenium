﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver {
    class ControlViaHelperBoolean : IControlFactoryFromHelper {

        public IPortalControl create(ControlHelper Helper, IPortalDriver driver)
        {
            return !Helper.type.Contains("Boolean") ? null : new ControlBoolean(Helper);
        }


        class ControlBoolean : ControlFromHelper
        {
            private Boolean truthyFalsy(string testMe)
            {
                if (testMe == null) return false;
                if (testMe == "") return false;
                testMe = testMe.Trim().ToUpper();
                if (testMe == "TRUE" || testMe == "YES") return true;
                if (testMe == "FALSE" || testMe == "NO") return false;
                throw new ArgumentException("Could not evaluate the truthy/falsy of" + testMe);
            }

            public ControlBoolean(ControlHelper Helper) : base(Helper)
            {
            }

            public override bool Is(IPortalControlValue value)
            {
                if (value.OID != null) return false;
                return truthyFalsy(Helper.text) == truthyFalsy(value.Text);
            }

            public override bool Is(string textOrOID)
            {
                if (string.IsNullOrEmpty(textOrOID)) return this.IsClear;
                return truthyFalsy(textOrOID) == truthyFalsy(this.Helper.OID);
            }

            public override IPortalControlValue Val()
            {
                return new PortalControlValue(this.OID, this.Text, this.IsClear);
            }

            public override IPortalControlValue Val(string TextOrOID)
            {
                if (string.IsNullOrEmpty(TextOrOID)) { this.Clear(); } 
                else { setControlBoolean(this.truthyFalsy(TextOrOID)); }

                return null;
            }

            public override IPortalControlValue Clear()
            {
                if (this.IsClear) return null;
                if (this.Helper.type == "BooleanCheckbox") this.setControlBoolean(false);
                if (this.Helper.type == "BooleanRadio")
                {
                    IWebElement elem = this.Helper.widgets.Last();
                    string elemID = elem.GetAttribute("id");
                    IWebElement label = elem.FindElement(By.XPath("following-sibling::label[@for='" + elemID + "']"));
                    if (label != null)
                    {
                        label.Click();
                    }
                    else
                    {
                        elem.Click();
                    }
                }

                if (this.Helper.type == "BooleanSelect") new SelectElement(this.Helper.widgets.Last()).SelectByIndex(0);
                return null;
            }

            private void setControlBoolean(Boolean trueFalse)
            {
                var currentValue = this.Is("TRUE");

                if (this.Helper.type == "BooleanCheckbox" && trueFalse != currentValue)
                {
                    IWebElement elem = this.Helper.widgets[0];
                    string elemID = elem.GetAttribute("id");
                    IWebElement label = elem.FindElement(By.XPath("following-sibling::label[@for='" + elemID + "']"));
                    if (label != null)
                    {
                        label.Click();
                    }
                    else
                    {
                        elem.Click();
                    }
                    return;
                }

                if (trueFalse == currentValue && !this.IsClear) return;

                if (this.Helper.type == "BooleanSelect")
                {
                    var selector = new SelectElement(this.Helper.widgets.Last());
                    var selectIndex = 0;
                    for (; selectIndex<this.Helper.rangeOID.Count; selectIndex++)
                    {
                        var optionValue = this.Helper.rangeOID[selectIndex];
                        if (string.IsNullOrEmpty(optionValue)) continue;
                        if (truthyFalsy(optionValue) == trueFalse) break;
                    }
                    selector.SelectByIndex(selectIndex);
                    return;
                }

                if (this.Helper.type == "BooleanRadio")
                {
                    var selectIndex = 0;
                    for (; selectIndex < this.Helper.rangeOID.Count; selectIndex++) {
                        var optionValue = this.Helper.rangeOID[selectIndex];
                        if (optionValue == null) continue;
                        if (truthyFalsy(optionValue) == trueFalse) break;
                    }
                    IWebElement elem = this.Helper.widgets[selectIndex];
                    string elemID = elem.GetAttribute("id");
                    IWebElement label = elem.FindElement(By.XPath("following-sibling::label[@for='" + elemID + "']"));
                    if (label != null)
                    {
                        label.Click();
                    }
                    else
                    {
                        elem.Click();
                    }
                    return;
                }
            }
            

            public override IPortalControlValue Val(DateTime date)
            {
                throw new NotSupportedException("Date cannot be set to boolean Control");
            }


            public override IWebElement AddButton
            {
                get { throw new NotImplementedException(); }
            }
            public override IWebElement SelectButton
            {
                get { throw new NotImplementedException(); }
            }
        }
    }
}