﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using ClickPortal;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class IACUCProtocolBreeding : IACUCProtocolFormPage
    {
        public IACUCProtocolBreeding(IACUCProtocol protocol)
            : base(protocol)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[1A1CCE8D244CFE4B8ECCFAE3D526D960]]");
        }
    }
}
