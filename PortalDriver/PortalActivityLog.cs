﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver
{
    public class PortalActivityLog : IPortalActivityLog
    {
        private IPortalDriver _driver;

        private ICollection<PortalActivityLogItem> _historyLogItems;

        private static readonly string ACTIVITY_NAME_XPATH = ".//td[@class='List'][@data-drsv-column='1']//a";

        private static readonly string ACTIVITY_AUTHOR_XPATH = ".//td[@class='List'][@data-drsv-column='2']//a";

        private static readonly string ACTIVITY_DATETIME_XPATH = ".//td[@class='List'][@data-drsv-column='3']//span";
        
        public PortalActivityLog(IPortalDriver driver)
        {
            _driver = driver;
            Init();
        }

        protected void Init()
        {
            IWebElement historyTab = _driver.FindElement(By.XPath("//a[contains(text(),'History')]"));

            if (historyTab.GetAttribute("class").Equals("TabOff"))
            {
                historyTab.Click();
            }

            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementExists(By.XPath("//table[@class='TabComponentBodyTable']//tr[@data-drsv-row='0']")));

            IWebElement historyArea = _driver.FindElement(By.ClassName("TabComponentBodyTable"));
            IEnumerable<IWebElement> historyLogEntries = historyArea.FindElements(By.XPath("//tr[@data-drsv-row]"));

            _historyLogItems = new List<PortalActivityLogItem>();

            foreach (var historyLogEntry in historyLogEntries)
            {
                string name = historyLogEntry.FindElement(By.XPath(ACTIVITY_NAME_XPATH)).Text;
                string author = historyLogEntry.FindElement(By.XPath(ACTIVITY_AUTHOR_XPATH)).Text;
                string dateTimeText = historyLogEntry.FindElement(By.XPath(ACTIVITY_DATETIME_XPATH)).Text;

                _historyLogItems.Add(new PortalActivityLogItem(name, author, DateTime.Parse(dateTimeText)));
            }
        }

        public bool HasActivity(string activityName)
        {
            return _historyLogItems.Any(i => i.ActivityName.Contains(activityName));
        }

        public class PortalActivityLogItem
        {
            public string ActivityName { get; private set; }

            public string AuthorName { get; private set; }

            public DateTime ActivityDate { get; private set; }

            public PortalActivityLogItem(string activityName, string authorName, DateTime activityDate)
            {
                ActivityName = activityName;
                AuthorName = authorName;
                ActivityDate = activityDate;
            }
        }
    }
}
