﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Newtonsoft.Json.Linq;

namespace ClickPortal.PortalDriver {

    internal class PortalJsonDriverConfiguration
    {

        private JObject configJObject;

        public PortalJsonDriverConfiguration(string json) {
            this.configJObject = JObject.Parse(json);
        }

        internal StoreConfig Store()
        {
            var stores = (JObject)this.configJObject["stores"];
            foreach (var item in stores) return Store(item.Key);
            return null;
        }


        internal StoreConfig Store(string storeName)
        {
            var storeObj = (JObject) this.configJObject["stores"][storeName];
            return storeObj == null ? null : new StoreConfig(storeObj);
        }


        internal class StoreConfig
        {

            private JToken storeObj;

            public StoreConfig(JToken storeObj)
            {
                this.storeObj = storeObj;
            }


            public string URL
            {
                get { return this.storeObj["url"].ToString(); }
            }

            public string ADMIN_USER
            {
                get { return this.storeObj["admin_user"].ToString(); }
            }

            public string ADMIN_PASS
            {
                get { return this.storeObj["admin_pass"].ToString(); }
            }

            public string Pass(string user)
            {
                return this.storeObj["users"][user].ToString();
            }


            public string USER1
            {
                get
                {
                    foreach (var item in ((JObject) this.storeObj["users"])) return item.Key;
                    return null;
                }
            }

            public bool useLoginLogin
            {
                get { return storeObj["use_login_login"] != null && (bool)storeObj["use_login_login"]; }
            }

            public string LOGIN_PATH
            {
                get { return this.storeObj["login_path"] != null ? this.storeObj["login_path"].ToString() : null; }
            }

            public string LOGOFF_PATH
            {
                get { return this.storeObj["logoff_path"] != null ? this.storeObj["logoff_path"].ToString() : null;  }
            }
        }
    }




}


