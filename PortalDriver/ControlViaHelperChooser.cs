﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace ClickPortal.PortalDriver {
    internal class ControlViaHelperChooser : IControlFactoryFromHelper {

        public IPortalControl create(ControlHelper helper, IPortalDriver driver) {

            if (helper.type == "Chooser") return new ControlChooser(helper, driver);
            return null;
        }

        internal class ControlChooser : ControlFromHelper {

            internal ControlChooser(ControlHelper helper, IPortalDriver driver):base(helper, driver) {
            }

            public override bool Is(IPortalControlValue value) {
                if (!string.IsNullOrEmpty(value.OID)) return false;
                return Is(value.Text);
            }

            public override bool Is(string textOrOID) {
                var text = textOrOID ?? "";
                if (text == "" && Helper.text == "") return true;
                return Helper.text.Trim().Contains(text.Trim());
            }

            public override bool HasHelpBubble
            {
                get
                {
                    return false;
                }
            }

            public override IPortalControlValue Clear()
            {
                if (!this.IsClear) Helper.widgets[1].Click(); 
                return null;
            }

            public override IPortalControlValue Val() {
                if (this.IsClear) new PortalControlValue(null, "", true);
                return new PortalControlValue(Helper.OID, Helper.text.Trim(), false);
            }

            public override IPortalControlValue Val(string TextOrOID) {

                if (String.IsNullOrEmpty(TextOrOID))
                {
                    this.Clear();
                    return null;
                }

                if (Helper.widgets[0].GetAttribute("type") != "button")
                {
                    Helper.widgets[0].SendKeys(TextOrOID);
                    var firstChoice = By.CssSelector("tr.resultItem:nth-of-type(1)");
                    new WebDriverWait(Driver, TimeSpan.FromSeconds(20)).Until(drv => drv.FindElement(firstChoice)).Click();
                }
                else
                {
                    Driver.Window().New(Helper.widgets[0].Click);
                    // Check to see if the desired item is already displayed
                    try
                    {
                        Driver.FindElement(By.XPath("//div[@id='_webrRSV_DIV_0']//tbody/tr[td[2][contains(text(),'" + TextOrOID + "')]]/td[1]/input[@type='radio']")).Click();
                    }
                    catch (NoSuchElementException)
                    {
                        Driver.FindElement(By.Id("_webrRSV_FilterValue_0_0")).SendKeys(TextOrOID);
                        Driver.FindElement(By.XPath("//input[@type='button' and @value='Go']")).Click();
                    }
                    finally
                    {
                        new WebDriverWait(Driver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementExists(By.XPath("//div[@id='_webrRSV_DIV_0']//tbody/tr[td[2][contains(text(),'" + TextOrOID + "')]]/td[1]/input[@type='radio']"))).Click();
                        Thread.Sleep(500);
                    }

                    Driver.Window().Close(Driver.FindElement(By.Id("btnOk")).Click);
                }

                return null;
            }

            public override IPortalControlValue Val(DateTime date)
            {
                this.Val(date.ToString());
                return null;
            }

            public override IWebElement AddButton
            {
                get { throw new NotImplementedException(); }
            }
            public override IWebElement SelectButton
            {
                get {
                    var widgets = Helper.widgets;
                    if (widgets != null)
                    {
                        for (var i = 0; i < widgets.Count; i++)
                        {
                            var elem = widgets[i];
                            var elemId = elem.GetAttribute("id");
                            if (elemId != null && elemId.EndsWith("_selectBtn"))
                            {
                                return elem;
                            }
                        }
                    }
                    return null;
                }
            }
        }
    }
}
