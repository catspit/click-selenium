﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClickPortal.Pages;

namespace ClickPortal.SAFETY.Protocol.Pages
{
    public class SAFETYProtocolForm : SmartFormPage
    {
        public SAFETYProtocol Protocol;

        public SAFETYProtocolBasicInformationForm BasicInformationForm
        {
            get { return new SAFETYProtocolBasicInformationForm(this.Protocol); }
        }

        public SAFETYProtocolProtocolTeamMembers ProtocolTeamMembersForm
        {
            get { return new SAFETYProtocolProtocolTeamMembers(this.Protocol); }
        }

        public SAFETYProtocolFundingSources FundingSourcesForm
        {
            get { return new SAFETYProtocolFundingSources(this.Protocol); }
        }

        public SAFETYProtocolBiosafetySummary BiosafetySummary
        {
            get { return new SAFETYProtocolBiosafetySummary(this.Protocol); }
        }

        public SAFETYProtocolSelectAgentsSelectAgentToxinsAndToxins SelectAgentsSelectAgentToxinsAndToxins
        {
            get { return new SAFETYProtocolSelectAgentsSelectAgentToxinsAndToxins(this.Protocol); }
        }

        public SAFETYProtocolBiohazards Biohazards
        {
            get { return new SAFETYProtocolBiohazards(this.Protocol); }
        }

        public SAFETYProtocolRiskGroupAndContainmentPractices RiskGroupAndContainmentPractices
        {
            get { return new SAFETYProtocolRiskGroupAndContainmentPractices(this.Protocol); }
        }

        public SAFETYProtocolDualUseResearchOfConcern DualUseResearchOfConcern
        {
            get { return new SAFETYProtocolDualUseResearchOfConcern(this.Protocol); }
        }

        public SAFETYProtocolWasteManagement WasteManagement
        {
            get { return new SAFETYProtocolWasteManagement(this.Protocol); }
        }

        public SAFETYProtocolSupportingDocuments SupportingDocuments
        {
            get { return new SAFETYProtocolSupportingDocuments(this.Protocol); }
        }

        public SAFETYProtocolForm(SAFETYProtocol protocol)
        {
            this.Protocol = protocol;
        }

        public override void FillOutUsingTestData()
        {
            throw new NotImplementedException();
        }
    }
}
