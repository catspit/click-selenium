﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using ClickPortal;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class IACUCProtocolBasicInformationForm : IACUCProtocolFormPage
    {
        public IACUCProtocolBasicInformationForm(IACUCProtocol protocol)
            : base(protocol)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[2DB6D1AF8C78A347A8DD2233E7CB51DF]]");
        }

        public void selectResearchTeam(string team)
        {
            driver.FindElement(By.XPath("//td[text()='" + team + "']/../td/input[@type='radio']")).Click();
        }

        public void fillOutTitleProtocol(string title)
        {
            driver.FindElement(By.XPath("//textArea[@name='_ClickIACUCSubmission.customAttributes.fullTitle_text']")).SendKeys(title);
        }
        public void fillOutResearchTeam(string researchTeamName)
        {
            driver.FindElement(By.XPath("//div[@id='__ClickIACUCSubmission.customAttributes.researchTeam_container']//tr/td/label[@for][text()='" + researchTeamName + "']")).Click();
        }

        public void fillOutShortTitle(string title)
        {
            driver.FindElement(By.XPath("//input[@name='_ClickIACUCSubmission.name']")).SendKeys(title);
        }

        public void fillOutSummaryOfResearch(string summary)
        {
            driver.FindElement(By.XPath("//textArea[@name='_ClickIACUCSubmission.description']")).SendKeys(summary);
        }

        public void selectIntention(string intention)
        {
            IWebElement intentionElement = driver.FindElement(By.XPath("//td/label[@for][text()='" + intention + "']"));
            (driver as IJavaScriptExecutor).ExecuteScript(string.Format("window.scrollTo(0,{0});", intentionElement.Location.Y));

            intentionElement.Click();
        }

        public void clickContinue()
        {
            driver.FindElement(By.XPath("//*[@id='continue_btn_Bottom']")).Click();
        }

        public void selectPrincipalInvestigatorByUserID(string investigatorUserID)
        {
            IWebElement selectPIButton =  driver.FindElement(By.XPath("//span[@id='__ClickIACUCSubmission.customAttributes.investigator.customAttributes.studyTeamMember_container']//button[contains(@id,'_selectBtn')]"));
            SelectPersonForm popup = new SelectPersonForm(selectPIButton);
            popup.searchAndSelectPerson(investigatorUserID, "User ID");
        }
    }
}
