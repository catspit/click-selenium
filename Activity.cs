﻿using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;

namespace ClickPortal
{
    public abstract class Activity
    {
        protected readonly IPortalDriver Driver;
        protected readonly Project Project;
        protected readonly Workspace WorkspacePage;
        public const string OK_BUTTON_ID = "okBtn";

        protected Activity(Workspace workspace)
        {
            Project = workspace.Project;
            Driver = Project.Driver;
            WorkspacePage = workspace;
        }

        public abstract string DisplayName { get; }

        public void ClickOkButton()
        {
            var okButton = Driver.FindElement(By.Id(OK_BUTTON_ID));
            okButton.Click();
            WorkspacePage.FocusRootWindow();
        }

        public abstract void RunUsingTestData();

        public virtual void OpenActivityWindow()
        {
            var link = Driver.FindElement(By.XPath("//a[text() = '" + DisplayName + "']"));
            var okButtonLocator = By.Id(OK_BUTTON_ID);
            Driver.Window().New(link.Click);
        }
    }
}
