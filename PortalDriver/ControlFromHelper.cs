﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver
{
    abstract class ControlFromHelper : IPortalControl
    {
        protected ControlHelper Helper;
        protected IPortalDriver Driver;

        protected ControlFromHelper(ControlHelper helper)
        {
            Helper = helper;
        }

        protected ControlFromHelper(ControlHelper helper, IPortalDriver driver)
        {
            Helper = helper;
            Driver = driver;
        }

        public abstract bool Is(IPortalControlValue value);

        public abstract bool Is(string textOrOID);

        public abstract IPortalControlValue Val();

        public abstract IPortalControlValue Val(string TextOrOID);

        public abstract IPortalControlValue Val(DateTime date);

        public abstract IPortalControlValue Clear();

        public abstract IWebElement AddButton { get; }

        public virtual string OID { get { return Helper.OID; } }
        public virtual string Text { get { return Helper.text; } }
        public virtual string Type { get { return Helper.type; } }
        public virtual string Caption { get { return Helper.captionText; } }
        public virtual bool HasHelpBubble { get { return false; } }
        public virtual int Count { get { return Helper.Values.Count; } }
        public virtual bool IsClear { get { return String.IsNullOrWhiteSpace(OID); } }

        public virtual IEnumerator<IPortalControlValue> GetEnumerator()
        {
            return Helper.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual IPortalControlValue this[int index]
        {
            get { return Helper.Values[index]; }
        }

        public virtual bool Is()
        {
            return Helper.container.First().Displayed;
        }


    }
}
