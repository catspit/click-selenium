﻿using System.Collections.Generic;
using ClickPortal.PortalDriver;

namespace ClickPortal
{
    public class Person : ClickEType
    {
        [ETypeProperty("ID")]
        public string ID;

        [ETypeProperty("firstName")]
        public string FirstName;

        [ETypeProperty("lastName")]
        public string LastName;

        public string UserID;

        public bool CreateAccount;

        public List<string> Roles;

        public Person() : base("Person")
        {
            ID = "Person-" + this.InstanceNumber;
            FirstName = "Test";
            LastName = "Person";
            CreateAccount = false;
            Roles = new List<string>();
            Roles.Add("Registered User");
        }

        public override string CreateScript
        {
            get
            {
                string script = base.CreateScript;

                if (CreateAccount)
                {
                    script += "Person.CreateAccount(entity, '" + UserID + "', '1234', false);";
                    for (int i = 0; i < Roles.Count; i++)
                    {
                        script += @"var role" + i + @" = ApplicationEntity.getElements(""UserRoleForID"", ""ID"", """ + Roles[i] + @""");
if (role" + i + @".count() != 1) { throw new Error(""Incorrect number of user roles found with ID '" + Roles[i] + @"'"") }
entity.setQualifiedAttribute(""roles"", role" + i + @", ""add"");";
                    }
                }

                return script;
            }
        }
    }
}
