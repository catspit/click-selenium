﻿using OpenQA.Selenium;

namespace ClickPortal.PortalDriver
{
    class PortalBy8 : PortalBy
    {
        public PortalBy8(By byStrategy) : base(byStrategy)
        {
            ;
        }

        public new static By TabTitle(string titleTextToFind)
        {
            const string template = "//nav[contains(@class,'NavigationTabControl')]//a[contains(text(),'$TITLE$')]";
            var xpath = template.Replace("$TITLE$", titleTextToFind);
            return By.XPath(xpath);
        }
    }
}
