﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Procedure.Pages
{
    public class IACUCProcedureEuthanasiaForm : IACUCProcedureFormPage
    {
        public IACUCProcedureEuthanasiaForm(IACUCProcedure procedure) : base(procedure)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _procedure.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[C33F26CCD6DC89419F52E55624A72666]]");
        }
    }
}
