﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver {
    internal class ControlViaHelperString : IControlFactoryFromHelper
    {

        public IPortalControl create(ControlHelper helper, IPortalDriver driver)
        {
            return "DatePicker Input Textarea".Contains(helper.type) ? new ControlString(helper) : null;
        }

        internal class ControlString : ControlFromHelper
        {
            internal ControlString(ControlHelper helper):base(helper)
            {
            }

            public override bool Is(IPortalControlValue value)
            {
                if (!string.IsNullOrEmpty(value.OID)) return false;
                return Is(value.Text);
            }

            public override bool Is(string textOrOID)
            {
                var text = textOrOID ?? "";
                if (text == "" && Helper.text == "") return true;
                return Helper.text.Trim().Contains(text.Trim());
            }

            public override string OID
            {
                get { return null; }
            }

            public override bool IsClear
            {
                get { return Helper.text.Trim() == ""; }
            }

            public override IEnumerator<IPortalControlValue> GetEnumerator()
            {
                return this.createValuesArray().GetEnumerator();
            }

            private ReadOnlyCollection<IPortalControlValue> createValuesArray()
            {
                return this.IsClear ? 
                    new ReadOnlyCollection<IPortalControlValue>(new IPortalControlValue[] {}) : 
                    new ReadOnlyCollection<IPortalControlValue>(new IPortalControlValue[]{new PortalControlValue(null,Helper.text.Trim(),false)});
            }

            public override IPortalControlValue this[int index]
            {
                get { return this.createValuesArray()[index]; }
            }

            public override int Count
            {
                get { return this.createValuesArray().Count; }
            }

            public override IPortalControlValue Clear()
            {
                Helper.widgets[0].SendKeys(Keys.Control + "a");
                Helper.widgets[0].SendKeys(Keys.Delete);
                return null;
            }

            public override IPortalControlValue Val()
            {
                if (this.IsClear) new PortalControlValue(null, "", true);
                return new PortalControlValue(null, Helper.text.Trim(), false);
            }

            public override IPortalControlValue Val(string TextOrOID)
            {
                this.Clear();
                Helper.widgets[0].SendKeys(TextOrOID);
                return null;
            }

            public override IPortalControlValue Val(DateTime date)
            {
                this.Clear();
                Helper.widgets[0].SendKeys(date.ToString());
                return null;
            }

            public override IWebElement AddButton
            {
                get { throw new NotImplementedException(); }
            }
            public override IWebElement SelectButton
            {
                get { throw new NotImplementedException(); }
            }
        }
    }
}
