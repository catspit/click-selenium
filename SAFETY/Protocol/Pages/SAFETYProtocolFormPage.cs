﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClickPortal;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.SAFETY.Protocol.Pages
{
    public class SAFETYProtocolFormPage : PageController
    {
        protected SAFETYProtocol _protocol;

        public SAFETYProtocolFormPage(SAFETYProtocol protocol)
        {
            _protocol = protocol;
        }

        public bool isEditable()
        {
            return driver.FindElements(By.Id("InkSaveProjectEditor")).Count > 0;
        }

        public IWebElement SaveLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Save"));
            }
        }

        public IWebElement ExitLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Exit"));
            }
        }
    }
}
