﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver
{
    public class PortalActivity : IPortalActivity
    {
        private IPortalDriver Driver;
        private const string ACTIVITY_FORM_SD = "ResourceAdministration/Activity/form";
        private const string PROJECT_VALIDATION_SD = "ResourceAdministration/Project/ValidateProject";
        public bool ValidateBeforeExecution { get; private set; }
        internal string Locator;

        public PortalActivity(IPortalDriver driver, string locator)
        {
            Driver = driver;
            Locator = locator;
        }

        private IWebElement GetActivityLinkElement()
        {
            if (Driver.Url.Contains("RMConsole"))
            {
                return GetActivityLinkElementForRMConsole();
            }

            if(Driver.Url.Contains("PersonCompanyMgmt"))
            {
                return GetActivityLinkElementForContactAndOrgManagement();
            }

            //assume workspace
            try
            {
                return Driver.FindElement(By.XPath("//a[contains(@href, 'Activity') and text()='" + Locator + "']"));
            }
            catch (NoSuchElementException)
            {
                return GetActivityLinkElementByInternalName();
            }
        }

        private IWebElement GetActivityLinkElementForRMConsole()
        {
            Driver.SwitchTo().Frame("content");
            SelectElement selectElement = new SelectElement(Driver.FindElement(By.ClassName("selectControl")));
            IWebElement activityOption = selectElement.Options.FirstOrDefault(o => o.Text.Equals(Locator));
            return activityOption;
        }

        private IWebElement GetActivityLinkElementForContactAndOrgManagement()
        {
            Driver.FindElement(By.XPath("//span[contains(text(),'Execute')]")).Click();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            var activityOption = wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText(Locator)));
            return activityOption;
        }

        private IWebElement GetActivityLinkElementByInternalName()
        {
            string script = "? ApplicationEntity.getResultSet(\"ActivityType\").query(\"id = '" + Locator + "'\").elements().item(1);";
            string output = Driver.Store().Execute(script);
            output = output.Replace("[", "%5B").Replace("]", "%5D");
            return Driver.FindElement(By.XPath("//a[contains(@href,'ActivityType=" + output + "')]"));
        }

        public void Open()
        {
            IWebElement activityLink = GetActivityLinkElement();

            // Attempt to scroll to link location, in case it's off the page.
            (Driver as IJavaScriptExecutor).ExecuteScript(string.Format("window.scrollTo(0,{0});", activityLink.Location.Y));
            Driver.Window().New(activityLink.Click);
        }

        public void Submit()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            IWebElement okBtn = wait.Until(ExpectedConditions.ElementExists(By.Id("okBtn")));

            int numWindows = Driver.WindowHandles.Count;
            String ErrorMessage = String.Empty;

            okBtn.Click();
            // Wait for the activity window to close OR for an error to be displayed
            new WebDriverWait(Driver, TimeSpan.FromSeconds(30)).Until((d) => {
                if (d.WindowHandles.Count < numWindows)
                {
                    return true;
                }
                ReadOnlyCollection<IWebElement> error = Driver.FindElements(By.XPath("//span[@class='Error' and not(@id='pageClientSideError')]"));
                if (error.Count > 0)
                {
                    ErrorMessage = error[0].Text;
                    return true;
                }

                return false;
            });

            if (!String.IsNullOrEmpty(ErrorMessage))
            {
                throw new Exception("Error submitting activity: " + ErrorMessage);
            }

            Driver.SwitchTo().Window(Driver.WindowHandles.Last());
        }

        public void Submit(string username, string password)
        {
            new WebDriverWait(Driver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementExists(By.Id("okBtn"))).Click();

            Thread.Sleep(5000); //explicit sleep because even though frame exists, objects inside haven't loaded yet.  Switching to the frame and waiting 
                                // doesn't seem to fix the objects not loading (they don't seem to load at all if switched too soon).

            Driver.SwitchTo().Frame("GB_frame_confirmLoginMsg");
            Driver.FindElement(By.Id("confirmUserId")).SendKeys(username);
            Driver.FindElement(By.Id("confirmPassword")).SendKeys(password);

            int numWindows = Driver.WindowHandles.Count;
            Driver.FindElement(By.Id("confirmLoginSubmitBtn")).Click();

            Thread.Sleep(5000); //explicit sleep because for some reason the WebDriverWait below just seems to fail instantly.  We wait for the window to close

            if (numWindows == Driver.WindowHandles.Count)
            {
                Driver.SwitchTo().ParentFrame();
            }

            new WebDriverWait(Driver, TimeSpan.FromSeconds(10)).Until((d) =>
            {
                if (numWindows > Driver.WindowHandles.Count)
                {
                    // Window has closed, we're done here. 
                    Driver.SwitchTo().Window(Driver.WindowHandles[0]);
                    return true;
                }

                try
                {
                    // Check for errors and throw an Exception if found.
                    ReadOnlyCollection<IWebElement> errors = d.FindElements(By.XPath("//span[@class='Error']"));
                    if (errors.Count > 0)
                    {
                        String msg = errors[0].Text;
                        throw new ActivitySubmissionException(msg);
                    }
                }
                catch (System.NullReferenceException)
                {
                    // Don't like this -- You'd expect Selenium to throw a ElemenetDoesNotExist or WindowDoesNotExist exception instead...
                    return true;
                }

                return false;
            });
        }

        public void Cancel()
        {
            Driver.Window().Close(Driver.FindElement(By.Id("cancelBtn")).Click);
        }

        public bool Is()
        {
            try
            {
                GetActivityLinkElement();
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void OpenAndSubmit()
        {
            Open();
            Submit();
        }

        public class ActivitySubmissionException : Exception
        {
            public ActivitySubmissionException()
            {

            }

            public ActivitySubmissionException(string message) : base(message)
            {
            }
        }
    }
}
