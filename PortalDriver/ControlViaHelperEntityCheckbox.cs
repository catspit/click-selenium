﻿using System;
using System.Collections;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver
{
    internal class ControlViaHelperEntityCheckbox : IControlFactoryFromHelper
    {
        public IPortalControl create(ControlHelper helper, IPortalDriver driver)
        {
            if (helper.type == "EntityCheckbox") return new ControlEntityCheckbox(helper, driver);
            return null;
        }

        internal class ControlEntityCheckbox : ControlFromHelper
        {
            internal ControlEntityCheckbox(ControlHelper helper, IPortalDriver driver):base(helper,driver)
            {
            }

            public override bool Is(IPortalControlValue value)
            {
                throw new NotImplementedException();
            }

            public override bool Is(string textOrOID)
            {
                throw new NotImplementedException();
            }

            public override string OID
            {
                get {
                    string oidStr = "";
                    var checkboxes = Helper.container[0].FindElements(By.XPath("//input[@type='checkbox' and @name='" + Helper.attribute + @"_setItem']"));
                    for (var i = 0; i < checkboxes.Count; i++)
                    {
                        var box = checkboxes[i];
                        if (box.GetAttribute("checked") == "true")
                        {
                            if (oidStr == "")
                            {
                                oidStr = box.GetAttribute("value");
                            }
                            else
                            {
                                oidStr += "," + box.GetAttribute("value");
                            }
                        }
                    }
                    return oidStr;
                }
            }

            public override string Text
            {
                get { throw new NotImplementedException(); }
            }

            public override IEnumerator<IPortalControlValue> GetEnumerator()
            {
                throw new NotImplementedException();
            }

            public override IPortalControlValue this[int index]
            {
                get { throw new NotImplementedException(); }
            }

            public override int Count
            {
                get { throw new NotImplementedException(); }
            }

            public override IPortalControlValue Clear()
            {
                var checkedValues = Helper.Values;
                // Iterate checked items and uncheck each item
                for (var i = 0; i < checkedValues.Count; i++)
                {
                    var oid = checkedValues[i].OID;
                    this.Val(oid);  // uncheck item
                }
                return null;
            }

            public override IPortalControlValue Val()
            {
                if (this.IsClear) return new PortalControlValue(null, "", true);
                return new PortalControlValue(Helper.OID, Helper.text.Trim(), false);
            }

            public override IPortalControlValue Val(string TextOrOID)
            {
                if (String.IsNullOrEmpty(TextOrOID))
                {
                    this.Clear();
                    return null;
                }

                string shortestMatchingText = null;
                var index = 0;

                for (var i = 0; i < Helper.rangeOID.Count; i++)
                {
                    var oid = Helper.rangeOID[i];
                    var text = Helper.rangeText[i];

                    if (oid == TextOrOID)
                    {
                        index = i;
                        break;
                    }

                    if (!text.Contains(TextOrOID)) continue;
                    if (shortestMatchingText != null && shortestMatchingText.Length <= text.Length) continue;
                    shortestMatchingText = text;
                    index = i;
                }

                IWebElement elem = Helper.widgets[index];
                string elemID = elem.GetAttribute("id");
                IWebElement label = elem.FindElement(By.XPath("following-sibling::label[@for='" + elemID + "']"));
                if (label != null)
                {
                    label.Click();
                }
                else
                {
                    elem.Click();
                }
                return null;
            }

            public override IPortalControlValue Val(DateTime date)
            {
                throw new NotImplementedException();
            }

            public override string Type
            {
                get { throw new NotImplementedException(); }
            }

            public override IWebElement AddButton
            {
                get { throw new NotImplementedException(); }
            }

            public override IWebElement SelectButton
            {
                get { throw new NotImplementedException(); }
            }

        }
    }
}
