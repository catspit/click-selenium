﻿using ClickPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;

namespace ClickPortal.IRB
{
    public class IRBCR : IRBStudy
    {
        public IRBCR() 
        {
            this.SubmissionType = "CR";
            this.Name = this.Description = "Test CR";
            this.ResourceContainerTemplateID = "TMPL4DA9E8BDCB400";
        }
    }
}
