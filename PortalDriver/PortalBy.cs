﻿using System;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using OpenQA.Selenium;

namespace ClickPortal {

    public class PortalBy : By
    {
        private By byStrategy;

        protected PortalBy(By byStrategy)
        {
            this.byStrategy = byStrategy;
        }

        public static PortalBy ControlCaptionText(string captionTextToFind)
        {
            return new PortalBy(new CaptionBy(captionTextToFind));
        }

        public static PortalBy ControlAttribute(string attributeTailToFind)
        {
            return new PortalBy(new AttributeBy(attributeTailToFind));
        }

        public static By ProjectCreatorTitle(string titleTextToFind)
        {
            const string template = "//*[contains(@onclick,'createProject')][contains(@title,'$TITLE$') or contains(@value,'$TITLE$')]";
            var xpath = template.Replace("$TITLE$", titleTextToFind);
            return By.XPath(xpath);
        }

        public static By ProjectEditorAlt(string altTextToFind)
        {
            const string template = "//a[contains(@href,'ResourceAdministration/Project/ProjectEditor')]/img[contains(@alt,'$TITLE$')]";
            var xpath = template.Replace("$TITLE$", altTextToFind);
            return By.XPath(xpath);
        }

        public static By TabTitle(string titleTextToFind)
        {
            const string template = "//a[(@class='TabOn' or @class='TabOff') and contains(text(), '$TITLE$')]";
            var xpath = template.Replace("$TITLE$", titleTextToFind);
            return By.XPath(xpath);
        }

        public static By TopNavTabTitle(string titleTextToFind)
        {
            const string template = "//a[(@class='TopNavTabOn' or @class='TopNavTabOff') and contains(text(), '$TITLE$')] | //nav[@id='GlobalNavigation']//a[contains(text(),'$TITLE$')]";
            var xpath = template.Replace("$TITLE$", titleTextToFind);
            return By.XPath(xpath);
        }

        public override IWebElement FindElement(ISearchContext context)
        {
            return this.byStrategy.FindElement(context);
        }

        public override ReadOnlyCollection<IWebElement> FindElements(ISearchContext context)
        {
            return this.byStrategy.FindElements(context);
        }
    }


    public class CaptionBy : By
    {
        private string captionTextToFind;
        private string captionQuery;
        private Regex insideReplacement = new Regex("/[^\\S]+/g");

        public CaptionBy(string captionTextToFind)
        {
            if (captionTextToFind == null) throw new ArgumentNullException("captionTextToFind");
            this.captionTextToFind = captionTextToFind;
            const string captionQueryTemplate = "return portalFindElementsByCaption('$$$$CAPTION_TEXT$$$$')";
            this.captionQuery = captionQueryTemplate.Replace("$$$$CAPTION_TEXT$$$$", captionTextToFind.Replace("'", "\\'"));
        }


        public override IWebElement FindElement(ISearchContext context)
        {
            try
            {
                var exec = (IJavaScriptExecutor) context;
                ReadOnlyCollection<IWebElement> result = null;
                try
                {
                    result = (ReadOnlyCollection<IWebElement>) exec.ExecuteScript(this.captionQuery);
                }
                catch
                {
                    exec.ExecuteScript(PortalDriver.PortalDriver._clientControlHelpersJavascript);
                    result = (ReadOnlyCollection<IWebElement>)exec.ExecuteScript(this.captionQuery);
                }

                return result[0];
            }
            catch
            {
                throw new NoSuchElementException("could not locate element using: " + this.captionTextToFind);
            }
        }

        public override ReadOnlyCollection<IWebElement> FindElements(ISearchContext context)
        {
            try
            {
                var exec = (IJavaScriptExecutor) context;
                var result = (ReadOnlyCollection<IWebElement>) exec.ExecuteScript(this.captionQuery);
                return result;
            }
            catch
            {
                return new ReadOnlyCollection<IWebElement>(new IWebElement[]{});
            }
        }
    }


    public class AttributeBy : By {
        private string captionTextToFind;
        private string captionQuery;
        private Regex insideReplacement = new Regex("/[^\\S]+/g");

        public AttributeBy(string captionTextToFind) {
            if (captionTextToFind == null) throw new ArgumentNullException("captionTextToFind");
            this.captionTextToFind = captionTextToFind;
            const string captionQueryTemplate = "return portalFindElementsByAttributeTail('$$$$CAPTION_TEXT$$$$')";
            this.captionQuery = captionQueryTemplate.Replace("$$$$CAPTION_TEXT$$$$".Replace("'", "\\'"), captionTextToFind);
        }


        public override IWebElement FindElement(ISearchContext context) {
            try {
                var exec = (IJavaScriptExecutor)context;
                ReadOnlyCollection<IWebElement> result = null;
                try {
                    result = (ReadOnlyCollection<IWebElement>)exec.ExecuteScript(this.captionQuery);
                }
                catch {
                    exec.ExecuteScript(PortalDriver.PortalDriver._clientControlHelpersJavascript);
                    result = (ReadOnlyCollection<IWebElement>)exec.ExecuteScript(this.captionQuery);
                }

                return result[0];
            }
            catch {
                throw new NoSuchElementException("could not locate element using: " + this.captionTextToFind);
            }
        }

        public override ReadOnlyCollection<IWebElement> FindElements(ISearchContext context) {
            try {
                var exec = (IJavaScriptExecutor)context;
                var result = (ReadOnlyCollection<IWebElement>)exec.ExecuteScript(this.captionQuery);
                return result;
            }
            catch {
                return new ReadOnlyCollection<IWebElement>(new IWebElement[] { });
            }

        }
    }
}






//             var xpath = "//span[@id='_span_ALD_caption'][./*/text()[contains(.,'Comment')]]";
//xpath = "//*[contains(@*,'TrainingCompleted')]";
//var a = MockPortalControl.portalDriver.FindElements(By.XPath(xpath));
//var container = "ancestor::*[@id[contains(.,'_container')]]";
//var b = a[0].FindElement(By.XPath(container));
//var c = b.GetAttribute("id")
