﻿using ClickPortal.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ClickPortal.IACUC.Substance.Pages
{
    public class SubstanceWorkspace : PageController
    {
        public String OID;
        private String _projectOid;

        public SubstanceWorkspace(String Oid)
        {
            this.OID = Oid;
        }

        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=" + OID);
        }

        public String Status
        {
            get
            {
                return driver.FindElement(By.XPath("/*[@id='_ClickSubstance.status.ID_container']/span[3]/span/span[1]")).Text;
            }
        }

        public void OpenActivity(String LinkText)
        {
            IWebElement anchor = driver.FindElement(By.LinkText(LinkText));
            OpenPopup(anchor, null, By.XPath("//td[@class='FormHead']"));
            
        }
        public bool HasActivity(String LinkText)
        {
            if (driver.FindElements(By.LinkText(LinkText)).Count > 0)
            {
                return true;
            }

            return false;
        }

        public String projectOid
        {
            get
            {
                if (_projectOid == default(String))
                {
                    IWebElement scriptTag = driver.FindElement(By.XPath("//script[contains(.,'PortalTools.currentResource')]"));
                    String script = scriptTag.GetAttribute("innerHTML");
                    Regex regex = new Regex(@"PortalTools.currentResource = '(com.webridge.entity.Entity\[OID\[[0-9A-F]{32}\]\])';");
                    Match m = regex.Match(script);
                    if (!m.Success)
                    {
                        throw new Exception("Problem finding protocol OID.");
                    }
                    _projectOid = m.Groups[1].Value;
                }

                return _projectOid;
            }
        }
    }
}
