﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Internal;

namespace ClickPortal.PortalDriver
{
    public class PortalDriver : IPortalDriver
    {
        private static List<IPortalDriver> _portalDrivers = new List<IPortalDriver>();
        
        private PortalDriver _adminDriver;
        private readonly IWebDriver _baseDriver;

        internal static string _clientControlHelpersJavascript;
        private static bool hasExported = false;

        internal PortalJsonDriverConfiguration jsonConfig;
        private IPortalControl lastControl;
        private IPortalActivity lastActivity;
        private IPortalJob lastJob;

        private IPortalStore defaultStore;
        private Dictionary<string, IPortalStore> stores;

        public static void DisposeAll()
        {
            foreach (var d in _portalDrivers)
            {
                try
                {
                    d.Dispose();
                }
                catch
                {
                }
            }
        }

        public PortalDriver(String jsonConfig = null, IWebDriver driver = null)
        {

            _portalDrivers.Add(this);

            if (!hasExported) {
                //static resources, driver export, javascript loaded from assembly
                exportAllResources();
                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = this.GetType().Namespace + ".ClientControlHelpers.js";

                using (var stream = assembly.GetManifestResourceStream(resourceName))
                using (var reader = new StreamReader(stream)) {
                    _clientControlHelpersJavascript = reader.ReadToEnd();
                }
                hasExported = true;
            }

            this._baseDriver = driver != null ? driver : defaultDriver();
            if (jsonConfig != null) this.jsonConfig = new PortalJsonDriverConfiguration(jsonConfig);
        }


        private IWebDriver defaultDriver() {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("disable-extensions");
            options.AddArguments("--disable-gpu");
            return new ChromeDriver(this.ResourceDirectory, options);
        }


        internal PortalDriver AdminDriver {
            get {
                if (_adminDriver != null) return this._adminDriver;
                ChromeOptions options = new ChromeOptions();
                options.AddArguments("disable-extensions");
                options.AddArguments("--disable-gpu");
                this._adminDriver = new PortalDriver(null, new ChromeDriver(this.ResourceDirectory, options));
                return this._adminDriver;
            }
        }




        private void exportAllResources()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            String exportResourcesPath = this.GetType().Namespace + ".ExportResources";

            var resources = from String resource in assembly.GetManifestResourceNames()
                            where resource.StartsWith(exportResourcesPath)
                            select resource;



            foreach (String resource in resources) {
                var file = resource.Replace(exportResourcesPath + ".", "");
                WriteResourceToFile(resource, this.ResourceDirectory + file);
            }

        }

        private static void WriteResourceToFile(string resourceName, string fileName) {
            using (var resource = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName)) {
                if (resource == null) return;

                using (var file = new FileStream(fileName, FileMode.Create, FileAccess.Write)) {
                    //log("exporting: " + resourceName + " to " + fileName);
                    resource.CopyTo(file);
                }
            }
        }

        public void WaitUntilPageLoaded()
        {
            new WebDriverWait(this, TimeSpan.FromSeconds(10)).Until(
    d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public IWebElement FindElement(By @by)
        {
            return this._baseDriver.FindElement(by);
        }

        public ReadOnlyCollection<IWebElement> FindElements(By @by)
        {
            return this._baseDriver.FindElements(by);
        }

        public void Dispose()
        {
            if (this._adminDriver != null) this._adminDriver.Dispose();
            this._baseDriver.Dispose();
        }

        public void Close()
        {
            this._baseDriver.Close();
        }

        public void Quit()
        {
            if (this._adminDriver != null) this._adminDriver.Quit();
            this._baseDriver.Quit();
        }


        public IOptions Manage()
        {
            return this._baseDriver.Manage();
        }


        private class NavigationDecorator : INavigation
        {
            private INavigation baseNavigator;
            private IJavaScriptExecutor jsRunner;
            private string script;

            public NavigationDecorator(INavigation baseNavigator, IJavaScriptExecutor jsRunner, string script)
            {
                this.baseNavigator = baseNavigator;
                this.jsRunner = jsRunner;
                this.script = script;
            }

            private void injectJavascript()
            {
                this.jsRunner.ExecuteScript(this.script);
            }

            public void Back()
            {
                this.baseNavigator.Back();
                this.injectJavascript();
            }

            public void Forward()
            {
                this.baseNavigator.Forward();
                this.injectJavascript();
            }

            public void GoToUrl(string url)
            {
                this.baseNavigator.GoToUrl(url);
                this.injectJavascript();
            }

            public void GoToUrl(Uri url)
            {
                this.baseNavigator.GoToUrl(url);
                this.injectJavascript();
            }

            public void Refresh()
            {
                this.baseNavigator.Refresh();
                this.injectJavascript();
            }
        }


        public INavigation Navigate()
        {
            var nextPage = this._baseDriver.Navigate();
            return new NavigationDecorator(nextPage, (IJavaScriptExecutor)this, _clientControlHelpersJavascript);
        }


        public ITargetLocator SwitchTo()
        {
            return this._baseDriver.SwitchTo();
        }

        public string Url
        {
            get { return this._baseDriver.Url; }
            set { this._baseDriver.Url = value; }
        }

        public string Title
        {
            get { return this._baseDriver.Title; }
        }

        public string PageSource
        {
            get { return this._baseDriver.PageSource; }
        }

        public string CurrentWindowHandle
        {
            get { return this._baseDriver.CurrentWindowHandle; }
        }

        public ReadOnlyCollection<string> WindowHandles
        {
            get { return this._baseDriver.WindowHandles; }
        }

        public IPortalControl Control()
        {
            return this.lastControl;
        }

        public IPortalControl Control(string locator)
        {
            this.lastControl = new PortalControl(this, locator);
            return this.lastControl;
        }

        public IPortalControl Control(PortalBy @by)
        {
            return this.Control((By) by);
        }

        public IPortalControl Control(By @by)
        {
            this.lastControl = new PortalControl(this, by);
            return this.lastControl;
        }

        public IPortalStore Store(string configIdOrUrl)
        {
            if (stores[configIdOrUrl] == null)
            {
                stores[configIdOrUrl] = new PortalStore(this, configIdOrUrl);
            }
            return stores[configIdOrUrl];
        }

        public IPortalStore Store()
        {
            if (defaultStore == null)
            {
                defaultStore = new PortalStore(this, null);
            }
            return defaultStore;
        }


        public IPortalConsole Console(string configIdOrUrl) {
            return new PortalConsole(this, configIdOrUrl);
        }

        public IPortalConsole Console()
        {
            return new PortalConsole(this,null);
        }

        public IPortalProjectCreator ProjectCreator(string title)
        {
            return this.ProjectCreator(PortalBy.ProjectCreatorTitle(title));
        }

        public IPortalProjectCreator ProjectCreator(By @by)
        {
            return new PortalProjectCreator(by, this);
        }

        public IPortalProjectEditor ProjectEditor(string title)
         {
             return this.ProjectEditor(PortalBy.ProjectEditorAlt(title));
         }

        public IPortalProjectEditor ProjectEditor(By @by)
        {
            return new PortalProjectEditor(by, this);
        }

        public IPortalActivityLog ActivityLog()
        {
            return new PortalActivityLog(this);
        }

        public IPortalActivity Activity()
        {
            return lastActivity;
        }

        public IPortalActivity Activity(string locator)
        {
            lastActivity = new PortalActivity(this, locator);
            return lastActivity;
        }

        public IPortalJob Job()
        {
            return lastJob;
        }

        public IPortalJob Job(string name)
        {
            lastJob = new PortalJob(this, name);
            return lastJob;
        }

        public IPortalSmartform Smartform()
        {
            return new PortalSmartform(this);
        }

        public IPortalTab Tab(string title)
        {
            if (this.Store().portalVersionMajor == 8)
            {
                return this.Tab(PortalBy8.TabTitle(title), title);
            }
            return this.Tab(PortalBy.TabTitle(title), title);
        }

        public IPortalTab Tab(By @by)
        {
            return new PortalTab(by, this);
        }
        public IPortalTab Tab(By @by, string title)
        {
            return new PortalTab(by, title, this);
        }

        public IPortalProjectListing ProjectListing()
        {
            return new ProjectListing(null, this);
        }

        public IPortalTopNavTab TopNavTab(string title)
        {
            return this.TopNavTab(PortalBy.TopNavTabTitle(title));
        }

        public IPortalTopNavTab TopNavTab(By @by)
        {
            return new PortalTopNavTab(by, this);
        }

        public IPortalChooser Chooser()
        {
            return new PortalChooser(this);
        }

        public IPortalWindow Window()
        {
            return new PortalWindow(this);
        }

        public string StoreName
        {
            get { throw new NotImplementedException(); }
        }

        public string ResourceDirectory
        {
            get
            {
                return Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            }
        }

        public IKeyboard Keyboard {
            get
            {
                var chrome = (IHasInputDevices)this._baseDriver;
                return chrome.Keyboard;
            }
        }

        public IMouse Mouse
        {
            get
            {
                var chrome = (IHasInputDevices)this._baseDriver;
                return chrome.Mouse;
            }
        }

        public string UrlForFileResource(string resourceFileName)
        {
            return "file:\\\\" + this.ResourceDirectory + resourceFileName;
        }

        public object ExecuteScript(string script, params object[] args)
        {

            var jsExec = (IJavaScriptExecutor) this._baseDriver;
            return jsExec.ExecuteScript(script, args);
        }

        public object ExecuteAsyncScript(string script, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void NavigateToRelativePath(String path)
        {
            NavigateToRelativePath(path, new Dictionary<String, String>());
        }

        public void NavigateToRelativePath(String path, Dictionary<String, String> query)
        {
            UriBuilder uri = new UriBuilder(this.getBaseURL(null));
            uri.Scheme = Uri.UriSchemeHttps;
            uri.Path += path;

            if (query != null && query.Count > 0)
            {
                foreach (KeyValuePair<String, String> param in query)
                {
                    if (uri.Query != null && uri.Query.Length > 1)
                        uri.Query = uri.Query.Substring(1) + "&" + param.Key + "=" + param.Value;
                    else
                        uri.Query = param.Key + "=" + param.Value;
                }
            }

            try
            {
                this.Navigate().GoToUrl(uri.Uri.GetComponents(UriComponents.HttpRequestUrl, UriFormat.Unescaped));
            }
            catch (UnhandledAlertException)
            {
                // Portal 8 sometimes throws an alert message if you navigate away from a page too quickly. 
                // It's safe to just dismiss it and try again.
                IAlert alert = this.SwitchTo().Alert();
                if (alert.Text != "Error retrieving tab content: [object Object]")
                {
                    throw;
                }

                alert.Dismiss();
                new WebDriverWait(this, TimeSpan.FromSeconds(3)).Until(ExpectedConditions.AlertState(false));
                this.SwitchTo().DefaultContent();
                Thread.Sleep(500); // Explicit wait to give the content a chance to load.

                this.NavigateToRelativePath(path, query);
            }
        }

        internal PortalJsonDriverConfiguration.StoreConfig getStoreConfig(string StoreLabelOrURL) {
            if (this.jsonConfig == null) return null;
            return (StoreLabelOrURL == null) ?
                jsonConfig.Store() : jsonConfig.Store(StoreLabelOrURL);
        }

        public string getBaseURL(string StoreLabelOrURL) {
            var sc = getStoreConfig(StoreLabelOrURL);
            return (sc == null) ? StoreLabelOrURL : sc.URL;
        }

        public void InFrame(By by, Action<IPortalDriver> p)
        {
            this.SwitchTo().Frame(this.FindElement(by));
            try
            {
                p(this);
            }
            finally
            {
                this.SwitchTo().ParentFrame();
            }
        }

        public void PerformActions(IList<ActionSequence> seq)
        {
            IActionExecutor driver = (IActionExecutor)this._baseDriver;
            driver.PerformActions(seq);
        }

        public void ResetInputState()
        {
            IActionExecutor driver = (IActionExecutor)this._baseDriver;
            driver.ResetInputState();
        }

        public bool IsActionExecutor
        {
            get
            {
                IActionExecutor driver = (IActionExecutor)this._baseDriver;
                return driver.IsActionExecutor;
            }
        }
    }
}