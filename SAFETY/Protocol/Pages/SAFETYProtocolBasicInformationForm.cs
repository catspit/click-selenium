﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClickPortal;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.SAFETY.Protocol.Pages
{
    public class SAFETYProtocolBasicInformationForm : SAFETYProtocolFormPage
    {
        public SAFETYProtocolBasicInformationForm(SAFETYProtocol protocol)
            : base(protocol)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[D3745CA36B43DA4FACF3E4F185EFC7A3]]");
        }

        public void fillOutTitleProtocol(string title)
        {
            driver.FindElement(By.XPath("//textArea[@name='_ClickSafetySubmission.customAttributes.protocolTitle_text']")).SendKeys(title);
        }

        public void fillOutShortTitle(string title)
        {
            driver.FindElement(By.XPath("//input[@name='_ClickSafetySubmission.name']")).SendKeys(title);
        }

        public void fillOutSummaryOfResearch(string summary)
        {
            driver.FindElement(By.XPath("//textArea[@name='_ClickSafetySubmission.description']")).SendKeys(summary);
        }

        public void selectSafetyReview(string safetyreview)
        {
            IWebElement safetyreviewElement = driver.FindElement(By.XPath("//td[text()='" + safetyreview + "']/../td/input[@type='radio']"));
            (driver as IJavaScriptExecutor).ExecuteScript(string.Format("window.scrollTo(0,{0});", safetyreviewElement.Location.Y));

            driver.FindElement(By.XPath("//td[text()='" + safetyreview + "']/../td/input[@type='radio']")).Click();
        }

        public void clickContinue()
        {
            driver.FindElement(By.XPath("//*[@id='continue_btn_Bottom']")).Click();
        }

        public void selectPrincipalInvestigatorByUserID(string investigatorUserID)
        {
            IWebElement selectPIButton = driver.FindElement(By.XPath("//span[@id='__ClickSafetySubmission.customAttributes.investigator.customAttributes.studyTeamMember_container']//button[contains(@id,'_selectBtn')]"));
            SelectPersonForm popup = new SelectPersonForm(selectPIButton);
            popup.searchAndSelectPerson(investigatorUserID, "User ID");
        }
    }
}
