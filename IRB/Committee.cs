﻿using System;
using System.Collections.Generic;
using ClickPortal;

namespace ClickPortal.IRB
{
    public class Committee : ClickEType
    {
        protected static int created = 0;

        [ETypeProperty("ID")]
        public string ID;

        [ETypeProperty("name")]
        public string Name;

        [ETypeProperty("company", RelatedEType = "Company", RelatedKeyName = "ID")]
        public string Company;

        [ETypeProperty("createdBy", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string CreatedBy;

        [ETypeProperty("customAttributes.committeeType", RelatedEType = "_CommitteeType", RelatedKeyName = "customAttributes.name")]
        public string CommitteeType;

        [ETypeProperty("description")]
        public string Description;

        [ETypeProperty("customAttributes.adminOffice", RelatedEType = "_AdminOffice", RelatedKeyName = "ID")]
        public string AdministrativeOffice;


        public List<String> committeeAdministrators;
        public List<String> committeeMembers;

        private int committeeNumber;

        public Committee() :
            this(new List<string>() { "000000ADMINZZ" }, new List<string>() { "000000ADMINZZ" })
        {
            ;
        }

        public Committee(List<String> cAdmin, List<String> cMember) : base("_Committee")
        {
            this.committeeNumber = ++created;
            this.Name = this.ID = "Test-" + committeeNumber;
            this.Company = "Huron Consulting Group";
            this.CreatedBy = "administrator";
            this.CommitteeType = "Institutional Review Board";
            this.Description = "This is a test committee, please remove at your convenience.";
            this.AdministrativeOffice = "Human Subjects Protection Office";

            committeeAdministrators = (cAdmin.Count > 0) ? cAdmin : new List<string>() { "000000ADMINZZ" };
            committeeMembers = (cMember.Count > 0) ? cMember : new List<string>() { "000000ADMINZZ" };
        }

        public override void CreateEntity(PortalDriver.PortalDriver Driver)
        {
            base.CreateEntity(Driver);
        }

        public override string CreateScript
        {
            get
            {
                string script = base.CreateScript;
                foreach (string cAdmin in this.committeeAdministrators)
                {
                    script += @"
var _" + cAdmin + @" = getElements(""PersonForID"", ""ID"", """ + cAdmin + @""").item(1);
etype.setQualifiedAttribute(""customAttributes.committeeAdministrators"", _" + cAdmin + @", ""add"");
etype.setQualifiedAttribute(""customAttributes.committeeMembers"", _" + cAdmin + @",""add"");
etype.setQualifiedAttribute(""customAttributes.committeeChairs"", _" + cAdmin + @",""add"");
etype.setQualifiedAttribute(""contacts"", _" + cAdmin + @",""add"");";
                }

                foreach (string cMember in this.committeeMembers)
                {
                    script += @"
var _" + cMember + @" = getElements(""PersonForID"", ""ID"", """ + cMember + @""").item(1);
var memberType = getResultSet(""_ClickCommitteeMembershipType"").query(""ID='Chair'"").elements().item(1);
var ctm" + cMember + @" = _ClickCommitteeMemberInfo.createEntity();
ctm" + cMember + @".setQualifiedAttribute(""customAttributes.member"", _" + cMember + @");
ctm" + cMember + @".setQualifiedAttribute(""customAttributes.membershipType"", memberType);
etype.setQualifiedAttribute(""customAttributes.committeeMemberInfo"", ctm" + cMember + @", ""add"");
etype.setQualifiedAttribute(""customAttributes.committeeMembers"", _"+cMember + @",""add"");
etype.setQualifiedAttribute(""customAttributes.committeeChairs"", _" + cMember + @",""add"");
etype.setQualifiedAttribute(""contacts"", _" + cMember + @",""add"");";
                }
                script += @"
etype.syncWithCommitteeMemberInfo();
etype.validateChair();
etype.setQualifiedAttribute(""status"", getElements(""ProjectStatusForProjectTypeAndID"",""ID"",""Open"",""projectType"",""_Committee"")(1));
etype.setQualifiedAttribute(""projects"", EntityUtils.createEntitySet(""Project""));";
                return script;
            }
        }

        public void addAdministrator(string userId, PortalDriver.PortalDriver driver)
        {
            String script = @"var committee = wom.getEntityFromString(""" + this.OID + @""");
var user = getElements(""personForUserId"", ""UserId"", """ + userId + @""")(1);
committee.setQualifiedAttribute(""customAttributes.committeeAdministrators"", user, ""add"");
committee.setQualifiedAttribute(""customAttributes.committeeMembers"", user, ""add"");
committee.setQualifiedAttribute(""customAttributes.committeeChairs"", user, ""add"");
committee.setQualifiedAttribute(""contacts"", user, ""add"");
committee.syncWithCommitteeMemberInfo();
committee.validateChair();";
            driver.Store().Execute(script);
        }
    }
}
