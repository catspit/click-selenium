﻿using System;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver {
    internal class ControlViaHelperReadOnly : IControlFactoryFromHelper
    {

        public IPortalControl create(ControlHelper helper, IPortalDriver driver)
        {
            return "readOnly".Contains(helper.type) ? new ControlReadOnly(helper) : null;
        }
        
        internal class ControlReadOnly : ControlFromHelper
        {
            internal ControlReadOnly(ControlHelper helper):base(helper)
            {
            }

            public override IWebElement AddButton
            {
                get { throw new NotImplementedException(); }
            }

            public override IWebElement SelectButton
            {
                get { throw new NotImplementedException(); }
            }

            public override IPortalControlValue Clear()
            {
                throw new NotImplementedException();
            }

            public override bool Is(IPortalControlValue value)
            {
                if (!string.IsNullOrEmpty(value.OID)) return false;
                return Is(value.Text);
            }

            public override bool Is(string textOrOID)
            {
                var text = textOrOID ?? "";
                if (text == "" && Helper.text == "") return true;
                return Helper.text.Trim().Contains(text.Trim());
            }

            public override IPortalControlValue Val()
            {
                return new PortalControlValue(Helper.OID, Helper.text.Trim(), false);
            }

            public override IPortalControlValue Val(string TextOrOID)
            {
                throw new NotImplementedException();
            }

            public override IPortalControlValue Val(DateTime date)
            {
                throw new NotImplementedException();
            }
        }
    }
}
