﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class IACUCProtocolSupportingDocuments : IACUCProtocolFormPage
    {
        public IACUCProtocolSupportingDocuments(IACUCProtocol protocol)
            : base(protocol)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[1D3C81C1FE50E941881374F15430250B]]");
        }
    }
}
