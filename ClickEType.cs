﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace ClickPortal
{
    public class ClickEType
    {
        public readonly string ETypeName;
        protected int InstanceNumber;
        protected static int TotalCreated = 0;

        public ClickEType(string name)
        {
            this.ETypeName = name;

            // Update the instance number
            this.InstanceNumber = TotalCreated;
            TotalCreated++;
        }

        public string OID;


        public virtual string CreateScript
        {
            get
            {
                string scriptPart1 = "var etype = " + this.ETypeName + ".createEntity()";
                string scriptPart2 = "";
                string scriptPart3 = "";
                int relatedCount = 0;
                foreach (FieldInfo field in this.GetType().GetFields())
                {
                    foreach (object attr in field.GetCustomAttributes(true))
                    {
                        if (attr is ETypeProperty && field.GetValue(this) != null)
                        {
                            string value;
                            if (((ETypeProperty)attr).RelatedEType != null)
                            {
                                string fieldname = ((ETypeProperty)attr).RelatedKeyName == null ? "ID" : ((ETypeProperty)attr).RelatedKeyName;
                                scriptPart3 += "\nvar related" + ++relatedCount + " = getResultSet(\""
                                    + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                    + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + (field.Name.Equals("Status") ? " AND projectType = '" + ETypeName + "'" : "")
                                    + @""");
if (related" + relatedCount + @".count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                value = "related" + relatedCount + ".elements()(1)";
                            }
                            else
                            {
                                value = JsonConvert.SerializeObject(field.GetValue(this));
                            }
                            string propertyName = ((ETypeProperty)attr).PropertyName;
                            if (propertyName == "ID")
                            {
                                scriptPart2 += "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + value + ");";
                            }
                            else
                            {
                                scriptPart3 += "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + value + ");";
                            }
                        }
                    }
                }
                scriptPart3 += "\n?etype;";
                return scriptPart1 + scriptPart2 + scriptPart3;
            }
        }

        public virtual string DestroyScript
        {
            get
            {
                return @"EntityUtils.getObjectFromString(""" + this.OID + @""").unregisterEntity();
?""Done"";";
            }
        }

        public virtual void CreateEntity(PortalDriver.PortalDriver Driver)
        {
            var output = Driver.Store().Execute(this.CreateScript);
            if (!new Regex(@"com\.webridge\.\S+\[OID\[\w{32}\]\]").Match(output).Success)
            {
                throw new Exception("Error creating test entity: " + output);
            }
            this.OID = output;
        }

        public virtual void DestroyEntity(PortalDriver.PortalDriver Driver)
        {
            String output = Driver.Store().Execute(this.DestroyScript);
            if (!new Regex(@"^Done$").Match(output).Success)
            {
                throw new Exception("Error destroying test entity: " + output);
            }
        }
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple=true)]
    public class ETypeProperty : System.Attribute
    {
        public readonly string PropertyName;
        public string RelatedEType;
        public string RelatedKeyName;
    
        public ETypeProperty(string name)
        {
            this.PropertyName = name;
        }
    }
}