﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace ClickPortal.PortalDriver
{
    class PortalConsole : IPortalConsole
    {

        private PortalDriver portalDriver;
        private string urlOrLabel;
        private const string ConsolePath = "/DebugConsole/CommandWindow/Command";

        internal PortalConsole(PortalDriver portalDriver, string urlOrLabel)
        {
            this.portalDriver = portalDriver;
            this.urlOrLabel = urlOrLabel;
        }

        public bool Go()
        {
            var url = this.portalDriver.getBaseURL(this.urlOrLabel);
            this.portalDriver.Navigate().GoToUrl(url + ConsolePath + "?_webrNew=All");
            return this.Is();
        }

        public bool Is()
        {
            return (this.portalDriver.Url.ToLower().Contains(ConsolePath.ToLower()));
        }

        public string Execute(string script)
        {
            ((IJavaScriptExecutor)this.portalDriver).ExecuteScript(@"document.getElementById('CommandScript.script').value = arguments[0];", script);
            IWebElement runButton = this.portalDriver.FindElement(By.Id("Run"));
            ((IJavaScriptExecutor)this.portalDriver).ExecuteScript("arguments[0].scrollIntoView();", runButton);
            runButton.Click();
            new WebDriverWait(this.portalDriver, TimeSpan.FromSeconds(30)).Until(driver =>
            {
                // Make sure to wait until the command console script finished running.
                var outputScript = driver.FindElement(By.XPath("//span[@class='ControlAreaRight']/span[@id='Status']"));
                var output = outputScript.Text;

                var outputValueContainer = driver.FindElement(By.Id("OutputScript"));
                var outputValue = outputValueContainer.GetAttribute("value");

                return output.IndexOf("saving") == -1 && output.IndexOf("running") == -1 && output != null && output != "";
            });
            
            return this.portalDriver.FindElement(By.Id("OutputScript")).GetAttribute("value");
        }
    }
}