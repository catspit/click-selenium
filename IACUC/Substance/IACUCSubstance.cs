﻿using ClickPortal.IACUC.ResearchTeam;
using ClickPortal.IACUC.Substance.Pages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ClickPortal.IACUC.Substance
{


    public class IACUCSubstance : ClickEType
    {

        protected static int created = 0;

        //id, description, status, reesearch team
        [ETypeProperty("ID")]
        public string ID;

        [ETypeProperty("name")]
        public string Name;

        [ETypeProperty("description")]
        public string Description;
        [ETypeProperty("company", RelatedEType = "Company")]
        public string Company;
        [ETypeProperty("createdBy", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string CreatedBy;

        [ETypeProperty("customAttributes.researchTeam", RelatedEType = "_ClickResearchTeam", RelatedKeyName = "name")]
        public IACUCResearchTeam ResearchTeam;

        [ETypeProperty("status", RelatedEType = "ProjectStatus")]
        public string Status;
        [ETypeProperty("owner", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string Owner;
        protected int protocolNumber;
        public string ResourceContainerTemplateID = "TMPL8D094BCCE52180C";
        public string ResourceContainerParent = "CLICK_IACUC_HOME";
        private IACUCSubstanceForm _substanceForm;


        protected SubstanceWorkspace workspace;

        public SubstanceWorkspace Workspace
        {
            get
            {
               
                return workspace;
            }
            set
            {
                this.workspace = value;
            }
        }


        public new String OID
        {
            get
            {
                if (base.OID == default(String))
                {
                    this.Workspace.Navigate();
                    base.OID = this.Workspace.projectOid;
                }
                return base.OID;
            }
            set
            {
                base.OID = value;
            }
        }

        public IACUCSubstanceForm SubstanceForm
        {
            get
            {
                if (_substanceForm == null)
                {
                    _substanceForm = new IACUCSubstanceForm(this);
                }
                return _substanceForm;
            }
        }
        

        public IACUCSubstance() : base("_ClickSubstance")
        {
            this.protocolNumber = ++created;
            this.ID = "TEST-" + protocolNumber;
            this.Name = this.Description = "Test Substance " + protocolNumber;
            this.ResearchTeam = new IACUCResearchTeam();

        }
        #region workspace
        public void FindWorkspace(PortalDriver.PortalDriver Driver)
        {
            string getWorkspaceOidScript = @"?wom.getEntityFromString(""" + this.OID + @""").getQualifiedAttribute(""resourceContainer"");";
            string workspaceOid = Driver.AdminDriver.Console().Execute(getWorkspaceOidScript);
            this.Workspace = new SubstanceWorkspace(workspaceOid);
        }
        #endregion

        #region create script
        public override string CreateScript
        {
            get
            {
                string script = @"var etype = wom.createTransientEntity(""_ClickSubstance"");
etype.registerEntity();
etype.initialize();";
                string propertiesThatMustBeAddedAtTheEnd = "";
                List<string> optionalParams = new List<string>();

                int relatedCount = 0;
                foreach (FieldInfo field in this.GetType().GetFields())
                {
                    foreach (object attr in field.GetCustomAttributes(true))
                    {
                        if (attr is ETypeProperty && field.GetValue(this) != null)
                        {
                            if (((ETypeProperty)attr).RelatedEType != null)
                            {
                                string fieldname = ((ETypeProperty)attr).RelatedKeyName == null ? "ID" : ((ETypeProperty)attr).RelatedKeyName;
                                switch (((ETypeProperty)attr).PropertyName)
                                {
                                    case "owner":
                                        script += "\nvar entOwnerPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entOwnerPerson == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "company":
                                        script += "\nvar entCompany = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entCompany == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "createdBy":
                                        script += "\nvar entCreatedByPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entCreatedByPerson == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "status":
                                        propertiesThatMustBeAddedAtTheEnd += "\nvar related" + ++relatedCount + " = getResultSet(\""
                                    + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                    + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @" AND projectType='_ClickSubstance'"
                                    + @""");
if (related" + relatedCount + @".count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }" + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + "related" + relatedCount + ".elements()(1)" + ");";
                                        break;
                                    case "customAttributes.researchTeam":
                                        script += "\nvar researchTeam = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "='" + this.ResearchTeam.Name + "'"
                                    + @""").elements().item(1);
if (researchTeam == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    default:
                                        optionalParams.Add("\nvar related" + ++relatedCount + " = getResultSet(\""
                                    + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                    + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""");
if (related" + relatedCount + @".count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }" + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + "related" + relatedCount + ".elements()(1)" + ");");
                                        break;
                                }
                            }
                            else
                            {
                                switch (((ETypeProperty)attr).PropertyName)
                                {
                                    case "ID":
                                        propertiesThatMustBeAddedAtTheEnd += "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");";
                                        break;

                                    default:
                                        optionalParams.Add("\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");");
                                        break;
                                }
                            }
                        }
                    }
                }

                script += "\netype.setRequiredFields(entOwnerPerson, entCompany, entCreatedByPerson);";
                script += "\netype.setQualifiedAttribute(\"customAttributes.researchTeam\", researchTeam);";
                foreach (string snip in optionalParams)
                {
                    script += snip;
                }
                script += @"
var rc = ResourceContainer.createFromResource(etype, getElements(""ContainerTemplateForID"", ""ID"", """ + ResourceContainerTemplateID + @""")(1));
rc.setQualifiedAttribute(""parent"", getElements(""ContainerBaseForID"", ""ID"", """ + ResourceContainerParent + @""")(1));
etype.setQualifiedAttribute(""resourceContainer"", rc);
var sch = ShadowSCH.getSCH();
ProjectStateTransition.processProject(sch, etype, etype, null);
" + propertiesThatMustBeAddedAtTheEnd + @"
?etype;";
                return script;
            }
        }
        #endregion




        #region create and destroy entity
        public override void CreateEntity(PortalDriver.PortalDriver Driver)
        {
            if (this.ResearchTeam != null)
            {
                this.ResearchTeam.CreateEntity(Driver);
            }
            base.CreateEntity(Driver);
            FindWorkspace(Driver);
        }

        public override void DestroyEntity(PortalDriver.PortalDriver Driver)
        {
            if (this.ResearchTeam != null)
            {
                this.ResearchTeam.DestroyEntity(Driver);
            }
            base.DestroyEntity(Driver);
        }
        #endregion


    }
}
