﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Substance.Pages
{
    public class IACUCSubstanceInformation : IACUCSubstanceFormPage
    {
        public IACUCSubstanceInformation(IACUCSubstance substance)
            : base(substance)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _substance.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[AF6D09602CA0594BBB2DCFA31666706A]]");
        }
    }
}
