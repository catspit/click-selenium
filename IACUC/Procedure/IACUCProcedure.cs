﻿using ClickPortal;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ClickPortal.Pages;
using ClickPortal.IACUC.Procedure.Pages;
using ClickPortal.PortalDriver;
using Newtonsoft.Json;


namespace ClickPortal.IACUC.Procedure
{
    public class IACUCProcedure : ClickEType
    {
        protected static int created = 0;

        [ETypeProperty("ID")]
        public string ID;
        [ETypeProperty("name")]
        public string Name;
        [ETypeProperty("description")]
        public string Description;
        [ETypeProperty("company", RelatedEType = "Company")]
        public string Company;
        [ETypeProperty("status", RelatedEType = "ProjectStatus")]
        public string Status;
        [ETypeProperty("createdBy", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string creatorUserID;
        [ETypeProperty("customAttributes.researchTeam", RelatedEType = "_ClickResearchTeam", RelatedKeyName = "name")]
        public string ResearchTeam;
        [ETypeProperty("owner", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string Owner;
        [ETypeProperty("dateEnteredState")]
        public string DateEnteredState;
        [ETypeProperty("customAttributes.procedureType", RelatedEType = "_ClickProcedureType", RelatedKeyName = "customAttributes.name")]
        public string ProcedureType;
        [ETypeProperty("customAttributes.procedureScope", RelatedEType = "_ClickProcedureSubstanceScope", RelatedKeyName = "customAttributes.name")]
        public string ProcedureScope;


        [ETypeProperty("customAttributes.species", RelatedEType = "_ClickSpecies", RelatedKeyName = "customAttributes.commonName")]
        public string Species;
        [ETypeProperty("customAttributes.causesPainAndDistress")]
        public bool isPainAndDistress;

        public string ResourceContainerTemplateID = "TMPL8D02B5766D47C23";
        public string ResourceContainerParent = "CLICK_IACUC_HOME";

        private IACUCProcedureWorkspace workspace;
        private IACUCProcedureForm _procedureForm;
        protected int procedureNumber;

        public IACUCProcedure() : base("_ClickProcedure")
        {
            this.procedureNumber = ++created;
            this.ID = "TEST-" + procedureNumber;
            this.Name = this.Description = "Test Procedure " + procedureNumber;
            this.Company = "Test Company";
            this.creatorUserID = this.Owner = "administrator";
            this.DateEnteredState = DateTime.Now.ToShortDateString();
            this.ProcedureType = "Survival Surgery";
            this.ResearchTeam = "sup111Team";
            this.Species = "Mice (Laboratory)";
            this.isPainAndDistress = false;
        }

        public IACUCProcedure(IACUCProcedure source) : this()
        {
           this.OID = source.OID;
           this.ID = source.ID;
           this.Name = source.Name;
           this.Description = source.Description;
           this.Company = source.Company;
           this.Status = source.Status;
           this.creatorUserID = source.creatorUserID;
           this.Owner = source.Owner;
           this.ProcedureType = source.ProcedureType;
           this.ProcedureScope = source.ProcedureScope;
           this.Species = source.Species;
           this.isPainAndDistress = source.isPainAndDistress;
           this.Workspace = new IACUCProcedureWorkspace(source.Workspace.OID);
        }
        
        public IACUCProcedure(String ID) : this()
        {
            this.ID = ID;
        }

        public IACUCProcedureWorkspace Workspace
        {
            get
            {
                if (workspace == null)
                {
                    //FindWorkspaceFromProtocolList(protocolList);
                }
                return workspace;
            }
            set
            {
                this.workspace = value;
            }
        }

        public IACUCProcedureForm ProcedureForm
        {
            get
            {
                if (_procedureForm == null)
                {
                    _procedureForm = new IACUCProcedureForm(this);
                }
                return _procedureForm;
            }
        }

        public override string CreateScript
        {
            get
            {
                string script = @"var etype = wom.createTransientEntity(""_ClickProcedure"");
etype.registerEntity();
etype.initialize();";
                string scriptThatCreatesID = "";
                string scriptSetsProcedureScope = "";
                List<string> optionalParams = new List<string>();

                int relatedCount = 0;
                foreach (FieldInfo field in this.GetType().GetFields())
                {
                    foreach (object attr in field.GetCustomAttributes(true))
                    {
                        if (attr is ETypeProperty && field.GetValue(this) != null)
                        {
                            if (((ETypeProperty)attr).RelatedEType != null)
                            {
                                string fieldname = ((ETypeProperty)attr).RelatedKeyName == null ? "ID" : ((ETypeProperty)attr).RelatedKeyName;
                                switch (((ETypeProperty)attr).PropertyName)
                                {
                                    case "owner":
                                        script += "\nvar entOwnerPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entOwnerPerson == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "company":
                                        script += "\nvar entCompany = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entCompany == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "createdBy":
                                        script += "\nvar entCreatedByPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entCreatedByPerson == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "customAttributes.procedureScope":  //This has to come AFTER ProjectStateTransition.processProject()
                                        scriptSetsProcedureScope = "\nvar related" + ++relatedCount + " = getResultSet(\""
                                    + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                    + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""");
if (related" + relatedCount + @".count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }" + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + "related" + relatedCount + ".elements()(1)" + ");";
                                        break;
                                    default:
                                        optionalParams.Add("\nvar related" + ++relatedCount + " = getResultSet(\""
                                    + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                    + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + (field.Name.Equals("Status") ? " AND projectType = '" + ETypeName + "'" : "")
                                    + @""");
if (related" + relatedCount + @".count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }" + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + "related" + relatedCount + ".elements()(1)" + ");");
                                        break;
                                }
                            }
                            else
                            {
                                if (((ETypeProperty)attr).PropertyName == "ID")
                                {
                                    scriptThatCreatesID = "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");";
                                }
                                else
                                {
                                    optionalParams.Add("\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");");
                                }
                            }
                        }
                    }
                }

                script += "\netype.setRequiredFields(entOwnerPerson, entCompany, entCreatedByPerson);";
                foreach (string snip in optionalParams)
                {
                    script += snip;
                }
                script += @"
var rc = ResourceContainer.createFromResource(etype, getElements(""ContainerTemplateForID"", ""ID"", """ + ResourceContainerTemplateID + @""")(1));
rc.setQualifiedAttribute(""parent"", getElements(""ContainerBaseForID"", ""ID"", """ + ResourceContainerParent + @""")(1));
etype.setQualifiedAttribute(""resourceContainer"", rc);
var sch = ShadowSCH.getSCH();
var view = wom.getEntityFromString(""com.webridge.entity.Entity[OID[4A09C07137608B43BC6FAFFCBC186504]]"");
wom.putContext(""currentView"", view, true);
ProjectStateTransition.processProject(sch, etype, etype, null);
" + scriptThatCreatesID + "\r\n" + scriptSetsProcedureScope + @"
?etype;";
                return script;
            }
        }

        public override void CreateEntity(PortalDriver.PortalDriver Driver)
        {
            base.CreateEntity(Driver);
            FindWorkspace(Driver);
        }

        public void FindWorkspace(PortalDriver.PortalDriver Driver)
        {
            string getWorkspaceOidScript = @"?wom.getEntityFromString(""" + this.OID + @""").getQualifiedAttribute(""resourceContainer"");";
            string workspaceOid = Driver.AdminDriver.Console().Execute(getWorkspaceOidScript);
            this.Workspace = new IACUCProcedureWorkspace(workspaceOid);
        }

        public new String OID
        {
            get
            {
                if (base.OID == default(String))
                {
                    this.Workspace.Navigate();
                    base.OID = this.Workspace.projectOid;
                }
                return base.OID;
            }
            set
            {
                base.OID = value;
            }
        }
    }
}
