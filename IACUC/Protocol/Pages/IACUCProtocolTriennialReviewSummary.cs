﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class IACUCProtocolTriennialReviewSummary : IACUCProtocolFormPage
    {
        public IACUCProtocolTriennialReviewSummary(IACUCProtocol protocol) : base(protocol)
        {
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[9A0C6F8395D12E4EA5AE21F555959E89]]");
        }
    }
}
