﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using ClickPortal;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class IACUCProtocolAnimalJustification : IACUCProtocolFormPage
    {
        public IACUCProtocolAnimalJustification(IACUCProtocol protocol)
            : base(protocol)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[4F9C12AAA04C0F4A8A6028A7A9AADD34]]");
        }
    }
}
