﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Procedure.Pages
{
    public class IACUCProcedureNonSurvivalSurgeryForm : IACUCProcedureFormPage
    {
        public IACUCProcedureNonSurvivalSurgeryForm(IACUCProcedure procedure) : base(procedure)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _procedure.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[07B5C77CCEFA654EB416E77A508BFCC0]]");
        }
    }
}
