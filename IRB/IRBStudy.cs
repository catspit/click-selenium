﻿using ClickPortal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using System.Linq;
using System.Collections;

namespace ClickPortal.IRB
{
    public class IRBStudy : ClickEType
    {
        protected static int created = 0;

        [ETypeProperty("ID")] public string ID;

        [ETypeProperty("customAttributes.submissionType", RelatedEType = "_SubmissionType")]
        public string
            SubmissionType;

        [ETypeProperty("customAttributes.studyType", RelatedEType = "_ClickIRBStudyType", RelatedKeyName = "customAttributes.name")]
        public string StudyType;

        [ETypeProperty("customAttributes.irbSubmissionCustomExtension.customAttributes.studyfinderData.customAttributes.studyfinderIntent", RelatedEType = "_StudyfinderIntent", RelatedKeyName = "customAttributes.displayName")]
        public string StudyfinderIntent;

        [ETypeProperty("name")] public string Name;

        [ETypeProperty("description")]
        [ETypeProperty("customAttributes.studyTeamDescription")]
        public string
            Description;

        [ETypeProperty("company", RelatedEType = "Company")] public string Company;

        [ETypeProperty("customAttributes.IRB", RelatedEType = "_AdminOffice")] public string IRB;

        [ETypeProperty("status", RelatedEType = "ProjectStatus")] public string Status;

        [ETypeProperty("customAttributes.externalIRBInvolved")] public bool ExternalIRBInvolved;

        [ETypeProperty("createdBy", RelatedEType = "Person", RelatedKeyName = "userId")]
        [ETypeProperty("customAttributes.investigator.customAttributes.studyTeamMember", RelatedEType = "Person",
            RelatedKeyName = "userId")]
        [ETypeProperty("customAttributes.primaryContact.customAttributes.studyTeamMember", RelatedEType = "Person",
                RelatedKeyName = "userId")]
        public string InvestigatorUserID;

        [ETypeProperty("customAttributes.investigator.customAttributes.hasFinancialInterest")]
        public bool InvestigatorHasFinancialInterest;

        [ETypeProperty("owner", RelatedEType = "Person", RelatedKeyName = "userId")] public string Owner;

        [ETypeProperty("customAttributes.irbDetermination", RelatedEType = "_IRBDetermination")]
        public string
            DeterminationCode;

        [ETypeProperty("dateEnteredState")] public string DateEnteredState;

        [ETypeProperty("customAttributes.dateEndOfApproval")] public string DateEndOfApproval;

        [ETypeProperty("customAttributes.drugInvolved")] public bool DrugInvolved;

        [ETypeProperty("customAttributes.deviceInvolved")] public bool DeviceInvolved;

        [ETypeProperty("customAttributes.externalSitesPresent")] public bool ExternalSitesPresent;

        [ETypeProperty("customAttributes.primaryReviewer.customAttributes.reviewer", RelatedEType = "Person",
            RelatedKeyName = "userId")]
        public string PrimaryReviewer;

        [ETypeProperty("customAttributes.nonCommitteeReviewChecklist.customAttributes.isContinuingReviewRequired")]
        public bool IsContinuingReviewRequired;

        public List<String> ModificationTypes;
        public List<String> StudyTeamMemberIDs;
        public List<String> Contacts;
        public List<String> FundingSources;
        public List<String> AncillaryReviews;

        public string ResourceContainerTemplateID = "TMPL4CDAAA5987800";
        public string ResourceContainerParent = "CLICK_IRB_HOME";
        public string CreateView = "VIEW4CB6168E99000";

        public StudyWorkspace workspace;
        private IRBStudyForm _studyForm;
        protected int studyNumber;

        public IRBStudy()
            : base("_IRBSubmission")
        {
            // Defaults for a new study. These can be overridden by using a constructor
            // or object initializer.
            this.studyNumber = ++created;
            this.ID = "TEST-" + studyNumber;
            this.SubmissionType = "STUDY";
            this.Name = this.Description = "Test Study " + studyNumber;
            this.Company = "Test Company";
            this.IRB = "IRB 1";
            this.Status = "Approved";
            this.ExternalIRBInvolved = false;
            this.DrugInvolved = false;
            this.DeviceInvolved = false;
            this.ExternalSitesPresent = false;
            this.InvestigatorUserID = this.Owner = "administrator";
            this.InvestigatorHasFinancialInterest = false;
            this.StudyTeamMemberIDs = new List<string>();
            this.Contacts = new List<string>();
            this.FundingSources = new List<string>();
            this.AncillaryReviews = new List<string>();
            this.ModificationTypes = new List<string>();
            this.DateEnteredState = DateTime.Now.ToShortDateString();
            this.IsContinuingReviewRequired = false;
        }

        // Copy Constructor
        public IRBStudy(IRBStudy source)
            : this()
        {
            this.OID = source.OID;
            this.ID = source.ID;
            this.SubmissionType = source.SubmissionType;
            this.Name = source.Name;
            this.Description = source.Description;
            this.Company = source.Company;
            this.IRB = source.IRB;
            this.Status = source.Status;
            this.ExternalIRBInvolved = source.ExternalIRBInvolved;
            this.DrugInvolved = source.DrugInvolved;
            this.DeviceInvolved = source.DeviceInvolved;
            this.ExternalSitesPresent = source.ExternalSitesPresent;
            this.InvestigatorUserID = source.InvestigatorUserID;
            this.InvestigatorHasFinancialInterest = false;
            this.Owner = source.Owner;
            this.StudyTeamMemberIDs = new List<string>(source.StudyTeamMemberIDs);
            this.Contacts = new List<string>(source.Contacts);
            this.ModificationTypes = new List<string>(source.ModificationTypes);
            this.Workspace = new StudyWorkspace(this);
            this.IsContinuingReviewRequired = source.IsContinuingReviewRequired;
        }

        public IRBStudy(String ID)
            : this()
        {
            this.ID = ID;
        }

        public StudyWorkspace Workspace
        {
            get { return new StudyWorkspace(this); }
            set { this.workspace = value; }
        }

        public IRBStudyForm StudyForm
        {
            get
            {
                if (_studyForm == null)
                {
                    _studyForm = new IRBStudyForm(this);
                }
                return _studyForm;
            }
        }

        public override string CreateScript
        {
            get
            {
                string script = base.CreateScript;

                // Add dummy "Other Study Team Members" document
                script += @"
var doc = Document.createEntity();
etype.setQualifiedAttribute(""customAttributes.otherStudyTeamMembersInfo"", doc, ""add"");
var stm = EntityUtils.createEntitySet(""_StudyTeamMemberInfo"");
etype.setQualifiedAttribute(""customAttributes.studyTeamMembers"", stm);";

                script += @"etype.contacts = EntityUtils.createEntitySet(""Person"");";
                // Create and add study team members
                foreach (String stmID in this.StudyTeamMemberIDs)
                {
                    script += @"
var " + stmID + @" = getElements(""PersonForID"", ""ID"", """ + stmID + @""").item(1);
var stm" + stmID + @" = _StudyTeamMemberInfo.createEntity();
stm" + stmID + @".setQualifiedAttribute(""customAttributes.studyTeamMember"", " + stmID + @");
etype.setQualifiedAttribute(""customAttributes.studyTeamMembers"", stm" + stmID + @", ""add"");";
                }

                foreach (String contact in this.Contacts)
                {
                    script += @"
var " + contact + @" = getElements(""PersonForID"", ""ID"", """ + contact + @""").item(1);
etype.setQualifiedAttribute(""contacts"", " + contact + @", ""add"");";
                }
                if (this.ModificationTypes != null && this.ModificationTypes.Count > 0)
                {
                    int modTypeCt = 1;
                    foreach (String typeID in this.ModificationTypes)
                    {
                        script += @"
    var modType" + modTypeCt + @" = getResultSet(""_IRBModificationType"").query(""ID='" + typeID + @"'"").elements().item(1);
    etype.setQualifiedAttribute(""customAttributes.modificationDetails.customAttributes.modificationTypes"", modType" + modTypeCt + @",""add"");";
                        modTypeCt++;
                    }
                }

                // Create and add funding sources
                foreach (string source in this.FundingSources)
                {
                    string fsname = "fs" + FundingSources.Count;
                    script += @"
                        var " + fsname + @" = _FundingSource.createEntity();
                        var company = getElements(""CompanyForID"", ""ID"", """ + source + @""");
                        if (company.count() != 1) { throw new Error(""Incorrect number of entities found for Funding Source '" + source +
                              @"'.""); }
                        company = company.item(1);
                        " + fsname + @".setQualifiedAttribute(""customAttributes.organization"", getElements(""CompanyForID"", ""ID"", """ +
                              source + @""")(1));
                        etype.setQualifiedAttribute(""customAttributes.fundingSources"", " + fsname + @", ""add"");";
                }

                foreach (string ancillary in this.AncillaryReviews)
                {
                    string arname = "ar" + AncillaryReviews.Count;
                    script += @"
                        var arContainer = _ancillaryReview.createEntity();
                        var " + arname + @" = _AncillaryReviewSEL.createEntity();

                        var company = getElements(""CompanyForID"", ""ID"", """ + ancillary + @""");
                        if (company.count() != 1) { throw new Error(""Incorrect number of entities found for Ancillary Review '" + ancillary +
                              @"'.""); }
                        company = company.item(1);
                        " + arname + @".setQualifiedAttribute(""customAttributes.reviewOrganization"", getElements(""CompanyForID"", ""ID"", """ +
                              ancillary + @""")(1));
                        " + arname + @".setQualifiedAttribute(""customAttributes.required"", ""Yes"");

                        etype.setQualifiedAttribute(""customAttributes.ancillaryReviews"", arContainer, ""add"");
                        arContainer.setQualifiedAttribute(""customAttributes.ancillaryReviewSelection"", " + arname + @");";
                }

                script += @"
                    var rc = ResourceContainer.createFromResource(etype, getElements(""ContainerTemplateForID"", ""ID"", """ +
                          ResourceContainerTemplateID + @""")(1));
                    rc.setQualifiedAttribute(""parent"", getElements(""ContainerBaseForID"", ""ID"", """ + ResourceContainerParent +
                          @""")(1));
                    etype.setQualifiedAttribute(""resourceContainer"", rc);
                    _IRBSubmission.onStudyCreate(etype);";
                script += @"
                    etype.categories = EntityUtils.createEntitySet(""Category"");
                    etype.activities = EntityUtils.createEntitySet(""Activity"");
                    if( etype.getQualifiedAttribute(""customAttributes.irbSubmissionCustomExtension"") === null ) { etype.setQualifiedAttribute(""customAttributes.irbSubmissionCustomExtension"", _IRBSubmissionCustomExtension.createEntity()); }";

                return script;
            }
        }

        public override void CreateEntity(PortalDriver.PortalDriver Driver)
        {
            base.CreateEntity(Driver);
            FindWorkspace(Driver);
        }

        public override string DestroyScript
        {
            get
            {
                string script = @"var draft = wom.getEntityFromString(""" + this.OID +
                                @""").getQualifiedAttribute(""customAttributes.draftStudy"");
                    if (draft) {
                        draft.unregisterEntity();
                    }";
                script += base.DestroyScript;
                return script;
            }
        }

        public void FindWorkspace(PortalDriver.PortalDriver Driver)
        {
            this.Workspace = new StudyWorkspace(this);
        }

        [System.Obsolete("Use FindWorkspace instead.")]
        public void FindWorkspaceFromStudyList(IRBStudyList studylist)
        {
            studylist.NavigateAllSubmissions();
            studylist.Filter("ID", this.ID);
            string href = studylist.Results[0].href;
            this.Workspace = new StudyWorkspace(href.Substring(href.IndexOf("com.webridge.entity")));
        }

        public new String OID
        {
            get
            {
                if (base.OID == default(String))
                {
                    this.Workspace.Navigate();
                    base.OID = this.Workspace.projectOid;
                }
                return base.OID;
            }
            set { base.OID = value; }
        }

        public static IRBStudy FromCurrentWorkspace()
        {
            IRBStudy study = new IRBStudy();
            IWebDriver driver = PageController.driver;
            IWebElement JSContainer =
                driver.FindElement(By.XPath("//script[contains(text(), 'PortalTools.currentResource')]"));
            String script = JSContainer.GetAttribute("innerHTML");
            study.OID = new Regex(@"PortalTools\.currentResource = '(.*)'").Match(script).Groups[1].Value;
            study.ID = new Regex(@"PortalTools\.currentResourceID = '(.*)'").Match(script).Groups[1].Value;
            study.workspace =
                new StudyWorkspace(new Regex(@"PortalTools\.currentContainer = '(.*)'").Match(script).Groups[1].Value);
            return study;
        }

        public void AddToMeeting(Meeting meeting, IPortalDriver Driver)
        {
            String ai = meeting.AddAgendaItem(this, Driver);
            Driver.Store().Execute(@"var ai = wom.getEntityFromString(""" + ai + @""");
                var study = wom.getEntityFromString(""" + this.OID + @""");
                study.setQualifiedAttribute(""customAttributes.currentAgendaItem"", ai);");
        }

        public void setStudyTeamMemberAttributes(string studyTeamMemberID, List<KeyValuePair<string, string>> attributes,
                                                 IPortalDriver Driver)
        {
            string script = "";
            if ((studyTeamMemberID == null) || (attributes.Equals(null)))
            {
                return;
            }

            if (!this.StudyTeamMemberIDs.Contains(studyTeamMemberID))
            {
                this.StudyTeamMemberIDs.Add(studyTeamMemberID);
                Driver.Store()
                      .Execute(@"var " + studyTeamMemberID + @" = getElements(""PersonForID"", ""ID"", """ +
                            studyTeamMemberID + @""").item(1);
                            var study = wom.getEntityFromString(""" + this.OID + @""");
                            var stm" + studyTeamMemberID + @" = _StudyTeamMemberInfo.createEntity();
                            stm" + studyTeamMemberID + @".setQualifiedAttribute(""customAttributes.studyTeamMember"", " + studyTeamMemberID + @");
                            study.setQualifiedAttribute(""customAttributes.studyTeamMembers"", stm" + studyTeamMemberID + @", ""add"");
                            study.setQualifiedAttribute(""customAttributes.readers"", " + studyTeamMemberID + @", ""add"");");
            }

            script += @"var study = wom.getEntityFromString(""" + this.OID + @""");";

            foreach (KeyValuePair<string, string> attribute in attributes)
            {
                string id = StudyTeamMemberIDs[StudyTeamMemberIDs.IndexOf(studyTeamMemberID)];
                switch (attribute.Key)
                {
                    case "financial interest":
                        script +=
                            @"var studyTeamInfo = study.customAttributes.studyTeamMembers.query(""customAttributes.studyTeamMember.ID='" +
                            id + @"'"").elements().item(1);
                            studyTeamInfo.setQualifiedAttribute(""customAttributes.hasFinancialInterest"", " + attribute.Value + @");";
                        break;
                    case "involved in consent process":
                        script +=
                            @"var studyTeamInfo = study.customAttributes.studyTeamMembers.query(""customAttributes.studyTeamMember.ID='" +
                            id + @"'"").elements().item(1);
                            studyTeamInfo.setQualifiedAttribute(""customAttributes.involvedInConsentProcess"", " + attribute.Value + @");";
                        break;
                    case "role on study":
                        script += @"var studyTeamInfo = study.customAttributes.studyTeamMembers.query(""customAttributes.studyTeamMember.ID='" +
                            id + @"'"").elements().item(1);
                            var studyTeamMemberRole = getResultSet(""_studyTeamMemberRole"").query(""customAttributes.name = '" + attribute.Value + @"'"").elements()(1);
                            studyTeamInfo.setQualifiedAttribute(""customAttributes.rolesOnStudy"", studyTeamMemberRole, ""add"");";
                        break;
                }
            }

            Driver.Store().Execute(script);
        }

    }

    public class IRBStudyList : PageController
    {
        public struct Result
        {
            public String ID;
            public String Name;
            public DateTime Modified;
            public String State;
            public String PIFirstName;
            public String PILastName;
            public String Coordinator;
            public String SubmissionType;
            public String href;
        }

        private Dictionary<String, String> _ComponentIDTabs = new Dictionary<string, string> {
            { "In-Review", "3A410C7E87F5F749BC8AD015360F38AB" },
            { "Active", "82B4BE6B59C66A46A819B4E9728C157A" },
            { "Archived", "1100D1B8F2B5F049B6295B0FD8D259EB" },
            { "New Information Reports", "DFD614E03214774CBAF33E414CAA658B" },
            { "All Submissions", "AF7415D1BC8C8F419AB63042EC948113" }
        };

        private String _ComponentID;

        public IRBStudyList()
        {
            // See if we can get a ComponentID from the current page. Do nothing if not.
            try
            {
                this.FillComponentIDFromPage();
            }
            catch (NoSuchElementException)
            {
                ;
            }
        }

        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity%5BOID%5B4EDBFFB90C0F82469C15F12A7D167AD7%5D%5D");
            this._ComponentID = _ComponentIDTabs["In-Review"];
        }

        public void NavigateAllSubmissions()
        {
            this.Navigate();
            driver.Tab("All Submissions").Click();
            this._ComponentID = _ComponentIDTabs["All Submissions"];
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until((d) => { return d.FindElement(By.Id(_ComponentID + "_data")).GetAttribute("data-is-ready") == "true"; });
        }

        public String FilterBy
        {

            get
            {
                return findFilterByBox().SelectedOption.Text;
            }
            set
            {
                findFilterByBox().SelectByText(value);
            }
        }

        private SelectElement findFilterByBox()
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//select[contains(@id,'_queryField')]")));
            ReadOnlyCollection<IWebElement> potentials = driver.FindElements(By.XPath("//select[contains(@id,'_queryField')]"));
            SelectElement filterSelect = null;
            foreach (IWebElement el in potentials)
            {
                if (el.Displayed)
                {
                    filterSelect = new SelectElement(el);
                    break;
                }
            }

            if (filterSelect == null)
            {
                throw new NoSuchElementException();
            }
            return filterSelect;
        }
        public String FilterText
        {
            get
            {
                return findFilterTextField().GetAttribute("value");
            }
            set
            {
                IWebElement textField = findFilterTextField();

                new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until((_driver) => { return textField.Enabled; });
                textField.Clear();
                new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until((_driver) => { return textField.Enabled; });
                textField.SendKeys(value);
            }
        }

        private IWebElement findFilterTextField()
        {
            IWebElement textField = null;
            ReadOnlyCollection<IWebElement> potentials = driver.FindElements(By.XPath("//input[contains(@id,'_queryCriteria')]"));
            foreach (IWebElement el in potentials)
            {
                if (el.Displayed)
                {
                    textField = el;
                    break;
                }
            }

            if (textField == null)
            {
                throw new NoSuchElementException();
            }
            return textField;
        }

        public void Filter()
        {
            driver.FindElement(By.XPath("//input[contains(@id,'_requery')]")).Click();
            // Wait until the table has refreshed.
            if (driver.Store().portalVersionMajor == 8)
            {
                new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementExists(By.XPath("//div[@class='DRSV-DataArea ']")));
            }
            else
            {
                IWebElement tableBefore = driver.FindElement(By.XPath("//div[@id='component" + this._ComponentID + "']/table"));
                new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until((_driver) => { try { bool _enabled = tableBefore.Enabled; return false; } catch (StaleElementReferenceException) { return true; } });
            }
        }

        /// <summary>
        /// Convenience function to set all filter criteria at once.
        /// </summary>
        /// <param name="by">The text of the filter by box</param>
        /// <param name="token">The token to enter into the filter textfield</param>
        public void Filter(String by, String token)
        {
            this.FilterBy = by;
            this.FilterText = token;
            this.Filter();
        }

        public ReadOnlyCollection<Result> Results
        {
            get
            {
                List<Result> _results = new List<Result>();
                IWebElement table = null;
                IEnumerable rows = null;
                if (driver.Store().portalVersionMajor == 8)
                {
                    table = findVisibleDataArea();
                    rows = table.FindElements(By.TagName("tr"));
                }
                else
                {
                    table = driver.FindElement(By.XPath("//*[@id='component" + this._ComponentID + "']/table"));
                    rows = table.FindElements(By.TagName("tr")).Skip(1);
                }
                foreach (IWebElement row in rows)
                {
                    ReadOnlyCollection<IWebElement> cells = row.FindElements(By.TagName("td"));
                    if (cells[0].Text == @"No data to display.")
                    {
                        return new ReadOnlyCollection<Result>(_results);
                    }
                    
                    Result thisRow = new Result();
                    thisRow.href = cells[0].FindElement(By.TagName("a")).GetAttribute("href");
                    thisRow.ID = cells[1].Text;
                    thisRow.Name = cells[2].Text;
                    try
                    {
                        thisRow.Modified = DateTime.Parse(cells[3].Text);
                    }
                    catch (System.FormatException)
                    {
                        ;
                    }
                    thisRow.State = cells[4].Text;
                    thisRow.PIFirstName = cells[5].Text;
                    thisRow.PILastName = cells[6].Text;
                    thisRow.Coordinator = cells[7].Text;
                    thisRow.SubmissionType = cells[8].Text;
                    _results.Add(thisRow);
                }
                return new ReadOnlyCollection<Result>(_results);
            }
        }

        private IWebElement findVisibleDataArea()
        {
            ReadOnlyCollection<IWebElement> potentials = driver.FindElements(By.XPath("//div[@class='DRSV-DataArea ']"));
            foreach (IWebElement el in potentials)
            {
                if (el.Displayed)
                {
                    return el;
                }
            }
            throw new NoSuchElementException();
        }

        public void FillComponentIDFromPage()
        {
            IWebElement script = driver.FindElement(By.CssSelector("script:contains('drsv')"));
            string pattern = @"drsv_component['(\w+)'] = {};";
            Regex r = new Regex(pattern);
            Match m = r.Match(script.Text);
            this._ComponentID = m.Captures[1].Value;
            /*
            IWebElement hidden = driver.FindElement(By.XPath("//input[@name='_webrUpdateOID' and contains(@value,'com.webridge.entity.Entity')]"));
            String oid = hidden.GetAttribute("value");
            Regex re = new Regex(@"com\.webridge\.entity\.Entity\[OID\[([^\]]+)]]");
            this._ComponentID = re.Match(oid).Groups[1].Value;
            */
        }
    }

    public class StudyWorkspace : PageController
    {
        public String OID;
        private String _projectOid;

        public StudyWorkspace(IRBStudy study)
        {
            this._projectOid = study.ID;
        }

        public StudyWorkspace(String Oid)
        {
            this.OID = Oid;
        }

        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial", new Dictionary<string, string> { { "ProjectID", this._projectOid } });
        }

        public String Status
        {
            get
            {
                return driver.FindElement(By.XPath("//*[@id='__IRBSubmission.status.ID_container']")).Text;
            }
        }

        public void OpenActivity(String LinkText)
        {
            IWebElement anchor = driver.FindElement(By.LinkText(LinkText));
            driver.Window().New(anchor.Click);
        }

        public String projectOid
        {
            get
            {
                if (_projectOid == default(String))
                {
                    IWebElement scriptTag = driver.FindElement(By.XPath("//script[contains(.,'PortalTools.currentResource')]"));
                    String script = scriptTag.GetAttribute("innerHTML");
                    Regex regex = new Regex(@"PortalTools.currentResource = '(com.webridge.entity.Entity\[OID\[[0-9A-F]{32}\]\])';");
                    Match m = regex.Match(script);
                    if (!m.Success)
                    {
                        throw new Exception("Problem finding study OID.");
                    }
                    _projectOid = m.Groups[1].Value;
                }

                return _projectOid;
            }
        }

        public SubmitDesignatedReviewActivity SubmitDesignatedReviewActivity()
        {
            OpenActivity("Submit Designated Review");
            return new SubmitDesignatedReviewActivity();
        }

        public PrepareLetterActivity PrepareLetterActivity()
        {
            OpenActivity("Prepare Letter");
            return new PrepareLetterActivity();
        }

        public AssignCoordinatorActivity AssignCoordinatorActivity()
        {
            OpenActivity("Assign Coordinator");
            return new AssignCoordinatorActivity();
        }

        public SendLetterActivity SendLetterActivity()
        {
            OpenActivity("Send Letter");
            return new SendLetterActivity();
        }

        public SubmitActivity SubmitActivity()
        {
            OpenActivity("Submit");
            return new SubmitActivity();
        }
    }

    public class IRBStudyForm
    {
        [Obsolete("Using individual StudyFormPage classes has been deprecated, please use study.StudyForm.Page(<PageName>) instead.")]
        public IRBStudyBasicInformationForm BasicInformationForm;

        [Obsolete("Using individual StudyFormPage classes has been deprecated, please use study.StudyForm.Page(<PageName>) instead.")]
        public IRBStudyFundingSourcesForm FundingSourcesForm;

        [Obsolete("Using individual StudyFormPage classes has been deprecated, please use study.StudyForm.Page(<PageName>) instead.")]
        public IRBStudyExternalIRBForm ExternalIRBForm;

        [Obsolete("Using individual StudyFormPage classes has been deprecated, please use study.StudyForm.Page(<PageName>) instead.")]
        public IRBStudyLocalSiteDocuments LocalSiteDocuments;

        [Obsolete("Using individual StudyFormPage classes has been deprecated, please use study.StudyForm.Page(<PageName>) instead.")]
        public IRBStudySupportingDocuments SupportingDocuments;

        [Obsolete("Using individual StudyFormPage classes has been deprecated, please use study.StudyForm.Page(<PageName>) instead.")]
        public IRBStudyFinalPage FinalPage;

        [Obsolete("Using individual StudyFormPage classes has been deprecated, please use study.StudyForm.Page(<PageName>) instead.")]
        public IRBStudyTeamMembersForm StudyTeamMembersForm;

        [Obsolete("Using individual StudyFormPage classes has been deprecated, please use study.StudyForm.Page(<PageName>) instead.")]
        public IRBStudyScopeForm StudyScopeForm;

        [Obsolete("Using individual StudyFormPage classes has been deprecated, please use study.StudyForm.Page(<PageName>) instead.")]
        public IRBStudyDrugsForm DrugsForm;
        
        [Obsolete("Using individual StudyFormPage classes has been deprecated, please use study.StudyForm.Page(<PageName>) instead.")]
        public IRBStudyDevicesForm DevicesForm;

        public IRBStudy Study;

        public IRBStudyForm(IRBStudy study)
        {
// Selectively disable Obsolete warnings
#pragma warning disable 618
            Study = study;
            BasicInformationForm = new IRBStudyBasicInformationForm(study);
            FundingSourcesForm = new IRBStudyFundingSourcesForm(study);
            DrugsForm = new IRBStudyDrugsForm(study);
            DevicesForm = new IRBStudyDevicesForm(study);
            StudyTeamMembersForm = new IRBStudyTeamMembersForm(study);
            StudyScopeForm = new IRBStudyScopeForm(study);
            ExternalIRBForm = new IRBStudyExternalIRBForm(study);
            LocalSiteDocuments = new IRBStudyLocalSiteDocuments(study);
            SupportingDocuments = new IRBStudySupportingDocuments(study);
            FinalPage = new IRBStudyFinalPage(study);
#pragma warning restore 618
        }

        public IRBStudyFormPage Page(string name)
        {
            switch (name)
            {
                case "Basic Information":
                    return new IRBStudyBasicInformationForm(Study);

                case "Local Site Documents":
                    return new IRBStudyLocalSiteDocuments(Study);

                case "Supporting Documents":
                    return new IRBStudySupportingDocuments(Study);

                case "Final Page":
                    return new IRBStudyFinalPage(Study);
            }

            return new IRBStudyFormPage(Study, name);
        }
    }

    public class IRBStudyFormPage : PageController
    {
        protected IRBStudy _study;
        private string _name;
        private string _wizardPageOID;

        public IRBStudyFormPage(IRBStudy study)
        {
            _study = study;
        }

        public IRBStudyFormPage(IRBStudy study, string name)
        {
            _study = study;
            _name = name;
        }

        public bool isEditable()
        {
            return driver.FindElements(By.Id("lnkSaveProjectEditor")).Count > 0;
        }

        public IWebElement SaveLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Save"));
            }
        }

        public override void Navigate()
        {
            // Fetch oid from settings CDT using a Command Console script; Keeping this here instead of constructor for lazy loading
            if (_wizardPageOID == null)
            {
                string script = @"?getResultSet(""_IRBSettingsCustomSmartformStep"").query(""customAttributes.stepName='";
                script += _name;
                script += @"'"").elements().item(1).getQualifiedAttribute(""customAttributes.wizardPage"");";
                _wizardPageOID = driver.Store().Execute(script);

                // Verify the result is a valid OID
                if (!new Regex(@"com.webridge.entity.Entity\[OID\[[A-Z0-9]{32}]]").Match(_wizardPageOID).Success)
                {
                    throw new Exception("Attempt to navigate to an invalid smartform page: " + _name);
                }
            }
            if (_study.OID == _study.ID)
            {
                string script = @"?getResultSet(""_IRBSubmission"").query(""ID='";
                script += _study.ID;
                script += @"'"").elements().item(1);";
                _study.OID = driver.Store().Execute(script);
            }
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _study.OID + "&Mode=smartform&WizardPageOID=" + _wizardPageOID);
        }

        public void uploadDocument(String fullPropertyPath, String filePath, String category = null)
        {
            //The Add button for Attach the protocol on Basic Information
            IWebElement protocolAddButton = driver.FindElement(By.XPath("//*[@id='_" + fullPropertyPath + "_container']//button[contains(@id,'_addBtn')]"));
            AttachmentUploadForm popup = new AttachmentUploadForm(protocolAddButton);
            popup.UploadAttachment(filePath, category);

            this.SaveLink.Click();
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.LinkText("Save")));
        }
    }

    public class IRBStudyBasicInformationForm : IRBStudyFormPage
    {
        public IRBStudyBasicInformationForm(IRBStudy study) :
            base(study, "Basic Information")
        {
            ;
        }

        public void UploadProtocol(string filepath)
        {
            uploadDocument("_IRBSubmission.customAttributes.irbProtocolAttachments", filepath);
        }
                
    }

    public class IRBStudyLocalSiteDocuments : IRBStudyFormPage
    {
        public IRBStudyLocalSiteDocuments(IRBStudy study) :
            base(study, "Local Site Documents")
        {
            ;
        }

        public void UploadConsentForm(string filepath)
        {
            uploadDocument("_IRBSubmission.customAttributes.recruitmentDetails.customAttributes.recruitmentAttachments", filepath);
        }
        public void UploadSupportingDocument(string filepath)
        {
            uploadDocument("_IRBSubmission.customAttributes.attachments", filepath, "Other");
        }
    }

    public class IRBStudySupportingDocuments : IRBStudyFormPage
    {
        public IRBStudySupportingDocuments(IRBStudy study) :
            base(study, "Supporting Documents")
        {
            ;
        }

        public void UploadSupportingDocument(string filepath)
        {
            uploadDocument("_IRBSubmission.customAttributes.attachments", filepath, "Other");
        }
    }

    public class IRBStudyFinalPage : IRBStudyFormPage
    {
        public IRBStudyFinalPage(IRBStudy study) :
            base(study, "Final Page")
        {
            ;
        }

        public void FinishPage()
        {
            IWebElement anchor = driver.FindElement(By.XPath("//*[@id='finish_btn_Top']"));
            anchor.Click();
        }
    }

    public class IRBStudyFundingSourcesForm : IRBStudyFormPage 
    {

        public IRBStudyFundingSourcesForm(IRBStudy study) :
            base(study, "Funding Sources")
        {
            ;
        }
    }

    public class IRBStudyExternalIRBForm : IRBStudyFormPage
    {
        public IRBStudyExternalIRBForm(IRBStudy study) :
            base(study, "External IRB")
        {
            ;
        }
    }

    public class IRBStudyTeamMembersForm : IRBStudyFormPage
    {

        public IRBStudyTeamMembersForm(IRBStudy study) :
            base(study, "Study Team Members")
        {
            ;
        }
    }

    public class IRBStudyScopeForm : IRBStudyFormPage
    {
        public IRBStudyScopeForm(IRBStudy study)
            : base(study, "Study Scope")
        {
            ;
        }
    }

    public class IRBStudyDrugsForm : IRBStudyFormPage
    {
        public IRBStudyDrugsForm(IRBStudy study)
            : base(study, "Drugs")
        {
            ;
        }
    }


    public class IRBStudyDevicesForm : IRBStudyFormPage
    {
        public IRBStudyDevicesForm(IRBStudy study)
            : base(study, "Devices")
        {
            ;
        }
    }
}