﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using ClickPortal;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class SelectPersonForm : PageController
    {
        public SelectPersonForm(IWebElement launcher)
        {
            this.OpenPopup(launcher, "Select Person");
        }

        public void searchAndSelectPerson(String userInfo, String category = null)
        {
            IWebElement userField = new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists(By.Name("_webrRSV_FilterValue_0_0")));
            userField.SendKeys(userInfo);

            if (this.HasCategoryField())
            {
                if (category == null)
                {
                    throw new Exception("No category passed and this attachment form requires it.");
                }

                this.CategorySelect.SelectByText(category);
            }
            else
            {
                if (category != null)
                {
                    throw new Exception("Category passed but category field does not exist.");
                }
            }

            this.GoButton.Click();
            this.selectUser();
        }

        private void selectUser()
        {
            try
            {
                IWebElement radioButton = new WebDriverWait(driver, TimeSpan.FromSeconds(3)).Until(ExpectedConditions.ElementExists(By.XPath("//input[@id='webrRSV__SelectedItem_0']")));
                radioButton.Click();
            }
            catch (Exception)
            {
                try {
                    // After Portal 8 Upgrade, we need to click the label instead of radio button
                    IWebElement radioButton = new WebDriverWait(driver, TimeSpan.FromSeconds(3)).Until(ExpectedConditions.ElementExists(By.XPath("//input[@name='webrRSV__SelectedItem_0']")));
                    string labelID = radioButton.GetAttribute("id");
                    IWebElement label = new WebDriverWait(driver, TimeSpan.FromSeconds(3)).Until(ExpectedConditions.ElementExists(By.XPath("//label[@for='" + labelID + "']")));
                    label.Click();
                }
                catch (Exception)
                {
                    throw new Exception("Couldn't find the user or more than one result.");
                }

            }
            this.OKButton.Click();
            this.FocusRootWindow();
        }

        public IWebElement GoButton
        {
            get
            {
                return driver.FindElement(By.XPath("//input[@value='Go']"));
            }
        }

        public IWebElement OKButton
        {
            get
            {
                return driver.FindElement(By.XPath("//input[@id='btnOk']"));
            }
        }

        public SelectElement CategorySelect
        {
            get
            {
                return new SelectElement(driver.FindElement(By.Name("_webrRSV_FilterField_0_0")));
            }
        }

        public Boolean HasCategoryField()
        {
            return driver.FindElements(By.Name("_webrRSV_FilterField_0_0")).Count > 0;
        }
    }
}
