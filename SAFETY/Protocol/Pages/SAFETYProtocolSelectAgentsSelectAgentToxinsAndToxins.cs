﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.SAFETY.Protocol.Pages
{
    public class SAFETYProtocolSelectAgentsSelectAgentToxinsAndToxins : SAFETYProtocolFormPage
    {
        public SAFETYProtocolSelectAgentsSelectAgentToxinsAndToxins(SAFETYProtocol protocol)
            : base(protocol)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[3EFC80D9FB6493498500FB760404A9B6]]");
        }
    }
}
