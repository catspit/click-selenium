﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class IACUCSubmissionsList : PageController
    {
        public struct Result
        {
            public String ID;
            public String Name;
            public DateTime Modified;
            public String State;
            public String PIFirstName;
            public String PILastName;
            public String Coordinator;
            public String SubmissionType;
            public String href;
        }

        protected Dictionary<String, String> _ComponentIDTabs = new Dictionary<string, string> {
            { "In-Review", "6A030BB4B77F4140B1A15362AABA1E72" },
            { "Active", "ACC04BEFECBC3B499D39F15E79BDF4FE" },
            { "Archived", "383CF54215D34048B8FC9264F9BE489E" },
            { "Research Teams", "F70C2AD52E4E074CAC948AC45F8DE36E" },
            { "All Submissions", "4BA62024807DCF4A8D5821E2F835F26B" }
        };

        protected String _ComponentID;

        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity%5BOID%5BEB62E5BFA0D684419E3177BE34033D9D%5D%5D");
            this._ComponentID = _ComponentIDTabs["In-Review"];
        }

        public void NavigateAllSubmissions()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[9EB5178212175C439C7FFDF200BA9EFE]]&Tab2=com.webridge.entity.Entity[OID[4BA62024807DCF4A8D5821E2F835F26B]]");
            this._ComponentID = _ComponentIDTabs["All Submissions"];
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.XPath("//table[@class='TabComponentBodyTable']")));
        }

        public virtual void NavigateInReview()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[9EB5178212175C439C7FFDF200BA9EFE]]&Tab2=com.webridge.entity.Entity[OID[6A030BB4B77F4140B1A15362AABA1E72]]");
            this._ComponentID = _ComponentIDTabs["In-Review"];
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.XPath("//table[@class='TabComponentBodyTable']")));
        }

        public virtual void NavigateActive()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[B5844FAE149A2E47A9BEE0B3EEAA7570]]&Tab2=com.webridge.entity.Entity[OID[A7BAEE9FA050B345A52D8A330CFB8AC6]]");
            this._ComponentID = _ComponentIDTabs["Active"];
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.XPath("//table[@class='TabComponentBodyTable']")));
        }

        public String FilterBy
        {
            get
            {
                SelectElement filterSelect = new SelectElement(driver.FindElement(By.Id("component" + this._ComponentID + "_queryField1")));
                return filterSelect.SelectedOption.Text;
            }
            set
            {
                SelectElement filterSelect = new SelectElement(driver.FindElement(By.Id("component" + this._ComponentID + "_queryField1")));
                filterSelect.SelectByText(value);
            }
        }

        public String FilterText
        {
            get
            {
                IWebElement textField = driver.FindElement(By.Id("component" + this._ComponentID + "_queryCriteria1"));
                return textField.GetAttribute("value");
            }
            set
            {
                IWebElement textField = driver.FindElement(By.Id("component" + this._ComponentID + "_queryCriteria1"));
                new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until((_driver) => { return textField.Enabled; });
                textField.Clear();
                new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until((_driver) => { return textField.Enabled; });
                textField.SendKeys(value);
            }
        }

        public void Filter()
        {
            IWebElement tableBefore = driver.FindElement(By.XPath("//div[@id='component" + this._ComponentID + "']/table"));
            driver.FindElement(By.Id("component" + this._ComponentID + "_requery")).Click();
            // Wait until the table has refreshed.
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until((_driver) => { try { bool _enabled = tableBefore.Enabled; return false; } catch (StaleElementReferenceException) { return true; } });
        }

        /// <summary>
        /// Convenience function to set all filter criteria at once.
        /// </summary>
        /// <param name="by">The text of the filter by box</param>
        /// <param name="token">The token to enter into the filter textfield</param>
        public void Filter(String by, String token)
        {
            this.FilterBy = by;
            this.FilterText = token;
            this.Filter();
        }

        public ReadOnlyCollection<Result> Results
        {
            get
            {
                List<Result> _results = new List<Result>();
                IWebElement table = driver.FindElement(By.XPath("//*[@id='component" + this._ComponentID + "']/table"));
                ReadOnlyCollection<IWebElement> rows = table.FindElements(By.TagName("tr"));
                bool header = true;
                foreach (IWebElement row in rows)
                {
                    if (header)
                    {
                        header = false;
                    }
                    else
                    {
                        ReadOnlyCollection<IWebElement> cells = row.FindElements(By.TagName("td"));

                        Result thisRow = new Result();
                        thisRow.href = cells[0].FindElement(By.TagName("a")).GetAttribute("href");
                        thisRow.ID = cells[1].Text;
                        thisRow.Name = cells[2].Text;
                        try
                        {
                            thisRow.Modified = DateTime.Parse(cells[3].Text);
                        }
                        catch (System.FormatException)
                        {
                            ;
                        }
                        thisRow.State = cells[4].Text;
                        thisRow.PIFirstName = cells[5].Text;
                        thisRow.PILastName = cells[6].Text;
                        thisRow.Coordinator = cells[7].Text;
                        thisRow.SubmissionType = cells[8].Text;
                        _results.Add(thisRow);
                    }
                }
                return new ReadOnlyCollection<Result>(_results);
            }
        }

        public void FillComponentIDFromPage()
        {
            IWebElement hidden = driver.FindElement(By.XPath("//input[@name='_webrUpdateOID' and contains(@value,'com.webridge.entity.Entity')]"));
            String oid = hidden.GetAttribute("value");
            Regex re = new Regex(@"com\.webridge\.entity\.Entity\[OID\[([^\]]+)]]");
            this._ComponentID = re.Match(oid).Groups[1].Value;
        }
    }
}
