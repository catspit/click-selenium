﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Procedure.Pages
{
    public class IACUCProcedureProlongedPhysicalRestraintForm : IACUCProcedureFormPage
    {
        public IACUCProcedureProlongedPhysicalRestraintForm(IACUCProcedure procedure) : base(procedure)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _procedure.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[E3D688683623C844906D53336FEBAD7A]]");
        }
    }
}
