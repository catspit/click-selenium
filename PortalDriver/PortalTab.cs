﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ClickPortal.PortalDriver
{
    public class PortalTab : IPortalTab
    {
        private readonly string _tabName;
        private readonly By _searchBy;
        private readonly ISearchContext _context;
        // Returns the NavigationOverflowLink element if it exists & visble ('...' button)
        // Returns null if the link is not visible
        public IWebElement NavigationOverflowLink { //... link
            get {
                var currentContext = this._context;
                try
                {
                    // Get current tab element and use as currentContext
                    currentContext = currentContext.FindElement(_searchBy);
                }
                catch(Exception)
                {
                    // current tab not available
                }
                IWebElement expandLink = null;
                try
                {
                    expandLink = currentContext.FindElement(By.XPath(".//ancestor::nav//a[@class='NavigationOverflowLink']"));
                }
                catch(Exception)
                {
                    //more button [...] is not available
                }
                // Return null if button is invisible
                if (expandLink != null && expandLink.Displayed!=true)
                {
                    expandLink = null;
                }
                return expandLink;
            }
        }

        public PortalTab(By searchBy, ISearchContext context)
        {
            _searchBy = searchBy;
            _context = context;
        }

        public PortalTab(By searchBy, string tabName, ISearchContext context)
        {
            _tabName = tabName;
            _searchBy = searchBy;
            _context = context;
        }

        public bool Is()
        {
            return this._context.FindElements(_searchBy).Count != 0;
        }
        
        public void Click()
        {
            var driver = (IPortalDriver)this._context;
            IWebElement tabLink = driver.FindElement(_searchBy);
            /**
             * ORISIRB-6953 AUTOMATED TESTS: Failing Test CATS5815 (Workspace Review level and risk label)
             * Tab link may be hidden if browser window is too small
             * We need to click the [...] link to expand hidden tab links
             */
            if (tabLink.Displayed != true)
            {
                var expandLink = this.NavigationOverflowLink;
                if (expandLink != null)
                {
                    // Need to scroll to the expanded link before clicking.
                    ((IJavaScriptExecutor)driver).ExecuteScript(string.Format("window.scrollTo({0},{1});", expandLink.Location.X, expandLink.Location.Y));
                    expandLink.Click();
                    string hiddenTabLinkId = tabLink.GetAttribute("id") + "_overflow";
                    tabLink = driver.FindElement(By.Id(hiddenTabLinkId));
                }
            }
            tabLink.Click();
        }

        public IWebElement GetElement() {
            return this._context.FindElement(_searchBy);
        }

        public bool IsVisible()
        {
            return this._context.FindElement(_searchBy).Displayed;
        }
    }
}
