﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver
{
    class PortalControl : IPortalControl {
        protected static IControlFactoryFromHelper[] controlTypes = { 
                                                             new ControlViaHelperBoolean(),
                                                             new ControlViaHelperString(),
                                                             new ControlViaHelperSingleEntity(),
                                                             new ControlViaHelperChooser(),
                                                             new ControlViaHelperEntityCheckbox(),
                                                             new ControlViaHelperEntityTable(),
                                                             new ControlViaHelperMultiEntity(),
                                                             new ControlViaHelperReadOnly()
                                                          };
        internal ISearchContext context;
        internal IJavaScriptExecutor executor;
        internal IPortalDriver driver;
        internal By @by;
        internal String locator;
        internal IPortalControl cachedControl;
        internal IWebElement container;
        internal IWebElement element; //Located element
        public IWebElement Container {
            get {
                if (this.container == null)
                {
                    this.container = findContainer();
                }
                return this.container;
            }
        }

        public PortalControl(IPortalDriver driver, By @by )
        {
            this.by = by;
            this.context  = driver;
            this.driver = driver;
            this.executor = (IJavaScriptExecutor) context;
        }


        public PortalControl(IPortalDriver driver, String locator)
        {
            this.locator = locator;
            this.context = driver;
            this.driver = driver;
            this.executor = (IJavaScriptExecutor)context;
        }
        private IWebElement getElement()
        {
            if (this.element == null)
            {
                if (this.locator != null)
                {
                    try
                    {
                        this.by = PortalBy.ControlAttribute(locator);
                        this.element = new WebDriverWait(this.driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementToBeClickable(this.by));
                    }
                    catch
                    {
                        this.by = PortalBy.ControlCaptionText(locator);
                        this.element = new WebDriverWait(this.driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementToBeClickable(this.by));
                    }
                }
            }
            return this.element;
        }
        private IWebElement findContainer()
        {
            var elem = getElement();

            var isContainerElement = elem != null && elem.GetAttribute("id").EndsWith("_container") == true;
            if (isContainerElement == true)
            {
                return elem;
            }
            //Find container element from ancestors
            var container = elem.FindElement(By.XPath("ancestor::*[contains(@id,'_container')]"));
            return container;
        }

        private IPortalControl findControl()
        {
            if (this.cachedControl == null)
            {
                var elem = getElement();
                var helper = new ControlHelper(this.context, elem);

                this.cachedControl = PortalControl.controlTypes
                    .Select(factory => factory.create(helper, this.driver))
                    .FirstOrDefault(control => control != null);
            }

            return this.cachedControl;
        }

        private IWebElement findControlCaptionElement()
        {
            IWebElement elem = null;
            if (this.locator != null)
            {
                try
                {
                    this.by = PortalBy.ControlAttribute(locator);
                    elem = this.context.FindElement(this.by);
                }
                catch
                {
                    this.by = PortalBy.ControlCaptionText(locator);
                    elem = this.context.FindElement(this.by);
                }
            }

            return elem;
        }

        private IWebElement findControlHelpBubble()
        {
            IWebElement elem = null;
            try
            {
                // Portal 8
                elem = findControlCaptionElement().FindElement(By.XPath("//i[@helpurl]"));
            }
            catch (NoSuchElementException)
            {
                // Portal 6
                elem = findControlCaptionElement().FindElement(By.XPath("//img[@helpurl]"));
            }

            return elem;
        }

        public bool Is(IPortalControlValue value)
        {
            return this.findControl().Is(value);
        }

        public bool Is(string textOrOID)
        {
            return this.findControl().Is(textOrOID);
        }

        public string OID
        {
            get { return this.findControl().OID; }
        }

        public string Text
        {
            get
            {
                var control = this.findControl();
                if (control!=null) return this.findControl().Text;
                return null;
            }
        }

        public string Caption
        {
            get
            {
                IWebElement elem = findControlCaptionElement();
                if (elem != null)
                {
                    return elem.Text;
                }

                return null;
            }
        }

        public bool HasHelpBubble
        {
            get
            {
                IWebElement elem = findControlHelpBubble();
                if (elem != null)
                {
                    return !String.IsNullOrEmpty(elem.GetAttribute("helpurl"));
                }

                return false;
            }
        }

        public bool IsClear
        {
            get { return this.findControl().IsClear; }
        }

        public IPortalControl Control
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerator<IPortalControlValue> GetEnumerator()
        {
            return this.findControl().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.findControl().GetEnumerator();
        }

        public IPortalControlValue this[int index]
        {
            get { return this.findControl()[index]; ; }
        }

        public int Count
        {
            get { return this.findControl().Count;; }
        }

        public bool Is()
        {
            try
            {
                return this.findControl().Is();
            }
            catch
            {
                return false;
            }
        }

        public IPortalControlValue Clear()
        {
            this.findControl().Clear();
            return this.Val();
        }

        public IPortalControlValue Val()
        {
            return this.findControl().Val();
        }

        public IPortalControlValue Val(string TextOrOID)
        {
            this.findControl().Val(TextOrOID);
            return this.Val();
        }

        public IPortalControlValue Val(DateTime date)
        {
            this.findControl().Val(date);
            return this.Val();
        }

        public string Type
        {
            get
            {
                var control = this.findControl();
                if (control != null) return this.findControl().Type;
                return "undefined";
            }
        }
        public IWebElement AddButton
        {
            get
            {
                return this.findControl().AddButton;
            }
        }
        public IWebElement SelectButton
        {
            get
            {
                return this.findControl().SelectButton;
            }
        }

    }

    internal interface IControlFactoryFromHelper
    {
        IPortalControl create(ControlHelper helper, IPortalDriver driver);
    }

    internal interface IControlFactory {
        IPortalControl create(ReadOnlyCollection<IWebElement> elements, ISearchContext context);
    }

}



//GRAVEYARD
/*
public string FindElementContainingPortalAttribute()
{
    return PortalControl.FindElementContainingPortalAttribute(this.context, this.by);
}

public ReadOnlyCollection<IWebElement> FindElementsByPortalAttribute()
{
    return PortalControl.FindElementsByPortalAttribute(this.context, this.by);
}
*/



/*
internal static IWebElement FindElementContainingPortalAttribute(ISearchContext context, By by)
{
    var elements = context.FindElements(by);
    if (elements.Count == 0) return null;

    var ancestorContainerPath = By.XPath("ancestor::*[@id[contains(.,'_container')]]");
    var ancestorContainer = elements[0].FindElements(ancestorContainerPath);
    if (ancestorContainer.Count == 0) return null;

    return ancestorContainer[0];

    //             var xpath = "//span[@id='_span_ALD_caption'][.//text()[contains(.,'Comment')]]";
    //xpath = "//*[contains(@*,'TrainingCompleted')]";
    //var a = MockPortalControl.portalDriver.FindElements(By.XPath(xpath));
    //var container = "ancestor::*[@id[contains(.,'_container')]]";
    //var b = a[0].FindElement(By.XPath(container));
    //var c = b.GetAttribute("id");
}

*/



/*
internal static ReadOnlyCollection<IWebElement> FindElementsByPortalAttribute(ISearchContext context, String attribute)
{
    var attributeTemplateCondition =
        "//*[id='+' or @name='+' or @name='_webrRequired_+' or @name='_webrDocContentDelete_+' or @name='_webrDocContentNewFile_+' or " +
        " @name='+_webrSet' or @name='+_setItemAdd' or @name='+_setItemDelete' or @name='+_setItem' or @name='+_webrHidden' or " +
        "@id='_+_container' or @id='+_clientSideError']";


    var elems = context.FindElements(By.XPath(attributeTemplateCondition.Replace("+",attribute)));
            

    //var conditionContains = "(contains(@name,'" + attribute + "') or contains(@id,'" + attribute + "'))";
    //var conditionLastmost = "(not(contains(substring-after(@name,'" + attribute + "'),'.')) and not(contains(substring-after(@id,'" + attribute + "'),'.')) )";
    //var conditionLastmost = "(not(contains(substring-after(@name,'" + attribute + "'),'.')) and not(contains(substring-after(@id,'" + attribute + "'),'.')) )";
    //var elems = context.FindElements(By.XPath("//*["+ conditionContains+ " and "+ conditionLastmost +" ]"));

            
    if (elems.Count == 0) return null;
    return elems;
}
 */




//var elems = PortalControl.FindElementsByPortalAttribute(context, attribute);

//if (elems.Count == 0) return null;

/*
             var found = this.findControl();
    if (found == null) throw new ComponentNotFoundException();
    return found.Val();
 * *
*/
/*
        internal static string AttributeFromElement(ISearchContext context, By by) {
            var elements = context.FindElements(by);
            if (elements.Count == 0) return null;
            var ancestorContainerPath = By.XPath("ancestor::*[contains(@id,'_container')]"); // @id "_container"
            var possibleContainers = elements[0].FindElements(ancestorContainerPath); //old xpath 1.0 has no "ends-with"
            var containerElement = possibleContainers
                .FirstOrDefault(elem => containerEnding.IsMatch(elem.GetAttribute("id")) && elem.GetAttribute("id").StartsWith("_"));
            
            if (containerElement == null) return null;

            var attr = containerEnding.Replace(containerElement.GetAttribute("id"), "").Substring(1);
            return attr;

            /
            var ancestorContainerPath = By.XPath("ancestor::*[substring-after(@id,'_containe')='r']"); // @id ends-with "_container"
            var ancestorContainer = elements[0].FindElements(ancestorContainerPath);
            if (ancestorContainer.Count == 0) return null;
            var containerID = ancestorContainer[0].GetAttribute("id");
            return containerID.Replace("_container", "");
             /
        }


    */