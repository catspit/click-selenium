﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ClickPortal.Pages;
using OpenQA.Selenium;

namespace ClickPortal.IRB
{
    public class GeneralSettingsPage : PageController
    {
        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity%5BOID%5BC5F69EF8889104468A0C9DD9DADF2465%5D%5D");
        }

        public bool AutoAssignCoordCheckbox
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.assignCoordinatorToNewMods")).Selected; }
        }

        public bool CoordinatorCanEditCheckbox
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.enableCoordinatorToEditStudy")).Selected; }
        }

        public bool PIProxiesCheckbox
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.piProxy")).Selected; }
        }

        public bool VAResearchCheckbox
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.vaSupport")).Selected; }
        }

        public bool PICanSelectIRBCheckbox
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.showIRBOffice")).Selected; }
        }

        public bool CoordCanChangeOfficeCheckbox
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.showAssignIRB")).Selected; }
        }

        public String SSRSReportServerURL
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.reportServerUrl")).Text; }
        }

        public bool LockSystemCheckbox
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.lockIRB")).Selected; }
        }

        public string CommonRuleEffectiveDate
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.commonRuleEffectiveDate")).GetAttribute("value"); }
        }
    }

    public class SmartformStepsSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity%5BOID%5BC5F69EF8889104468A0C9DD9DADF2465%5D%5D");
            IWebElement tab = driver.FindElement(By.XPath("//a[contains(@href, '#SmartFormStepsTab')]"));
            tab.Click();
        }

        public IWebElement CustomStepsTable
        {
            get { return driver.FindElement(By.Id("_webrRSV_DIV_0")).FindElement(By.TagName("table")); }
        }

        public IWebElement CustomViewsTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='SmartFormStepsTab']/table[3]")); }
        }
    }

    public class NotificationSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity%5BOID%5BC5F69EF8889104468A0C9DD9DADF2465%5D%5D");
            IWebElement tab = driver.FindElement(By.XPath("//a[contains(@href, '#NMTemplatesTab')]"));
            tab.Click();
        }

        public String FromAddressText
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.notificationFromAddress")).GetAttribute("value"); }
        }

        public String ReplyToAddressText
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.notificationReplyToAddress")).GetAttribute("value"); }
        }

        public String ReminderDays
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.responseTimeExceededThreshold")).GetAttribute("value"); }
        }
        
        public IWebElement CustomNotificationTemplatesTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='NMTemplatesTab']/table[2]/tbody")); }
        }
    }

    public class WorkspaceTemplateSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity%5BOID%5BC5F69EF8889104468A0C9DD9DADF2465%5D%5D");
            IWebElement tab = driver.FindElement(By.XPath("//a[contains(@href, '#WSTemplatesTab')]"));
            tab.Click();
        }

        public IWebElement StudyReviewTemplateDropdown
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.templateStudyReview")); }
        }

        public IWebElement StudyCompleteTemplateDropdown
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.templateStudyComplete")); }
        }

        public IWebElement StudyArchivedTemplateDropdown
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.templateStudyArchived")); }
        }

        public IWebElement StudyExternalIRBTemplateDropdown
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.templateStudyExternalIRB")); }
        }

        public IWebElement MODCRReviewTemplateDropdown
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.templateMODCRReview")); }
        }

        public IWebElement MODCRCompleteTemplateDropdown
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.templateMODCRReview")); }
        }

        public IWebElement RNIReviewTemplateDropdown
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.templateRniReview")); }
        }

        public IWebElement RNICompleteTemplateDropdown
        {
            get { return driver.FindElement(By.Name("_IRBSettings.customAttributes.templateRniComplete")); }
        }

        public ReadOnlyCollection<IWebElement> TemplateDropdowns()
        {
            return new ReadOnlyCollection<IWebElement>( new List<IWebElement> {
                StudyReviewTemplateDropdown,
                StudyCompleteTemplateDropdown,
                StudyArchivedTemplateDropdown,
                StudyExternalIRBTemplateDropdown,
                MODCRReviewTemplateDropdown,
                MODCRCompleteTemplateDropdown,
                RNIReviewTemplateDropdown,
                RNICompleteTemplateDropdown
            });
        }
    }

    public class IntegrationSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity%5BOID%5BC5F69EF8889104468A0C9DD9DADF2465%5D%5D");
            IWebElement tab = driver.FindElement(By.XPath("//a[contains(@href, '#IntegrationSettingsTab')]"));
            tab.Click();
        }

        public bool GrantsIntegrationEnabled
        {
            get
            {
                return driver.FindElement(By.Name("_IRBSettings.customAttributes.integrationSettings.customAttributes.isGrantsIntegrationEnabled")).Selected;
            }
        }

        public bool CTMSIntegrationEnabled
        {
            get
            {
                return driver.FindElement(By.Name("_IRBSettings.customAttributes.integrationSettings.customAttributes.isCTMSIntegrationEnabled")).Selected;
            }
        }

        public bool IRBExchangeIntegrationEnabled
        {
            get
            {
                return driver.FindElement(By.Name("_IRBSettings.customAttributes.isIRBExchangeEnabled")).Selected;
            }
        }
    }

    public class AttachmentCategoriesSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate(PageController.TabPath("C5F69EF8889104468A0C9DD9DADF2465", "DB455C9382F0444C90C01B0C12CAF7AC"));
        }

        public IWebElement CategoryTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='_webrRSV_DIV_0']/table")); }
        }
    }

    public class WatermarkTypesSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate(PageController.TabPath("C5F69EF8889104468A0C9DD9DADF2465", "D224F8E7CD232E40AE0FC5A31774A568"));
        }

        public ReadOnlyCollection<IWebElement> WatermarkLinks
        {
            get { return driver.FindElements(By.XPath("//div[@id='_webrRSV_DIV_0']/table/tbody/tr/td[2]/a[@class='hyperlink']")); }
        }
    }

    public class DrugsSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate(PageController.TabPath("C5F69EF8889104468A0C9DD9DADF2465", "28D7A75A938D4344AD232C5D9EB1244F"));
        }

        public IWebElement DrugsTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='_webrRSV_DIV_0']/table")); }
        }
    }

    public class DevicesSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate(PageController.TabPath("C5F69EF8889104468A0C9DD9DADF2465", "71E1F9D44EF05D4DA2CE1C9AA2B9038E"));
        }

        public IWebElement DevicesTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='_webrRSV_DIV_0']/table")); }
        }
    }

    public class StudyTeamMemberRolesSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate(PageController.TabPath("C5F69EF8889104468A0C9DD9DADF2465", "728A400363EC924E83A4C8E66065828D"));
        }

        public IWebElement RolesTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='_webrRSV_DIV_0']/table")); }
        }
    }

    public class ReviewerRolesSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate(PageController.TabPath("C5F69EF8889104468A0C9DD9DADF2465", "27BE8F86FF1DA540AD911924AFEC641D"));
        }

        public IWebElement RolesTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='_webrRSV_DIV_0']/table")); }
        }
    }

    public class AncillaryReviewsSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate(PageController.TabPath("C5F69EF8889104468A0C9DD9DADF2465", "F8F84379D095904D8FF2C6A420056A41"));
        }
        public IWebElement ReviewTypesTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='_webrRSV_DIV_0']/table")); }
        }
    }

    public class ExemptCategoriesSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate(PageController.TabPath("C5F69EF8889104468A0C9DD9DADF2465", "51D5ED41ACF891459809D3D749F7364E"));
        }
        public IWebElement CategoriesTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='_webrRSV_DIV_0']/table")); }
        }
    }

    public class ExpeditedCategoriesSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate(PageController.TabPath("C5F69EF8889104468A0C9DD9DADF2465", "51D5ED41ACF891459809D3D749F7364E"));
            IWebElement tab = driver.FindElement(By.XPath("//a[contains(@href, '#ExpeditedTab')]"));
            tab.Click();
        }
        public IWebElement CategoriesTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='_webrRSV_DIV_1']/table")); }
        }
    }

    public class IRBOfficeSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate(PageController.TabPath("C5F69EF8889104468A0C9DD9DADF2465", "F4F030A0E6625F4EB3DA91AAC183B726"));
        }

        public IWebElement IRBsTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='_webrRSV_DIV_0']/table")); }
        }
    }
}