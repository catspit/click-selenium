﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.SAFETY.Protocol.Pages
{
    public class SAFETYProtocolRiskGroupAndContainmentPractices : SAFETYProtocolFormPage
    {
        public SAFETYProtocolRiskGroupAndContainmentPractices(SAFETYProtocol protocol)
            : base(protocol)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[CBA640BDC2FBBF4B8C4E19900CA5B255]]");
        }
    }
}
