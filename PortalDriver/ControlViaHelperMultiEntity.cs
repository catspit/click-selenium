﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver {
    internal class ControlViaHelperMultiEntity : IControlFactoryFromHelper {

        public IPortalControl create(ControlHelper helper, IPortalDriver driver)
        {
            if (helper.type == "EntityTable") return new ControlMultiEntity(helper, driver);
            return null;
        }

        internal class ControlMultiEntity : ControlFromHelper
        {
            public override int Count
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public override string OID
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public override string Text
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public override bool IsClear
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public override IWebElement AddButton
            {
                get
                {
                    throw new NotImplementedException();
                }
            }
            public override IWebElement SelectButton
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public override IPortalControlValue this[int index]
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            internal ControlMultiEntity(ControlHelper helper, IPortalDriver driver):base(helper,driver)
            {
            }

            public override bool Is(IPortalControlValue value)
            {
                throw new NotImplementedException();
            }

            public override bool Is(string textOrOID)
            {
                throw new NotImplementedException();
            }

            public override IEnumerator<IPortalControlValue> GetEnumerator()
            {
                throw new NotImplementedException();
            }

            public override IPortalControlValue Clear()
            {
                throw new NotImplementedException();
            }

            public override IPortalControlValue Val()
            {
                throw new NotImplementedException();
            }

            public override IPortalControlValue Val(string TextOrOID)
            {
                throw new NotImplementedException();
            }

            public override IPortalControlValue Val(DateTime date)
            {
                throw new NotImplementedException();
            }
        }
    }
}
