﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClickPortal.Pages;

namespace ClickPortal.IACUC.Procedure.Pages
{
    public class IACUCProcedureForm : SmartFormPage
    {
        public IACUCProcedureProcedureIdentificationForm ProcedureIdentificationForm;
        public IACUCProcedureAdministrationOfSubstancesForm AdministrationOfSubstancesForm;
        public IACUCProcedureAntibodyProductionForm AntibodyProductionForm;
        public IACUCProcedureBehavioralProceduresForm BehavioralProceduresForm;
        public IACUCProcedureEuthanasiaForm EuthanasiaForm;
        public IACUCProcedureFoodOrFluidRestrictionForm FoodOrFluidRestrictionForm;
        public IACUCProcedureNonSurvivalSurgeryForm NonSurvivalSurgeryForm;
        public IACUCProcedureProlongedPhysicalRestraintForm ProlongedPhysicalRestraintForm;
        public IACUCProcedureSurvivalSurgeryForm SurvivalSurgeryForm;
        public IACUCProcedureTissueCollectionForm TissueCollectionForm;

        public IACUCProcedure Procedure;

        public IACUCProcedureForm(IACUCProcedure procedure)
        {
            ProcedureIdentificationForm = new IACUCProcedureProcedureIdentificationForm(procedure);
            AdministrationOfSubstancesForm = new IACUCProcedureAdministrationOfSubstancesForm(procedure);
            AntibodyProductionForm = new IACUCProcedureAntibodyProductionForm(procedure);
            BehavioralProceduresForm = new IACUCProcedureBehavioralProceduresForm(procedure);
            EuthanasiaForm = new IACUCProcedureEuthanasiaForm(procedure);
            FoodOrFluidRestrictionForm = new IACUCProcedureFoodOrFluidRestrictionForm(procedure);
            NonSurvivalSurgeryForm = new IACUCProcedureNonSurvivalSurgeryForm(procedure);
            ProlongedPhysicalRestraintForm = new IACUCProcedureProlongedPhysicalRestraintForm(procedure);
            SurvivalSurgeryForm = new IACUCProcedureSurvivalSurgeryForm(procedure);
            TissueCollectionForm = new IACUCProcedureTissueCollectionForm(procedure);
        }

        public override void FillOutUsingTestData()
        {
            throw new NotImplementedException();
        }
    }
}
