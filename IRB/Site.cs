﻿using ClickPortal.PortalDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IRB
{
    public class Site : IRBStudy
    {
        public string MSSStudyID;

        public Site(string mSSStudyID) : base()
        {
            MSSStudyID = mSSStudyID;
            this.ID = "SITE" + mSSStudyID;
            SubmissionType = "IRBSITE";
        }

        public Site(IRBStudy mssStudy) : this(mssStudy.ID)
        {
        }

        public override string CreateScript
        {
	        get 
            {
                if(MSSStudyID == null )
                {
                    throw new Exception("Error creating SITE, MSS Study ID is NULL");
                }

                string script = base.CreateScript;

                script += @"var mssStudy = getResultSet(""_IRBSubmission"").query(""ID = '" + MSSStudyID + @"'"");
if( mssStudy.count() === 0 ){
    throw new Error(-1, ""Incorrect number of entities found when querying MSS Study"");
}
mssStudy = mssStudy.elements().item(1);
var today = new Date();
etype.setQualifiedAttribute(""dateCreated"", today);
etype.setQualifiedAttribute(""dateModified"", today);
etype.setQualifiedAttribute(""customAttributes.irbSettings"", _IRBSettings.getIRBSettings());
etype.setQualifiedAttribute(""customAttributes.mSSStudy"", mssStudy);
etype.setQualifiedAttribute(""customAttributes.parentStudy"", etype);
etype.setQualifiedAttribute(""resourceContainer"", ResourceContainer.createEntity());
etype.setQualifiedAttribute(""resourceContainer.name"", etype.getQualifiedAttribute(""name""));
etype.setQualifiedAttribute(""currentSmartFormStartingStep"", etype.getStartingWizardPageForSite());
etype.setQualifiedAttribute(""company"", mssStudy.getQualifiedAttribute(""company""));
etype.setQualifiedAttribute(""createdBy"", mssStudy.getQualifiedAttribute(""createdBy""));
etype.setQualifiedAttribute(""customAttributes.IRB"", mssStudy.getQualifiedAttribute(""customAttributes.IRB""));

var commonRule = mssStudy.getQualifiedAttribute(""customAttributes.currentCommonRule"");
etype.setQualifiedAttribute(""customAttributes.currentCommonRule"", commonRule);
etype.setQualifiedAttribute(""customAttributes.initialCommonRule"", commonRule);

// Because the SITE is created programatically instead of via state transition, need to include the resource container
etype.createWorkspace(mssStudy.resourceContainer, etype.setWorkspaceTemplate());";

                return script;
            }
        }

        public void Activate(IPortalDriver driver)
        {
            string script = @"var site = wom.getEntityFromString(""" + OID + @""");
var activeStatus = getResultSet(""ProjectStatus"").query(""ID = 'Active' AND projectType = '_IRBSubmission'"").elements().item(1);
site.status = activeStatus
var triggerActivity = wom.CreateTransientEntity(""_IRBSubmission_SendLetter"");
site.cloneIRBSubmission(triggerActivity);";

            driver.Store().Execute(script);

            script = @"var site = wom.getEntityFromString(""" + OID + @""");
? (site.getQualifiedAttribute(""customAttributes.draftStudy"") != null ? true : false)";

            int count = 0;
            while(driver.Store().Execute(script).Equals("false") && count < 15)
            {
                count++;
            }

            if( count == 15 )
            {
                throw new Exception("Error activating SITE, no draft study created.");
            }
        }
    }
}
