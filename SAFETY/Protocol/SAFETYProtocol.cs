﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClickPortal.SAFETY.Protocol.Pages;
using Newtonsoft.Json;

namespace ClickPortal.SAFETY.Protocol
{
    public class SAFETYProtocol : ClickEType
    {
        protected static int created = 0;

        [ETypeProperty("ID")]
        public string ID;
        [ETypeProperty("name")]
        [ETypeProperty("customAttributes.protocolTitle")]
        public string Name;
        [ETypeProperty("description")]
        public string Description;
        [ETypeProperty("company", RelatedEType = "Company")]
        public string Company;
        [ETypeProperty("customAttributes.adminOffice", RelatedEType = "_AdminOffice")]
        public string AdminOffice;
        [ETypeProperty("status", RelatedEType = "ProjectStatus")]
        public string Status;
        [ETypeProperty("createdBy", RelatedEType = "Person", RelatedKeyName = "userId")]
        [ETypeProperty("customAttributes.investigator.customAttributes.studyTeamMember", RelatedEType = "Person", RelatedKeyName = "userId")]
        [ETypeProperty("customAttributes.primaryContact.customAttributes.studyTeamMember", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string InvestigatorUserID;
        [ETypeProperty("owner", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string Owner;
        [ETypeProperty("dateEnteredState")]
        public string DateEnteredState;
        [ETypeProperty("customAttributes.safetyReviewType", RelatedEType = "_ClickSafetyReview", RelatedKeyName = "customAttributes.reviewName")]
        public string SafetyReview;
        [ETypeProperty("customAttributes.typeOfSubmission", RelatedEType = "_SubmissionType", RelatedKeyName = "ID")]
        public string SubmissionType;

        public SAFETYProtocol ParentProtocol;

        public string ResourceContainerTemplateID = "TMPL8D14CB9F7F15F29";
        public string ResourceContainerParent = "CLICK_SAFETY_HOME";
        public string CreateView = "VIEW8D11E570A3244CC";

        private ProtocolWorkspace workspace;
        private SAFETYProtocolForm _protocolForm;
        protected int protocolNumber;


        public SAFETYProtocol() : base("_ClickSafetySubmission")
            {
            this.protocolNumber = ++created;
            this.ID = "TEST-" + protocolNumber;
            this.Name = this.Description = "Test Protocol " + protocolNumber;
            this.Company = "Test Company";
            this.InvestigatorUserID = this.Owner = "administrator";
            this.DateEnteredState = DateTime.Now.ToShortDateString();
            this.SafetyReview = "Biosafety";
        }

        public SAFETYProtocol(SAFETYProtocol source) : this()
            {
            this.OID = source.OID;
            this.ID = source.ID;
            this.Name = source.Name;
            this.Description = source.Description;
            this.Company = source.Company;
            this.AdminOffice = source.AdminOffice;
            this.Status = source.Status;
            this.InvestigatorUserID = source.InvestigatorUserID;
            this.Owner = source.Owner;
            this.SafetyReview = source.SafetyReview;
            this.Workspace = new ProtocolWorkspace(source.Workspace.OID);
        }

        public SAFETYProtocol(String ID) : this()
            {
            this.ID = ID;
        }

        public ProtocolWorkspace Workspace
        {
            get
            {
                if (workspace == null)
                {
                    //FindWorkspaceFromProtocolList(protocolList);
                }
                return workspace;
            }
            set
            {
                this.workspace = value;
            }
        }

        public SAFETYProtocolForm ProtocolForm
        {
            get
            {
                if (_protocolForm == null)
                {
                    _protocolForm = new SAFETYProtocolForm(this);
                }
                return _protocolForm;
            }
        }

        public override string CreateScript
        {
            get
            {
                string script = @"var etype = wom.createTransientEntity(""_ClickSafetySubmission"");
    etype.registerEntity();
    etype.initialize();";
                string propertiesThatMustBeAddedAtTheEnd = "";
                List<string> optionalParams = new List<string>();

                int relatedCount = 0;
                foreach (FieldInfo field in this.GetType().GetFields())
                {
                    foreach (object attr in field.GetCustomAttributes(true))
                    {
                        if (attr is ETypeProperty && field.GetValue(this) != null)
                        {
                            if (((ETypeProperty)attr).RelatedEType != null)
                            {
                                string fieldname = ((ETypeProperty)attr).RelatedKeyName == null ? "ID" : ((ETypeProperty)attr).RelatedKeyName;
                                switch (((ETypeProperty)attr).PropertyName)
                                {
                                    case "owner":
                                        script += "\nvar entOwnerPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
    if (entOwnerPerson == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "company":
                                        script += "\nvar entCompany = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
    if (entCompany == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "createdBy":
                                        script += "\nvar entCreatedByPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
    if (entCreatedByPerson == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "status":
                                        propertiesThatMustBeAddedAtTheEnd += "\nvar related" + ++relatedCount + " = getResultSet(\""
                                    + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                    + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @" AND projectType='_ClickSafetySubmission'"
                                    + @""");
    if (related" + relatedCount + @".count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }" + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + "related" + relatedCount + ".elements()(1)" + ");"
                                    + "\n";
                                        break;
                                    case "customAttributes.typeOfSubmission":
                                        propertiesThatMustBeAddedAtTheEnd += "\nvar typeOfSubmission = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'') + "\");"
                                            + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", typeOfSubmission.elements()(1));";
                                        break;
                                    default:
                                        optionalParams.Add("\nvar related" + ++relatedCount + " = getResultSet(\""
                                    + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                    + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""");
    if (related" + relatedCount + @".count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }" + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + "related" + relatedCount + ".elements()(1)" + ");");
                                        break;
                                }
                            }
                            else
                            {
                                switch (((ETypeProperty)attr).PropertyName)
                                {
                                    case "ID":
                                        propertiesThatMustBeAddedAtTheEnd += "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");";
                                        break;

                                    default:
                                        optionalParams.Add("\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");");
                                        break;
                                }
                            }
                        }
                    }
                }

                script += "\netype.setRequiredFields(entOwnerPerson, entCompany, entCreatedByPerson);";
                foreach (string snip in optionalParams)
                {
                    script += snip;
                }
                script += @"
    var rc = ResourceContainer.createFromResource(etype, getElements(""ContainerTemplateForID"", ""ID"", """ + ResourceContainerTemplateID + @""")(1));
    rc.setQualifiedAttribute(""parent"", getElements(""ContainerBaseForID"", ""ID"", """ + ResourceContainerParent + @""")(1));
    etype.setQualifiedAttribute(""resourceContainer"", rc);
    var sch = ShadowSCH.getSCH();
    var view = wom.getEntityFromString(""com.webridge.entity.Entity[OID[B5A6F69A05E316449AB0CEFDA1F3B247]]"");
    wom.putContext(""currentView"", view, true);
    ProjectStateTransition.processProject(sch, etype, etype, null);
    " + propertiesThatMustBeAddedAtTheEnd + @"
    ?etype;";
                return script;
            }
        }

        public override void CreateEntity(PortalDriver.PortalDriver Driver)
        {
            base.CreateEntity(Driver);
            FindWorkspace(Driver);
        }

        public void FindWorkspace(PortalDriver.PortalDriver Driver)
        {
            string getWorkspaceOidScript = @"?wom.getEntityFromString(""" + this.OID + @""").getQualifiedAttribute(""resourceContainer"");";
            string workspaceOid = Driver.AdminDriver.Console().Execute(getWorkspaceOidScript);
            this.Workspace = new ProtocolWorkspace(workspaceOid);
        }

        public new String OID
        {
            get
            {
                if (base.OID == default(String))
                {
                    this.Workspace.Navigate();
                    base.OID = this.Workspace.projectOid;
                }
                return base.OID;
            }
            set
            {
                base.OID = value;
            }
        }

        public override void DestroyEntity(PortalDriver.PortalDriver Driver)
        {
            base.DestroyEntity(Driver);
        }
    }
}
