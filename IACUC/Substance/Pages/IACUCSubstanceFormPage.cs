﻿using ClickPortal.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Substance.Pages
{
    public class IACUCSubstanceFormPage : PageController
    {
        protected IACUCSubstance _substance;
        public IACUCSubstanceFormPage(IACUCSubstance substance)
        {
            _substance = substance;
        }

        public bool isEditable()
        {
            return driver.FindElements(By.Id("InkSaveProjectEditor")).Count > 0;
        }

        public IWebElement SaveLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Save"));
            }
        }

        public IWebElement ExitLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Exit"));
            }
        }
    }
}
