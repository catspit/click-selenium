﻿using ClickPortal.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.Controls
{
    public class SingleEntitySelection : ClickControl
    {
        public SingleEntitySelection(PageController currentPage, string propertyLabel) : base(currentPage, propertyLabel)
        {
        }

        public void SetValue(string filterName, string value)
        {
            // Click select button and open popup window
            var selectButton = Container.FindElement(By.CssSelector("input[value='Select...']"));
            CurrentPage.OpenPopup(selectButton, null, By.Id("btnOk"));

            // Select filter name from the drop down list
            var select = Driver.FindElement(By.Name("_webrRSV_FilterField_0_0"));
            var selectElement = new SelectElement(select);
            selectElement.SelectByText(filterName);

            // Search using the value query
            Driver.FindElement(By.Name("_webrRSV_FilterValue_0_0")).SendKeys(value);
            var goButton = Driver.FindElement(By.CssSelector("input[type='button'][value='Go']"));
            goButton.Click();

            // Select the first item from the result
            Driver.FindElement(By.Id("webrRSV__SelectedItem_0")).Click();
            Driver.FindElement(By.Id("btnOk")).Click();
        }

        public override void SetValue(string value)
        {
            // Type value to the input field
            var inputField = Container.FindElement(By.CssSelector("input[_chooserconfig]"));
            inputField.SendKeys(value);

            // Select the first item from the suggested results 
            var chooserItemLocator = By.CssSelector("table[class='jsonSuggestResults'] > tbody > tr");
            CurrentPage.WaitForElementToBeVisible(chooserItemLocator);
            
            var chooserItem = Container.FindElement(chooserItemLocator);
            chooserItem.Click();
        }
    }
}
