﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.SAFETY.Protocol.Pages
{
    public class SAFETYProtocolSupportingDocuments : SAFETYProtocolFormPage
    {
        public SAFETYProtocolSupportingDocuments(SAFETYProtocol protocol)
            : base(protocol)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[F62A75D688F27E4EA11382EB8AA09BFD]]");
        }
    }
}
