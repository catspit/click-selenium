﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Procedure.Pages
{
    public class IACUCProcedureTissueCollectionForm : IACUCProcedureFormPage
    {
        public IACUCProcedureTissueCollectionForm(IACUCProcedure procedure) : base(procedure)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _procedure.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[F23B7FA88A401040BA30114B5AE31C61]]");
        }
    }
}
