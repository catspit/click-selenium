﻿namespace ClickPortal.Pages
{
    public abstract class Workspace : PageController
    {
        public Project Project;

        protected Workspace(Project project)
        {
            Project = project;
        }

        public abstract string GetIdFromWorkspace();
    }
}
