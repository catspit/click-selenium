﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver {
    public class PortalSmartform : IPortalSmartform
    {
        private PortalDriver driver;

        public PortalSmartform(PortalDriver driver)
        {
            this.driver = driver;
        }

        public string Title()
        {
            return this.driver.FindElement(By.TagName("h1")).Text;
        }

        public void Continue()
        {
            this.driver.FindElement(By.Id("continue_btn_Top")).Click();
        }

        public void Back()
        {
            this.driver.FindElement(By.Id("back_btn_Top")).Click();
        }

        public void Save()
        {
            //this.driver.FindElement(By.Id("lnkSaveProjectEditor_Top")).Click();
            var searchBy = By.Id("lnkSaveProjectEditor_Top");
            this.driver.FindElement(searchBy).Click();

            IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(driver1 => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public void Exit()
        {
            this.driver.FindElement(By.Id("lnkExitProjectEditor_Top")).Click();
        }

        public void JumpTo(string sfName)
        {
            var jumpToMenuBy = By.Id("jumpToMenuLink_Top");
            this.driver.FindElement(jumpToMenuBy).Click();

            this.driver.FindElement(By.XPath("//nobr[contains(text(), '" + sfName.Replace(" ", "\u00A0") + "')]")).Click();
        }

        public void Finish()
        {
            const string continueButtonId = "finish_btn_Top";
            driver.ExecuteScript("document.getElementById('" + continueButtonId + "').focus();", null);
            driver.FindElement(By.Id(continueButtonId)).Click();
        }
    }
}
