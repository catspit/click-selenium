﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using ClickPortal.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal
{
    public class RMConsole : PageController
    {
        public const string ORGANIZATIONS = "Organizations";
        public const string PERSONS = "Contacts";

        public override void Navigate()
        {
            base.Navigate("/RMConsole/MainFrame");
            ReadOnlyCollection<IWebElement> tabs = driver.FindElements(By.LinkText("Active"));
            if (tabs.Count > 0)
            {
                tabs[0].Click();
            }
        }

        public String getWorkspaceFromStudyID(String ID)
        {
            String workspaceOid = null;
            FastFindProject("IRB Submission", null, ID);

            driver.InFrame(By.Name("sidebar"), (dr) =>
            {
                dr.InFrame(By.Name("ifrmResults"), (d) =>
                {
                    IWebElement projectLink = d.FindElement(By.XPath("//*[@id='divResultsDisplay']/span/table/tbody/tr/td[2]/table/tbody/tr/td[2]/a"));
                    projectLink.Click();
                });
            });

            driver.InFrame(By.Name("content"), (dr) =>
            {
                IWebElement workspaceLink = new WebDriverWait(dr, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.LinkText("Workspace")));
                workspaceOid = workspaceLink.GetAttribute("href").Substring(workspaceLink.GetAttribute("href").IndexOf("com.webridge.entity")).Replace("%5B", "[").Replace("%5D", "]");
            });

            return workspaceOid;
        }

        public void FastFind(String searchType, String organization = null, String last = null, String first = null)
        {
            SelectSearchType(searchType);

            driver.InFrame(By.Name("sidebar"), (dr) =>
            {
                new WebDriverWait(dr, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementIsVisible(By.Id("tblPersonFields")));
                if (organization != null) dr.FindElement(By.Id("txtCompanyName")).SendKeys(organization);
                if (last != null) dr.FindElement(By.Id("txtPersonLastName")).SendKeys(last);
                if (first != null) dr.FindElement(By.Id("txtPersonFirstName")).SendKeys(first);
                dr.FindElement(By.Id("btnSearch")).Click();

                dr.InFrame(By.Id("ifrmResults"), (d) =>
                {
                    new WebDriverWait(d, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists(By.Id("divResultsDisplay")));
                });
            });
        }

        public void FastFindProject(String projectType, String projectName = null, String projectID = null, String organization = null)
        {
            SelectSearchType("Projects");

            driver.InFrame(By.Name("sidebar"), (dr) =>
            {
                WebDriverWait wait = new WebDriverWait(dr, TimeSpan.FromSeconds(5));
                SelectElement projectTypeSelect = new SelectElement(wait.Until(ExpectedConditions.ElementIsVisible(By.Id("selProjectType"))));
                projectTypeSelect.SelectByText(projectType);
                if (projectName != null) dr.FindElement(By.Id("txtProjectName")).SendKeys(projectName);
                if (projectID != null) dr.FindElement(By.Id("txtProjectID")).SendKeys(projectID);
                if (organization != null) dr.FindElement(By.Id("txtCompanyName")).SendKeys(organization);
                dr.FindElement(By.Id("btnSearch")).Click();

                dr.InFrame(By.Id("ifrmResults"), (d) =>
                {
                    new WebDriverWait(d, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists(By.XPath("//span[@class='Text8']")));
                });
            });
        }

        public RMConsolePersonPage OpenPersonResultByIndex(int index)
        {
            string PersonOid = null;
            driver.InFrame(By.Name("sidebar"), (dr) =>
            {
                dr.InFrame(By.Id("ifrmResults"), (d) =>
                {
                    ReadOnlyCollection<IWebElement> results = d.FindElements(By.XPath("//a[@class='hyperlink']"));
                    String href = results[index].GetAttribute("href");
                    Regex re = new Regex("Person=([^&]*)");
                    PersonOid = re.Match(href).Groups[1].Value;
                });
            });

            RMConsolePersonPage page = new RMConsolePersonPage(PersonOid);
            page.Navigate();
            return page;
        }

        public RMConsoleOrganizationPage OpenOrganizationResultByIndex(int index)
        {
            String OrgOID = "";

            driver.InFrame(By.Name("sidebar"), (dr) =>
            {
                dr.InFrame(By.Id("ifrmResults"), (d) =>
                {
                    ReadOnlyCollection<IWebElement> results = d.FindElements(By.XPath("//a[@class='hyperlink']"));
                    String href = results[index].GetAttribute("href");
                    Regex re = new Regex("Company=([^&]*)");
                    OrgOID = re.Match(href).Groups[1].Value;
                });
            });

            RMConsoleOrganizationPage page = new RMConsoleOrganizationPage(OrgOID);
            page.Navigate();
            return page;
        }

        private void SelectSearchType(String type)
        {
            driver.InFrame(By.Name("sidebar"), (d) =>
            {
                d.FindElement(By.LinkText("Fast Find")).Click();
                SelectElement searchType = new SelectElement(d.FindElement(By.Id("selSearchType")));
                searchType.SelectByText(type);
            });
        }
    }

    public class RMConsolePage : PageController
    {
        protected static void ClickTab(String TabName)
        {
            driver.InFrame(By.Name("content"), (dr) =>
            {
                dr.FindElement(By.LinkText(TabName)).Click();
                new WebDriverWait(dr, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.XPath("//body/input[@id='targetOid']")));
            });
        }
    }

    public class RMConsolePersonPage : RMConsolePage
    {
        protected string PersonOid;

        public RMConsolePersonPage(String Oid)
        {
            this.PersonOid = Oid;
        }

        public override void Navigate()
        {
            base.Navigate("/RMConsole/MainFrame?Person=" + this.PersonOid);
        }

        public RMConsolePersonAccountPage AccountPage
        {
            get
            {
                return new RMConsolePersonAccountPage(PersonOid);
            }
        }
    }

    public class RMConsolePersonAccountPage : RMConsolePersonPage
    {
        public RMConsolePersonAccountPage(String Oid) : base(Oid)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate();
            ClickTab("Account");
        }

        public ReadOnlyCollection<String> Roles
        {
            get
            {
                List<String> roleText = new List<string>();

                driver.InFrame(By.Name("content"), (dr) =>
                {
                    // assignedRolesHeaderTable: table containing the text "Assigned Role(s):". This is the header table.
                    IWebElement assignedRolesHeaderSpan = new WebDriverWait(dr, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementExists(By.XPath("//span[contains(text(),'Assigned Role(s)')]")));
                    IWebElement assignedRolesHeaderTable = assignedRolesHeaderSpan.FindElement(By.XPath("./ancestor-or-self::table"));
                    
                    // assignedRolesDataTale: table containing assigned actual roles
                    IWebElement assignedRolesDataTable = assignedRolesHeaderTable.FindElement(By.XPath("following-sibling::table"));
                    ReadOnlyCollection<IWebElement> assignedRolesDataRows = assignedRolesDataTable.FindElements(By.XPath("tbody/tr/td/table/tbody/tr"));
                    foreach (IWebElement row in assignedRolesDataRows)
                    {
                        roleText.Add(row.Text);
                    }
                });

                return new ReadOnlyCollection<String>(roleText);
            }
        }
    }

    public class RMConsoleOrganizationPage : RMConsolePage
    {
        protected String OrgOid;

        public RMConsoleOrganizationPage(String Oid)
        {
            this.OrgOid = Oid;
        }

        public override void Navigate()
        {
            base.Navigate("/RMConsole/Organization/OrganizationDetails?detailView=true&Company=" + this.OrgOid);
        }

        public RMConsoleOgranizationPropertiesPage PropertiesPage
        {
            get
            {
                return new RMConsoleOgranizationPropertiesPage(this.OrgOid);
            }
        }

    }

    public class RMConsoleOgranizationPropertiesPage : RMConsoleOrganizationPage
    {
        public RMConsoleOgranizationPropertiesPage(String Oid) : base(Oid)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate();
            ClickTab("Properties");
        }

        public void SelectView(String ViewNameOrOID)
        {
            driver.InFrame(By.Name("content"), (dr) => {
                IWebElement selectElem = new WebDriverWait(dr, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementIsVisible(By.XPath("//span[text()='Select View']/following-sibling::select")));
                SelectElement viewSelect = new SelectElement(selectElem);
                if (ViewNameOrOID != null && ViewNameOrOID.IndexOf("com.webridge.entity.Entity[OID[") == 0)
                {
                    string optionValue = selectElem.FindElement(By.XPath(".//option[contains(@value,'" + ViewNameOrOID + "')]")).GetAttribute("value");
                    viewSelect.SelectByValue(optionValue);
                }
                else
                {
                    viewSelect.SelectByText(ViewNameOrOID);
                }
                new WebDriverWait(dr, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.Id("_webr_EntityView")));
            });
        }

        public IWebElement GetControl(String PropertyName)
        {
            IWebElement control = null;
            driver.InFrame(By.Name("content"), (d) =>
            {
                 d.FindElement(By.Name(PropertyName));
            });

            return control;
        }
    }
}
