﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class IACUCProtocolAnnualReview : IACUCProtocolFormPage
    {
        public IACUCProtocolAnnualReview(IACUCProtocol protocol) : base(protocol)
        {
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[4748B220D9A56E4882F401AF0F4B34B0]]");
        }
    }
}
