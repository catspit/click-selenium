﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ClickPortal.PortalDriver;
using System.Linq;
using System.Collections.Generic;

namespace ClickPortal.Pages 
{
    public class PageController
    {
        public static String BaseURL = "";
        public static IPortalDriver driver;
        private String RootWindowHnd;
        private int NumWindows = 1;
        public const double PAGE_WAIT_TIMEOUT = 100.0;

        public PageController()
        {
            RootWindowHnd = driver.CurrentWindowHandle;
        }

        public virtual void Navigate()
        {
            Navigate("/");
        }

        public void Navigate(String pathUrl, Dictionary<string, string> query = null)
        {
            driver.NavigateToRelativePath(pathUrl, query); 
        }

        protected string GetBaseUrl()
        {
            var url = driver.Url; // get the current URL (full)
            var currentUri = new Uri(url); // create a Uri instance of it
            var storeName = currentUri.Segments[1].Replace("/", "");
            var baseUrl = currentUri.OriginalString.Replace(currentUri.PathAndQuery, "") + "/" + storeName; // just get the "base" bit of the URL
            return baseUrl;
        }

        public bool IsPrintSmartformsPage()
        {
            return GetUriLastSegment() == "PrintSmartForms";
        }

        public bool IsViewDifferencesPage()
        {
            return GetUriLastSegment() == "ViewChanges";
        }

        public bool IsSmartformPage()
        {
            return GetUriLastSegment() == "ProjectEditor";
        }

        public string GetUriLastSegment()
        {
            var currentUri = new Uri(driver.Url); // create a Uri instance of it
            var uriSegments = currentUri.Segments;
            return uriSegments[uriSegments.Length - 1];
        }

        [Obsolete("use PortalDriver.Window().New() instead")]
        public void OpenPopup(IWebElement anchor, string windowTitle = null, By locator = null)
        {
            driver.Window().New(anchor.Click);

            // Wait until the element with the given locator is visible
            if (locator != null)
            {
                new WebDriverWait(driver, TimeSpan.FromSeconds(30.00)).Until(ExpectedConditions.ElementIsVisible(locator));
            }
        }

        [Obsolete("use PortalDriver.Window().Close() instead")]
        public void ClosePopup()
        {
            driver.Window().Close(driver.Close);
        }

        [Obsolete("use PortalDriver.Window().Close() instead")]
        public void WaitForPopupClose(int seconds = 10)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(seconds)).Until((d) => { return d.WindowHandles.Count < NumWindows; });
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            NumWindows = driver.WindowHandles.Count;
        }

        public void WaitForElementToBeEnabled(By locator)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(PAGE_WAIT_TIMEOUT)).Until(ElementIsClickable(driver.FindElement(locator)));
        }

        public void WaitForElementToBeEnabled(IWebElement element)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(PAGE_WAIT_TIMEOUT)).Until(ElementIsClickable(element));
        }

        public void WaitForElementToBeVisible(By locator)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(PAGE_WAIT_TIMEOUT)).Until(ExpectedConditions.ElementIsVisible(locator));
        }

        public void FocusRootWindow()
        {
            driver.SwitchTo().Window(driver.WindowHandles.First());
        }

        public static Func<IWebDriver, IWebElement> ElementIsClickable(IWebElement element)
        {
            return driver =>
            {
                return (element != null && element.Displayed && element.Enabled) ? element : null;
            };
        }

        [Obsolete("Use Driver.TopNavTab() instead.")]
        public void ClickNavigatorTab(string tabName)
        {
            driver.FindElement(By.XPath("//a[starts-with(@class, 'TopNavTab')][text() = '" + tabName + "']")).Click();
        }

        public static string TabPath(string containerOid, string tabOid)
        {
            if (driver.Store().portalVersionMajor == 8)
            {
                return "/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[" + containerOid + "]]&tab2=" + tabOid;
            }

            return "/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[" + containerOid + "]]&Tab2=com.webridge.entity.Entity[OID[" + tabOid + "]]";
        }
    }
}
