﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver
{
    public class PortalJob : IPortalJob
    {
        internal IPortalDriver Driver;
        internal string Name;

        public PortalJob(IPortalDriver driver, string name)
        {
            Driver = driver;
            Name = name;
        }

        private IWebElement GetJobLink()
        {
            return Driver.FindElement(By.XPath("//td[@data-drsv-column='0']/span/a[text()='" + Name + "']"));
        }

        public bool Is()
        {
            try
            {
                GetJobLink();
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void Open()
        {
            Driver.Window().New(GetJobLink().Click);
        }

        public void Preview()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementExists(By.LinkText("Content"))).Click();
            Driver.Window().New(Driver.FindElement(By.XPath("//input[@value='Preview']")).Click);
        }
    }
}
