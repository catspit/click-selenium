﻿using System;
using System.Collections.ObjectModel;
using ClickPortal.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.BaseTypes
{
    public class ProjectListingComponent
    {
        private readonly PageController _page;
        private readonly IWebDriver _driver;

        public IWebElement FilterQuery
        {
            get
            {
                By queryLocator = By.CssSelector("input[type='text'][name$='_queryCriteria1']");
                _page.WaitForElementToBeEnabled(queryLocator);
                return _driver.FindElement(queryLocator);
            }
        }

        public SelectElement FilterBy
        {
            get
            {
                By filterLocator = By.CssSelector("select[id$='_queryField1']");
                _page.WaitForElementToBeEnabled(filterLocator);
                var filterByElement = _driver.FindElement(filterLocator);
                return new SelectElement(filterByElement);
            }
        }
        // constructor
        public ProjectListingComponent(PageController page, IWebDriver driver)
        {
            _page = page;
            _driver = driver;
        }

        // Click Go button to perform search
        public void ClickGoButton()
        {
            var goButton = _driver.FindElement(By.CssSelector("input[type='button'][value='Go'][id$='_requery']"));
            goButton.Click();
        }

        /// <summary>
        /// Return table rows from the project listing component. The first row at index 0 will be the header row.
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<IWebElement> GetProjectListingTableRows()
        {
            _page.WaitForElementToBeEnabled(By.CssSelector("input[id$='_pageInput']"));   // Wait until result displays
            var rows = _driver.FindElements(By.CssSelector("div[id^='component'] > table > tbody > tr"));
            return rows;
        }

        public void ClickLinkToProjectWorkspace(int rowNumber = 1)
        {
            var rows = GetProjectListingTableRows();
            if (rows.Count < 2)
            {
                throw new Exception("There are " + rows.Count + " rows in the table.");
            }
            var targetRow = rows[rowNumber];
            var linkToWorkspace = targetRow.FindElement(By.CssSelector("td[class='List'] > span[class='textControl'] > a[href*='Container=']"));
            linkToWorkspace.Click();
        }

        /// <summary>
        /// Select "Filter By" dropdown, type filter query, click "Go" button and click the row's workspace link. By default, click the workspace link from the first row.
        /// </summary>
        /// <param name="filterByText">"Filter By" dropdown text.</param>
        /// <param name="filterQuery">Send this string to the filter query field and click "Go" button to perform filter.</param>
        /// <param name="rowNumber">Click the workspace link from the N-th row.</param>
        public void FilterByAndClickWorkspaceLink(string filterByText = null, string filterQuery = null, int rowNumber = 1)
        {
            // Select filter by option
            if (filterByText != null)
            {
                FilterBy.SelectByText(filterByText);
            }

            // Enter filter query
            if (filterQuery != null)
            {
                FilterQuery.Clear();
                FilterQuery.SendKeys(filterQuery);
                ClickGoButton();
            }

            ClickLinkToProjectWorkspace(rowNumber);
        }
    }
}
