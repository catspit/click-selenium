﻿using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.PortalDriver
{
    public class PortalWindow : IPortalWindow
    {
        PortalDriver driver;

        public PortalWindow(PortalDriver Driver)
        {
            this.driver = Driver;            
        }

        public bool Is()
        {
            return (driver.WindowHandles.Count > 0);
        }

        public void New(Action act)
        {
            int numWindows = driver.WindowHandles.Count;
            act();
            new WebDriverWait(driver, TimeSpan.FromSeconds(30)).Until(d => d.WindowHandles.Count > numWindows);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            driver.WaitUntilPageLoaded();
        }

        public void Close(Action act)
        {
            int numWindows = driver.WindowHandles.Count;
            act();
            new WebDriverWait(driver, TimeSpan.FromSeconds(30)).Until(d => d.WindowHandles.Count < numWindows);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
        }

        public void Close()
        {
            int numWindows = driver.WindowHandles.Count;
            driver.Close();
            new WebDriverWait(driver, TimeSpan.FromSeconds(30)).Until(d => d.WindowHandles.Count < numWindows);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
        }
    }
}
