﻿using System;
using OpenQA.Selenium;

namespace ClickPortal.BaseTypes
{
    /**
     * Project Editor includes:
     * - Edit/View Project
     * - Printer Version
     * - View Differences
     */
    public class ProjectEditor
    {
        public const string PROJECT_EDITOR_CSS_SELECTOR = "table[class='ProjectEditorTable'] > tbody > tr > td > a";
        public const string CLOSE_BUTTON_ID = "btnClose";
        private readonly int _index;
        private readonly string _altText;
        private readonly string _imageFileName;
        private readonly IWebDriver _driver;
        private IWebElement _linkElement;
        
        public IWebElement LinkElement
        {
            get
            {
                if (_linkElement != null)
                {
                    return _linkElement;
                }
                // Initialize _element
                if (_index > -1)
                {
                    _linkElement = GetProjectCreatorByIndex();
                }
                else if (_altText != null)
                {
                    _linkElement = GetProjectCreatorByAltText();
                }
                else if (_imageFileName != null)
                {
                    _linkElement = GetProjectCreatorByImageFileName();
                }
                return _linkElement;
            }
        }

        private IWebElement GetProjectCreatorByIndex()
        {
            var creators = _driver.FindElements(By.CssSelector(PROJECT_EDITOR_CSS_SELECTOR));
            return creators[_index];
        }

        private IWebElement GetProjectCreatorByAltText()
        {
            var image =
                _driver.FindElement(By.CssSelector(PROJECT_EDITOR_CSS_SELECTOR + " > img[alt='" + _altText + "']"));
            return image.FindElement(By.XPath(".."));
        }

        private IWebElement GetProjectCreatorByImageFileName()
        {
            var image =
                _driver.FindElement(By.CssSelector(PROJECT_EDITOR_CSS_SELECTOR + " > img[src*='" + _imageFileName + "']"));
            return image.FindElement(By.XPath(".."));
        }

        public ProjectEditor(IWebDriver driver, int index = -1, string altText = null, string imageFileName = null)
        {
            if (index == -1 && altText == null && imageFileName == null)
            {
                throw new Exception("You need to pass at least one of the optional parameters.");
            }
            _driver = driver;
            _index = index;
            _altText = altText;
            _imageFileName = imageFileName;
        }

        public void ClickLink()
        {
            LinkElement.Click();
        }

        public void Close()
        {
            _driver.FindElement(By.Id(CLOSE_BUTTON_ID)).Click();
        }
    }
}
