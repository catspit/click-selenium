﻿using ClickPortal.PortalDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClickPortal.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.IRB
{
    public class Meeting : ClickEType
    {
        protected static int created = 0;

        [ETypeProperty("ID")]
        public string ID;

        [ETypeProperty("name")]
        public string Name;

        [ETypeProperty("company", RelatedEType = "Company", RelatedKeyName = "ID")]
        public string Company;

        [ETypeProperty("createdBy", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string CreatedBy;

        [ETypeProperty("customAttributes.dateTimeStart")]
        public string Start;

        [ETypeProperty("customAttributes.location")]
        public string Location;

        [ETypeProperty("status", RelatedEType = "ProjectStatus")]
        public string Status;

        public Committee Commitee;
        public string ResourceContainerTemplateID = "TMPL4D60CE2ECBC00";
        public string ResourceContainerParent = "CLICK_IRB_HOME";

        private bool isStandAloneCommittee;

        public IRBMeetingsWorkspace WorkSpace
        {
            get;
            set;
        }

        

        private int meetingNumber;

        public Meeting() : this(new Committee())
        {
            isStandAloneCommittee = false;
        }

        public Meeting(Committee committee) : base("_Meeting")
        {
            meetingNumber = ++created;
            ID = "MEETING" + meetingNumber;
            Name = "Test Meeting " + meetingNumber;
            Start = DateTime.Now.AddDays(5).ToLongDateString();
            Status = "Scheduled";
            Company = "test college";
            CreatedBy = "administrator";
            Location = "Test Location";
            isStandAloneCommittee = true;
            this.Commitee = committee;

        }

        public override void CreateEntity(PortalDriver.PortalDriver Driver)
        {
            if (!isStandAloneCommittee)
            {
                this.Commitee.Company = this.Company;
                 this.Commitee.CreateEntity(Driver);
            }
            base.CreateEntity(Driver);
            FindWorkspace(Driver);
        }

        public override void DestroyEntity(PortalDriver.PortalDriver Driver)
        {
            if (!isStandAloneCommittee || this.Commitee != null)
            {
                this.Commitee.DestroyEntity(Driver);
            }
            base.DestroyEntity(Driver);
        }

        public void FindWorkspace(PortalDriver.PortalDriver Driver)
        {
            string getWorkspaceOidScript = @"?wom.getEntityFromString(""" + this.OID + @""").getQualifiedAttribute(""resourceContainer"");";
            string workspaceOid = Driver.AdminDriver.Console().Execute(getWorkspaceOidScript);
            this.WorkSpace = new IRBMeetingsWorkspace(workspaceOid);
        }

        public override string CreateScript
        {
            get
            {
                string script = base.CreateScript;
                script += @"
var committee = wom.getEntityFromString("""+this.Commitee.OID + @""");
etype.setQualifiedAttribute(""customAttributes.committee"",committee);
var rc = ResourceContainer.createFromResource(etype, getElements(""ContainerTemplateForID"", ""ID"", """ + ResourceContainerTemplateID + @""")(1));
rc.setQualifiedAttribute(""parent"", getElements(""ContainerBaseForID"", ""ID"", """ + ResourceContainerParent + @""")(1));
etype.setQualifiedAttribute(""resourceContainer"", rc);
etype.processInitialStateTransition();";
                return script;
            }
        }

        public String AddAgendaItem(ClickEType project, IPortalDriver Driver)
        {
            return Driver.Store().Execute(@"var meeting = wom.getEntityFromString(""" + this.OID + @""");
var project = wom.getEntityFromString(""" + project.OID + @""");
var agendaItem = _AgendaItem.createEntity();
agendaItem.setQualifiedAttribute(""customAttributes.meeting"", meeting);
agendaItem.setQualifiedAttribute(""customAttributes.project"", project);
meeting.setQualifiedAttribute(""customAttributes.agendaItems"", agendaItem, ""add"");
?agendaItem;");
        }
    }

    public class IRBMeetingsWorkspace : PageController
    {
        public String OID;

        public IRBMeetingsWorkspace(String Oid)
        {
            this.OID = Oid;
        }

        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=" + OID);
        }

        public void OpenActivity(String LinkText)
        {
            IWebElement anchor = new WebDriverWait(driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementExists(By.LinkText(LinkText)));
            OpenPopup(anchor, null, By.XPath("//td[@class='FormHead']"));
        }
    }
}
