﻿using ClickPortal;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ClickPortal.Pages;
using ClickPortal.IACUC.Protocol.Pages;
using ClickPortal.IACUC.ResearchTeam;
using ClickPortal.PortalDriver;
using Newtonsoft.Json;

namespace ClickPortal.IACUC.Protocol
{
    public class IACUCProtocol : ClickEType
    {
        protected static int created = 0;

        [ETypeProperty("ID")]
        public string ID;
        [ETypeProperty("name")]
        [ETypeProperty("customAttributes.fullTitle")]
        public string Name;
        [ETypeProperty("description")]
        public string Description;
        [ETypeProperty("company", RelatedEType = "Company")]
        public string Company;
        [ETypeProperty("customAttributes.adminOffice", RelatedEType = "_AdminOffice")]
        public string AdminOffice;
        [ETypeProperty("status", RelatedEType = "ProjectStatus")]
        public string Status;
        [ETypeProperty("createdBy", RelatedEType = "Person", RelatedKeyName = "userId")]
        [ETypeProperty("customAttributes.investigator.customAttributes.studyTeamMember", RelatedEType = "Person", RelatedKeyName = "userId")]
        [ETypeProperty("customAttributes.primaryContact.customAttributes.studyTeamMember", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string InvestigatorUserID;
        [ETypeProperty("customAttributes.researchTeam", RelatedEType = "_ClickResearchTeam", RelatedKeyName = "name")]
        public IACUCResearchTeam ResearchTeam;
        [ETypeProperty("owner", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string Owner;
        [ETypeProperty("dateEnteredState")]
        public string DateEnteredState;
        [ETypeProperty("customAttributes.typeOfProtocol", RelatedEType = "_ClickProtocolType", RelatedKeyName = "customAttributes.name")]
        public string ProtocolType;
        [ETypeProperty("customAttributes.typeOfSubmission", RelatedEType = "_SubmissionType", RelatedKeyName = "ID")]
        public string SubmissionType;
        [ETypeProperty("customAttributes.committee", RelatedEType = "_Committee", RelatedKeyName = "name")]
        public string Committee;
        [ETypeProperty("customAttributes.hasBreeding")]
        public string HasBreeding;
        [ETypeProperty("customAttributes.lastDayOfAnnualReviewPeriod")]
        public string LastDayOfAnnualReviewPeriod;
        [ETypeProperty("customAttributes.lastDayOfTriennialApprovalPeriod")]
        public string LastDayOfTriennialApprovalPeriod;
        [ETypeProperty("customAttributes.hasBatchGracePeriodNotificationSent")]
        public bool HasBatchGracePeriodNotificationSent;

        public IACUCProtocol ParentProtocol;

        public string ResourceContainerTemplateID = "TMPL8D02B5766D47C23";
        public string ResourceContainerParent = "CLICK_IACUC_HOME";
        public string CreateView = "VIEW8D02C1F841BC560";

        private ProtocolWorkspace workspace;
        private IACUCProtocolForm _protocolForm;
        protected int protocolNumber;


        public IACUCProtocol() : base("_ClickIACUCSubmission")
        {
            this.protocolNumber = ++created;
            this.ID = "TEST-" + protocolNumber;
            this.Name = this.Description = "Test Protocol " + protocolNumber;
            this.Company = "Test Company";
            this.InvestigatorUserID = this.Owner = "administrator";
            this.DateEnteredState = DateTime.Now.ToShortDateString();
            this.ProtocolType = "Teaching";
            this.HasBreeding = "no";
            this.ResearchTeam = new IACUCResearchTeam();
        }

        public IACUCProtocol(IACUCProtocol source) : this()
        {
            this.OID = source.OID;
            this.ID = source.ID;
            this.Name = source.Name;
            this.Description = source.Description;
            this.Company = source.Company;
            this.AdminOffice = source.AdminOffice;
            this.Status = source.Status;
            this.InvestigatorUserID = source.InvestigatorUserID;
            this.Owner = source.Owner;
            this.ProtocolType = source.ProtocolType;
            this.Workspace = new ProtocolWorkspace(source.Workspace.OID);
            this.HasBreeding = source.HasBreeding;
            this.ResearchTeam = source.ResearchTeam;
        }

        public IACUCProtocol(String ID) : this()
        {
            this.ID = ID;
        }

        public ProtocolWorkspace Workspace
        {
            get
            {
                if (workspace == null)
                {
                    //FindWorkspaceFromProtocolList(protocolList);
                }
                return workspace;
            }
            set
            {
                this.workspace = value;
            }
        }

        public IACUCProtocolForm ProtocolForm
        {
            get
            {
                if (_protocolForm == null)
                {
                    _protocolForm = new IACUCProtocolForm(this);
                }
                return _protocolForm;
            }
        }

        public override string CreateScript
        {
            get
            {
                string script = @"var etype = wom.createTransientEntity(""_ClickIACUCSubmission"");
etype.registerEntity();
etype.initialize();";
                string propertiesThatMustBeAddedAtTheEnd = "";
                List<string> optionalParams = new List<string>();

                int relatedCount = 0;
                foreach (FieldInfo field in this.GetType().GetFields())
                {
                    foreach (object attr in field.GetCustomAttributes(true))
                    {
                        if (attr is ETypeProperty && field.GetValue(this) != null)
                        {
                            if (((ETypeProperty)attr).RelatedEType != null)
                            {
                                string fieldname = ((ETypeProperty)attr).RelatedKeyName == null ? "ID" : ((ETypeProperty)attr).RelatedKeyName;
                                switch (((ETypeProperty)attr).PropertyName)
                                {
                                    case "owner":
                                        script+="\nvar entOwnerPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entOwnerPerson == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "company":
                                        script+="\nvar entCompany = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entCompany == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "createdBy":
                                        script+="\nvar entCreatedByPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""").elements().item(1);
if (entCreatedByPerson == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "status":
                                        propertiesThatMustBeAddedAtTheEnd += "\nvar related" + ++relatedCount + " = getResultSet(\""
                                    + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                    + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @" AND projectType='_ClickIACUCSubmission'"
                                    + @""");
if (related" + relatedCount + @".count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }" + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + "related" + relatedCount + ".elements()(1)" + ");"
                                    + "\netype.setWorkspaceTemplate();";
                                        break;
                                    case "customAttributes.researchTeam":
                                        script+="\nvar researchTeam = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "='" + this.ResearchTeam.Name + "'"
                                    + @""").elements().item(1);
if (researchTeam == null) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }";
                                        break;
                                    case "customAttributes.typeOfSubmission":
                                        propertiesThatMustBeAddedAtTheEnd += "\nvar typeOfSubmission = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'') + "\");"
                                            + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", typeOfSubmission.elements()(1));";
                                        break;
                                    default:
                                        optionalParams.Add("\nvar related" + ++relatedCount + " = getResultSet(\""
                                    + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                    + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""");
if (related" + relatedCount + @".count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }" + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + "related" + relatedCount + ".elements()(1)" + ");");
                                        break;
                                }
                            }
                            else
                            {
                                switch (((ETypeProperty)attr).PropertyName)
                                {
                                    case "ID":
                                        propertiesThatMustBeAddedAtTheEnd += "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");";
                                        break;

                                    default:
                                        optionalParams.Add("\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");");
                                        break;
                                }
                            }
                        }
                    }
                }
                
                script += "\netype.setRequiredFields(entOwnerPerson, entCompany, entCreatedByPerson);";
                script += "\netype.setQualifiedAttribute(\"customAttributes.researchTeam\", researchTeam);";
                foreach (string snip in optionalParams)
                {
                    script += snip;
                }
                script += @"
var rc = ResourceContainer.createFromResource(etype, getElements(""ContainerTemplateForID"", ""ID"", """+ResourceContainerTemplateID+@""")(1));
rc.setQualifiedAttribute(""parent"", getElements(""ContainerBaseForID"", ""ID"", """ + ResourceContainerParent + @""")(1));
etype.setQualifiedAttribute(""resourceContainer"", rc);
var sch = ShadowSCH.getSCH();
var view = wom.getEntityFromString(""com.webridge.entity.Entity[OID[3A2CF4C1A9565E4E8CCDCBCFE4110B89]]"");
wom.putContext(""currentView"", view, true);
ProjectStateTransition.processProject(sch, etype, etype, null);
" + propertiesThatMustBeAddedAtTheEnd+@"
?etype;";
                return script;
            }
        }

        public override void CreateEntity(PortalDriver.PortalDriver Driver)
        {
            if (this.ResearchTeam != null)
            {
                this.ResearchTeam.CreateEntity(Driver);
            }
            base.CreateEntity(Driver);
            FindWorkspace(Driver);
        }

        public void FindWorkspace(PortalDriver.PortalDriver Driver)
        {
            string getWorkspaceOidScript = @"?wom.getEntityFromString(""" + this.OID + @""").getQualifiedAttribute(""resourceContainer"");";
            string workspaceOid = Driver.AdminDriver.Console().Execute(getWorkspaceOidScript);
            this.Workspace = new ProtocolWorkspace(workspaceOid);
        }

        public void ApproveProtocol(PortalDriver.PortalDriver driver)
        {
            string script = @"var project = wom.getEntityFromString(""" + OID + @""");
                            var finalVersion = project.getQualifiedAttribute(""customAttributes.correspondenceLetter.customAttributes.finalVersion"");
                            var reviewType = project.getQualifiedAttribute(""customAttributes.reviewType"");
                            var dateSubmitted = project.getQualifiedAttribute(""customAttributes.dateSubmitted"");
                            if( finalVersion == null ){
                                var template = ApplicationEntity.getResultSet(""_ClickIACUCCustomFileTemplate"").query(""id = 'IACUC00000007'"").elements().item(1).getQualifiedAttribute(""customAttributes.templateFile"");
                                project.setQualifiedAttribute(""customAttributes.correspondenceLetter.customAttributes.draftVersion"", template);
                                project.setQualifiedAttribute(""customAttributes.correspondenceLetter.customAttributes.finalVersion"", template);
                            }
                            if( dateSubmitted == null ){
                                project.setQualifiedAttribute(""customAttributes.dateSubmitted"", new Date());
                            }
                            if( reviewType == null ){
                                reviewType = ApplicationEntity.getResultSet(""_ClickIACUCReviewType"").query(""id = 'ID00000003'"").elements().item(1);
                                project.setQualifiedAttribute(""customAttributes.reviewType"", reviewType);
                            }
                            var approved = ApplicationEntity.getResultSet(""_ClickDetermination"").query(""id = 'APPROVED'"").elements().item(1);
                            project.setQualifiedAttribute(""customAttributes.determination"", approved);
                            var activityType = ActivityType.getActivityType(""_ClickIACUCSubmission_SendLetter"", ""_ClickIACUCSubmission"");
                            project.logActivity(ShadowSCH.getRealOrShadowSCH(), activityType, null);";

            string output = driver.AdminDriver.Console().Execute(script);
            if (!String.IsNullOrEmpty(output))
            {
                throw new Exception("Error approving protocol: " + output);
            }

            script = @"? wom.getEntityFromString(""" + OID + @""").getQualifiedAttribute(""status.id"")";

            output = driver.AdminDriver.Console().Execute(script);

            if (!output.Equals("Approved"))
            {
                throw new Exception("Error approving protocol, invalid final state: " + output + ". Invalid initial conditions?");
            }

            Status = "Approved";
        }

        public new String OID
        {
            get
            {
                if (base.OID == default(String))
                {
                    this.Workspace.Navigate();
                    base.OID = this.Workspace.projectOid;
                }
                return base.OID;
            }
            set
            {
                base.OID = value;
            }
        }

        public override void DestroyEntity(PortalDriver.PortalDriver Driver)
        {
            if (this.ResearchTeam != null)
            {
                this.ResearchTeam.DestroyEntity(Driver);
            }
            base.DestroyEntity(Driver);
        }
    }
}
