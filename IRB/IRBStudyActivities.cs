﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using ClickPortal.Pages;
using System.Threading;

namespace ClickPortal.IRB
{
    public class IRBSubmissionActivity : PageController
    {
        protected String ActivityText;

        protected IRBSubmissionActivity(String actText) : base()
        {
            ActivityText = actText;
        }

        public void Submit()
        {
            int windowCount = driver.WindowHandles.Count;
            driver.Window().Close(driver.FindElement(By.Id("okBtn")).Click);
        }

        public bool IsAvailable()
        {
            return driver.FindElements(By.LinkText(ActivityText)).Count > 0;
        }
    }

    public class ActivityErrorException : Exception 
    {
        public ActivityErrorException()
        {
            ;
        }

        public ActivityErrorException(string msg) : base(msg)
        {
            ;
        }

        public ActivityErrorException(string msg, Exception inner) : base(msg, inner)
        {
            ;
        }
    }


    public class SubmitDesignatedReviewActivity : IRBSubmissionActivity
    {
        public SubmitDesignatedReviewActivity() : base("Submit Designated Review")
        {
            ;
        }

        public bool ConflictingInterestCheckbox
        {
            get
            {
                return driver.FindElement(By.Name("_IRBSubmission_SubmitDesignatedReview.loggedFor.customAttributes.nonCommitteeReviewChecklist.customAttributes.conflictingInterest")).Selected;
            }
            set
            {
                IWebElement checkbox = driver.FindElement(By.Name("_IRBSubmission_SubmitDesignatedReview.loggedFor.customAttributes.nonCommitteeReviewChecklist.customAttributes.conflictingInterest"));
                if (checkbox.Selected != value)
                {
                    checkbox.Click();
                }
            }
        }

        public String Determination
        {
            get
            {
                IWebElement table = driver.FindElement(By.XPath("//*[@id='com.webridge.entity.Entity[OID[C1114D0E3D50454BB3F298898984A703]]_control']/table/tbody"));
                ReadOnlyCollection<IWebElement> rows = table.FindElements(By.TagName("tr"));
                foreach (IWebElement row in rows)
                {
                    ReadOnlyCollection<IWebElement> cells = row.FindElements(By.TagName("td"));
                    IWebElement rb = cells[0].FindElement(By.TagName("input"));
                    if (rb.Selected)
                    {
                        return cells[1].Text;
                    }
                }
                return null;
            }
            set
            {
                IWebElement table = driver.FindElement(By.XPath("//*[@id='com.webridge.entity.Entity[OID[C1114D0E3D50454BB3F298898984A703]]_control']/table/tbody"));
                ReadOnlyCollection<IWebElement> rows = table.FindElements(By.TagName("tr"));
                bool found = false;
                foreach (IWebElement row in rows)
                {
                    ReadOnlyCollection<IWebElement> cells = row.FindElements(By.TagName("td"));
                    if (cells[1].Text == value)
                    {
                        IWebElement rb = cells[0].FindElement(By.TagName("input"));
                        rb.Click();
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    throw new Exception("Attempted to set Determination that does not exist.");
                }
            }
        }

        public bool ReadyToSubmit
        {
            get
            {
                ReadOnlyCollection<IWebElement> rbs = driver.FindElements(By.Name("_IRBSubmission_SubmitDesignatedReview.customAttributes.readyForSubmission"));
                foreach (IWebElement rb in rbs)
                {
                    if (rb.GetAttribute("value") == "yes")
                    {
                        return true;
                    }
                }
                return false;
            }
            set
            {
                string rbVal = "yes";
                if (value == false)
                {
                    rbVal = "no";
                }
                driver.FindElement(By.XPath("//input[@name='_IRBSubmission_SubmitDesignatedReview.customAttributes.readyForSubmission'][@value='" + rbVal + "']")).Click();
            }
        }

    }

    public class PrepareLetterActivity : IRBSubmissionActivity
    {
        public PrepareLetterActivity() : base("Prepare Letter")
        {
            ;
        }

        public String DraftTemplate
        {
            get
            {
                SelectElement dropdown = new SelectElement(driver.FindElement(By.Name("_IRBSubmission_PrepareLetter.customAttributes.selectedFileTemplate")));
                return dropdown.SelectedOption.Text;
            }
            set
            {
                SelectElement dropdown = new SelectElement(driver.FindElement(By.Name("_IRBSubmission_PrepareLetter.customAttributes.selectedFileTemplate")));
                dropdown.SelectByText(value);
            }
        }

        public void GenerateDraft()
        {
            String currentDraft = null;
            if (driver.FindElements(By.XPath("//*[@id='com.webridge.entity.Entity[OID[DE52418B31346B4FA1D48F59ADAFF75B]]_display']/a")).Count > 0)
            {
                currentDraft = driver.FindElement(By.XPath("//*[@id='com.webridge.entity.Entity[OID[DE52418B31346B4FA1D48F59ADAFF75B]]_display']/a")).Text;
            }
            IWebElement button = driver.FindElement(By.XPath("//input[@value='Generate']"));
            button.Click();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));

            if (currentDraft != null)
            {
                wait.Until(ExpectedConditions.ElementExists(By.XPath("//*[@id='com.webridge.entity.Entity[OID[DE52418B31346B4FA1D48F59ADAFF75B]]_display']/a[text() != '" + currentDraft + "']")));
            }
            else
            {
                wait.Until(ExpectedConditions.ElementExists(By.XPath("//*[@id='com.webridge.entity.Entity[OID[DE52418B31346B4FA1D48F59ADAFF75B]]_display']/a")));
            }
        }
    }

    public class SendLetterActivity : IRBSubmissionActivity
    {
        public SendLetterActivity() : base("Send Letter")
        {
            ;
        }
    }

    public class AssignCoordinatorActivity : IRBSubmissionActivity
    {
        public AssignCoordinatorActivity() : base("Assign Coordinator")
        {
            ;
        }

        public ReadOnlyCollection<IWebElement> Rows
        {
            get
            {
                if (driver.Store().portalVersionMajor == 8)
                {
                    return driver.FindElements(By.XPath("//div[@id='__IRBSubmission_AssignOwner.loggedFor.owner_container']/span[3]/table/tbody/tr"));
                }

                return driver.FindElements(By.XPath("//*[@id='_webr_EntityView']/table/tbody/tr"));
            }
        }

        public ReadOnlyCollection<String> CoordinatorNames
        {
            get
            {
                ReadOnlyCollection<IWebElement> coordinatorRows = driver.FindElements(By.XPath("//*[@id='__IRBSubmission_AssignOwner.loggedFor.owner_container']/span[contains(@id,'_control')]/table/tbody/tr"));
                List<String> coordinatorNames = new List<String>();
                foreach (IWebElement row in coordinatorRows)
                {
                    ReadOnlyCollection<IWebElement> coordinatorCells = row.FindElements(By.TagName("td"));
                    String coordFirstName = coordinatorCells[1].Text;
                    String coordLastName = coordinatorCells[2].Text;
                    String coordName = coordFirstName + " " + coordLastName;
                    coordinatorNames.Add(coordName);
                    
                }

                return new ReadOnlyCollection<string>(coordinatorNames);
            }
        }

        public String AssignedCoordinator
        {
            get
            {
                foreach (IWebElement row in this.Rows)
                {
                    IWebElement radio = row.FindElement(By.XPath(".//input[@type='radio']"));
                    if (radio.Selected)
                    {
                        return row.FindElement(By.XPath(".//td[2]")).Text;
                    }
                }
                return null;
            }
            set
            {
                bool found = false;
                string[] names = value.Split(' ');
                
                foreach (IWebElement row in this.Rows)
                {
                    String firstName = row.FindElement(By.XPath(".//td[2]")).Text;
                    String lastName = row.FindElement(By.XPath(".//td[3]")).Text;
                    if (firstName.Equals(names[0]) && lastName.Equals(names[1]))
                    {
                        IWebElement radio = row.FindElement(By.XPath(".//td[1]/input[@type='radio']"));
                        radio.Click();
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    throw new Exception("Coordinator not listed.");
                }
            }
        }
    }

    public class SubmitActivity : IRBSubmissionActivity
    {
        public SubmitActivity() : base("Submit")
        {
            ;
        }

        public void Submit(String username, String password)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementExists(By.Id("okBtn"))).Click();

            Thread.Sleep(5000); //explicit sleep because even though frame exists, objects inside haven't loaded yet.  Switching to the frame and waiting 
                                // doesn't seem to fix the objects not loading (they don't seem to load at all if switched too soon).

            driver.SwitchTo().Frame("GB_frame_confirmLoginMsg");
            driver.FindElement(By.Id("confirmUserId")).SendKeys(username);
            driver.FindElement(By.Id("confirmPassword")).SendKeys(password);

            int numWindows = driver.WindowHandles.Count;
            driver.FindElement(By.Id("confirmLoginSubmitBtn")).Click();

            Thread.Sleep(5000); //explicit sleep because for some reason the WebDriverWait below just seems to fail instantly.  We wait for the window to close

            if (numWindows == driver.WindowHandles.Count)
            {
                driver.SwitchTo().ParentFrame();
            }

            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until((d) =>
            {
                if (numWindows > driver.WindowHandles.Count)
                {
                    // Window has closed, we're done here. 
                    driver.SwitchTo().Window(driver.WindowHandles[0]);
                    return true;
                }

                try { 
                    // Check for errors and throw an Exception if found.
                    ReadOnlyCollection<IWebElement> errors = d.FindElements(By.XPath("//span[@class='Error']"));
                    if (errors.Count > 0)
                    {
                        String msg = errors[0].Text;
                        throw new ActivityErrorException(msg);
                    }
                }
                catch (System.NullReferenceException)
                {
                    // Don't like this -- You'd expect Selenium to throw a ElemenetDoesNotExist or WindowDoesNotExist exception instead...
                    return true;
                }

                return false;
            });
        }
    }
}
