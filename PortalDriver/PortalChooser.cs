﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver
{
    internal class PortalChooser : IPortalChooser
    {
        private readonly IPortalDriver _driver;

        private int? _resultCount;

        public PortalChooser(IPortalDriver driver)
        {
            _driver = driver;
        }

        public int ResultCount
        {
            get
            {
                if (_resultCount == null)
                {
                    var text = _driver.FindElement(By.XPath("//tr[@valign='middle']/td[text()[contains(.,'of')]]")).Text;

                    _resultCount = int.Parse(text.Substring(text.IndexOf("of", System.StringComparison.Ordinal) + 3));
                }

                return _resultCount.Value;
            }
        }

        public void Select(IEnumerable<IDictionary<string, string>> options)
        {
            throw new NotImplementedException();
        }

        public void Filter(string filterBy, string text)
        {
            throw new NotImplementedException();
        }

        private void WaitForDRSV()
        {
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(5));
            wait.Until(ExpectedConditions.ElementExists(By.Id("_webrRSV_DIV_0")));
        }

        public void Next()
        {
            _driver.FindElement(By.ClassName("Icon-NextPage")).Click();
            WaitForDRSV();
        }

        public void Previous()
        {
            _driver.FindElement(By.ClassName("Icon-PrevPage")).Click();
            WaitForDRSV();
        }

        public void OK()
        {
            CloseWindowFromButton("OK");
        }

        public void Cancel()
        {
            CloseWindowFromButton("Cancel");
        }

        private void CloseWindowFromButton(string buttonValue)
        {
            _driver.Window().Close(_driver.FindElement(By.XPath("//input[@type='button'][@value='" + buttonValue + "']")).Click);
        }

        public bool ContainsOptions(IEnumerable<IDictionary<string, string>> options)
        {
            var chooserOptions = CurrentOptions();

            foreach (var chooserOption in chooserOptions)
            {
                var optionFound = false;

                foreach (var option in options)
                {
                    optionFound = option.Any(kvp => chooserOption[kvp.Key].Equals(kvp.Value));

                    if (optionFound)
                    {
                        break;
                    }
                }

                if (!optionFound)
                {
                    return false;
                }
            }

            return true;
        }

        private IEnumerable<IDictionary<string, string>> CurrentOptions()
        {
            List<Dictionary<string,string>> options = new List<Dictionary<string, string>>();

            var tableRows = _driver.FindElements(By.XPath("//div[@id='_webrRSV_DIV_0']/table/tbody/tr"));

            string[] headers = null;

            for (int i = 0; i < tableRows.Count; i++)
            {
                var row = tableRows[i];

                if (i == 0)
                {
                    var headerTds = row.FindElements(By.TagName("td"));
                    headers = new string[headerTds.Count - 1];
                    for (int j = 1; j < headerTds.Count; j++)
                    {
                        try
                        {
                            headers[j-1] = headerTds[j].FindElement(By.TagName("a")).Text.Trim();
                        }
                        catch (Exception)
                        {
                            headers[j - 1] = headerTds[j].Text.Trim();
                        }
                    }
                }
                else
                {
                    var tds = row.FindElements(By.ClassName("List"));
                    Dictionary<string, string> option = new Dictionary<string, string>();
                    options.Add(option);

                    for (int j = 1; j < tds.Count; j++)
                    {
                        option.Add(headers[j - 1], tds[j].Text);
                    }
                }
            }

            return options;
        }
    }
}
