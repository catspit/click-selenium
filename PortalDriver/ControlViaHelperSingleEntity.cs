﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.PortalDriver {
    internal class ControlViaHelperSingleEntity : IControlFactoryFromHelper {

        public IPortalControl create(ControlHelper helper, IPortalDriver driver) {
            if ("EntityRadio EntitySelect".Contains(helper.type)) return new ControlSingleEntity(helper);
            return null;
        }

        internal class ControlSingleEntity : ControlFromHelper {

            internal ControlSingleEntity(ControlHelper helper):base(helper) {
            }

            public override bool Is(IPortalControlValue value) {
                if (!string.IsNullOrEmpty(value.OID)) return false;
                return Is(value.Text);
            }

            public override bool Is(string textOrOID) {
                var text = textOrOID ?? "";
                if (text == "" && Helper.text == "") return true;
                return Helper.text.Trim().Contains(text.Trim());
            }

            public override IPortalControlValue Clear()
            {
                if (this.Type == "EntitySelect") new SelectElement(Helper.widgets.Last()).SelectByIndex(0);
                if (this.Type == "EntityRadio")
                {
                    IWebElement elem = Helper.widgets.Last();
                    string elemID = elem.GetAttribute("id");
                    try
                    {
                        IWebElement label = elem.FindElement(By.XPath("following-sibling::label[@for='" + elemID + "']"));
                        if (label != null)
                        {
                            label.Click();
                        }
                    }
                    catch(Exception)
                    {
                        elem.Click();
                    }

                }
                return null;
            }

            public override IPortalControlValue Val() {
                if (this.IsClear) return new PortalControlValue(null, "", true);
                return new PortalControlValue(Helper.OID, Helper.text.Trim(), false);
            }

            public override IPortalControlValue Val(string TextOrOID) {

                if (String.IsNullOrEmpty(TextOrOID))
                {
                    this.Clear();
                    return null;
                }

                string shortestMatchingText = null;
                var index = 0;

                for (var i=0; i < Helper.rangeOID.Count; i++)
                {
                    var oid = Helper.rangeOID[i];
                    var text = Helper.rangeText[i];

                    if (oid == TextOrOID)
                    {
                        index = i;
                        break;
                    }

                    if (!text.Contains(TextOrOID)) continue;
                    if (shortestMatchingText != null && shortestMatchingText.Length <= text.Length) continue;
                    shortestMatchingText = text;
                    index = i;
                }

                if(this.Type == "EntitySelect") new SelectElement(Helper.widgets.Last()).SelectByIndex(index);
                if (this.Type == "EntityRadio") {
                    IWebElement elem = Helper.widgets[index];
                    string elemID = elem.GetAttribute("id");
                    IWebElement label = elem.FindElement(By.XPath("following-sibling::label[@for='" + elemID+ "']"));
                    if (label != null)
                    {
                        label.Click();
                    }
                    else
                    {
                        elem.Click();
                    }
                }
                return null;
            }

            public override IPortalControlValue Val(DateTime date)
            {
                this.Val(date.ToString());
                return null;
            }

            public override IWebElement AddButton
            {
                get { throw new NotImplementedException(); }
            }
            public override IWebElement SelectButton
            {
                get { throw new NotImplementedException(); }
            }

        }
    }
}
