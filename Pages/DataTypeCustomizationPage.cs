﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using ClickPortal;
using ClickPortal.Pages;
using ClickPortal.PortalDriver;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.Pages
{
    public class DataTypeCustomizationPage : PageController
    {
        public String entityType;

        public DataTypeCustomizationPage(String entityType)
        {
            this.entityType = entityType;
        }

        public override void Navigate()
        {
            base.Navigate("/EntityTypeCustomization/EntityTypeDetails?EntityTypeName=" + this.entityType);
        }
    }
}
