﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClickPortal.Pages;

namespace ClickPortal.IACUC.Protocol.Pages
{
    public class IACUCProtocolForm : SmartFormPage
    {
        public IACUCProtocol Protocol;

        public IACUCProtocolBasicInformationForm BasicInformationForm
        {
            get { return new IACUCProtocolBasicInformationForm(this.Protocol); }
        }

        public IACUCProtocolAmendmentSummary AmendmentSummaryForm
        {
            get { return new IACUCProtocolAmendmentSummary(Protocol); }
        }

        public IACUCProtocolProtocolTeamMembers ProtocolTeamMembersForm
        {
            get { return new IACUCProtocolProtocolTeamMembers(this.Protocol); }
        }

        public IACUCProtocolFundingSources FundingSourcesForm
        {
            get { return new IACUCProtocolFundingSources(this.Protocol); }
        }

        public IACUCProtocolScientificAims ScientificAimsForm
        {
            get { return new IACUCProtocolScientificAims(this.Protocol);  }
        }

        public IACUCProtocolExperiments ExperimentsForm
        {
            get { return new IACUCProtocolExperiments(this.Protocol); }
        }

        public IACUCProtocolProcedurePersonnelAssigment ProcedurePersonnelAssignmentForm
        {
            get { return new IACUCProtocolProcedurePersonnelAssigment(this.Protocol); }
        }

        public IACUCProtocolStrains StrainsForm
        {
            get { return new IACUCProtocolStrains(this.Protocol); }
        }

        public IACUCProtocolAnimalJustification AnimalJustificationForm
        {
            get { return new IACUCProtocolAnimalJustification(this.Protocol); }
        }

        public IACUCProtocolAlternatives AlternativesForm
        {
            get { return new IACUCProtocolAlternatives(this.Protocol); }
        }

        public IACUCProtocolDuplication DuplicationForm
        {
            get { return new IACUCProtocolDuplication(this.Protocol); }
        }

        public IACUCProtocolHousing HousingForm
        {
            get { return new IACUCProtocolHousing(this.Protocol); }
        }

        public IACUCProtocolDisposition DispositionForm
        {
            get { return new IACUCProtocolDisposition(this.Protocol); }
        }

        public IACUCProtocolSupportingDocuments SupportingDocumentsForm
        {
            get { return new IACUCProtocolSupportingDocuments(this.Protocol); }
        }

        public IACUCProtocolBreeding BreedingForm
        {
            get { return new IACUCProtocolBreeding(this.Protocol); }
        }

        public IACUCProtocolAnnualReview AnnualReviewForm
        {
            get { return new IACUCProtocolAnnualReview(this.Protocol); }
        }

        public IACUCProtocolTriennialReviewSummary TriennialReviewSummary
        {
            get { return new IACUCProtocolTriennialReviewSummary(this.Protocol); }
        }

        public IACUCProtocolForm(IACUCProtocol protocol)
        {
            this.Protocol = protocol;
        }

        public override void FillOutUsingTestData()
        {
            throw new NotImplementedException();
        }
    }
}
