﻿
ControlHelpers = {}

ControlHelpers.endsWith = function (str, comp) {
    return str.substring(str.length - comp.length, str.length) === comp;
}

ControlHelpers.collars = function (start) {
    var suffix = "_clientSideError";
    var suffix_exclude = "_container";
    var attribute = null;
    var end = null;
    var prev = null;
    ControlHelpers.forward(start, function () {
        if (ControlHelpers.endsWith(this.id, suffix)) {
            end = this;
            attribute = this.id.slice(0, -suffix.length);
            return false;
        }
        else if (prev != null && ControlHelpers.endsWith(this.id, suffix_exclude)) {
            end = prev;
            attribute = start.id.replace("_", "").replace(/_container$/, "").replace(/_label$/, ""); // since we get id from _container, not _clientSideError, we need to remove leading '_'
            return false;
        }
        prev = this;
    })
    if (attribute)
        return [document.getElementById("_" + attribute + "_container"), end];
    return null;
}

ControlHelpers.collect = function (start) {
    var timeStart = new Date().getTime();

    var startEnd = ControlHelpers.collars(start);
    if (!startEnd)
        return null;
    tags = { all: [] };
    tags.CONTAINER = [startEnd[0]];

    ControlHelpers.forward(startEnd[0], function () {
        var elType = this.getAttribute("type") || "";
        var elName = this.tagName;

        var smoosh = elName + elType;
        if (!(smoosh in tags))
            tags[smoosh] = [];
        tags[smoosh].push(this);
        tags.all.push(this);

        if (this.id === "_span_ALD_caption") tags.CAPTION = [this];

        if (this === startEnd[1])
            return false;
    })

    var out = ControlHelpers.characterize(tags);

    console.log("elapsed:" + (new Date().getTime() - timeStart));
    console.log(tags);
    console.log("------------");
    return out;
}


ControlHelpers.characterize = function (tags) {
	console.dir(tags);
	if (tags.INPUTcheckbox) {
		if (tags.INPUTcheckbox.length === 1
        && (tags.INPUTcheckbox[0].value.indexOf("OID") === -1))
			return ControlHelpers.booleanCheckbox(tags);
		return ControlHelpers.entityCheckbox(tags);
	}

	if (tags.INPUTradio) {
		if (tags.INPUTradio.length >= 2
        && (tags.INPUTradio[0].value.indexOf("OID") === -1))
			return ControlHelpers.booleanRadio(tags);
		return ControlHelpers.entityRadio(tags);
	}


	if (tags.OPTION) {
		if (tags.OPTION.length >= 2
        && (tags.OPTION[1].value.indexOf("OID") === -1))
			return ControlHelpers.booleanSelect(tags);
		return ControlHelpers.entitySelect(tags);
	}

	if (tags.TEXTAREA)
		return ControlHelpers.textarea(tags);

	if (tags.IMG
    && tags.IMG[tags.IMG.length - 1].id.indexOf("ccCalendarButton") > -1)
		return ControlHelpers.datePicker();

	if (tags.INPUT && tags.INPUT[0].id.indexOf("]]_chooser") > -1)
		return ControlHelpers.chooser();

	if (tags.INPUTbutton
        && (tags.INPUTbutton[0].id.indexOf("]]_selectBtn") > -1)
        && tags.INPUTbutton[0].value === "Select...")
		return ControlHelpers.selectEntityButton();

	if (tags.INPUTbutton
        && (tags.INPUTbutton[0].id.indexOf("]]_selectBtn") > -1)
        && tags.INPUTbutton[0].value === "Add")
		return ControlHelpers.addEntityButton();

	if (tags.INPUTbutton
        && (tags.INPUTbutton[0].id.indexOf("]]_editBtn") > -1)
        && tags.INPUTbutton[0].value === "Add")
		return ControlHelpers.editEntityButton();

	// PORTAL 8
	if (tags.BUTTONbutton
        && (tags.BUTTONbutton[0].id.indexOf("]]_selectBtn") > -1))
		return ControlHelpers.selectEntityButton();

	if (tags.BUTTONbutton
        && (tags.BUTTONbutton[0].id.indexOf("]]_editBtn") > -1))
		return ControlHelpers.editEntityButton();

	if (tags.INPUTfile)
		return ControlHelpers.file();

	if (tags.INPUT && tags.INPUT.length === 1)
		return ControlHelpers.input(tags);
	
    if (!tags.INPUTradio && !tags.INPUTbutton && !tags.BUTTONbutton && !tags.INPUTcheckbox && !tags.INPUT) {
        return ControlHelpers.readOnly(tags);
    }

    if (tags.INPUThidden) {
        return ControlHelpers.entityTable(tags);
    }

    return "unknown control"
}

ControlHelpers.entityTable = function (tags) {
    var summary = ControlHelpers.createSummaryBase(tags);
    summary.type = "EntityTable";
    summary.widgets = tags.BUTTONbutton;
    return summary;
}

//GOLD
ControlHelpers.booleanRadio = function (tags) {
    var summary = ControlHelpers.createSummaryBase(tags);
    summary.type = "BooleanRadio";

    function booleanTextStyle(textChunk) {
        if (textChunk.indexOf("Yes") !== -1) return ["Yes", "No"];
        if (textChunk.indexOf("True") !== -1) return ["True", "False"];
        return null;
    }

    var rangeText = booleanTextStyle(tags.INPUTradio[0].parentNode.innerText);
    if (!rangeText) rangeText = booleanTextStyle(tags.INPUTradio[0].parentNode.parentNode.innerText)


    summary.rangeText = rangeText;
    summary.rangeOID = [tags.INPUTradio[0].value, tags.INPUTradio[1].value]

    var selectedIndex = null;
    if (tags.INPUTradio[0].checked) selectedIndex = 0;
    if (tags.INPUTradio[1].checked) selectedIndex = 1;

    if (selectedIndex !== null) {
        summary.selectedIndex = [selectedIndex]
        summary.text = rangeText[selectedIndex];
        summary.OID = tags.INPUTradio[selectedIndex].value;
        summary.valText = [summary.text];
        summary.valOID = [summary.OID];
    }

    summary.widgets = tags.INPUTradio;

    for (var i = 0; i < tags.A.length; i++) {
        if (tags.A[i].id === "lnkClear") summary.widgets.push(tags.A[i])
    }

    return summary;
}

ControlHelpers.trim = function (text) {
    return text.replace(/^\s+|\s+$/gm, '');
}


ControlHelpers.rowToValue = function (inRowElem) {
    var rowElem = inRowElem;
    while (rowElem && (rowElem.tagName !== 'TR')) rowElem = rowElem.parentNode;
    var text = [];
    for (var cell = ControlHelpers.firstElementChild(rowElem) ; cell; cell = ControlHelpers.nextElementSibling(cell)) {
        var shrunkenCell = cell.getAttribute("width");
        shrunkenCell = (shrunkenCell === "1%" || shrunkenCell === "1")
        var contents = ControlHelpers.trim(cell.textContent || cell.innerText);
        if (!contents && shrunkenCell) continue;
        //this is a Click 'control' cell, CONSIDER: check for end-most-ness 
        text.push(contents);
    }
    return text.join("|");
}



ControlHelpers.createSummaryBase = function (tags) {



    var summary = {
        type: "",
        OID: null,
        text: "",
        widgets: [],
        rangeText: [],
        rangeOID: [],
        valText: [],
        valOID: [],
        selected: [],
    }


    if (tags.CONTAINER) {
        summary.container = tags.CONTAINER;
        summary.caption = tags.CAPTION;


        var attribute = summary.container[0].id
        attribute = attribute.replace(/^__/, "_").replace(/_container$/, "");

        summary.attribute = attribute;
        summary.captionText = summary.caption[0].innerText;
    }

    return summary;
}


//GOLD
ControlHelpers.entityRadio = function (tags) {
    var summary = ControlHelpers.createSummaryBase(tags);
    summary.type = "EntityRadio";


    for (var i = 0; i < tags.INPUTradio.length; i++) {
        var radio = tags.INPUTradio[i];
        var text = ControlHelpers.rowToValue(radio);
        var OID = radio.value;
        var isChecked = radio.checked;

        summary.rangeText.push(text);
        summary.rangeOID.push(OID);
        if (isChecked) {
            summary.valText.push(text);
            summary.valOID.push(OID);
            summary.selected.push(i);
            summary.OID = OID;
            summary.text = text;
        }
    }


    summary.widgets = tags.INPUTradio;

    for (var i = 0; i < tags.A.length; i++) {
        if (tags.A[i].id === "lnkClear") summary.widgets.push(tags.A[i])
    }

    return summary;
}


//GOLD
ControlHelpers.booleanCheckbox = function (tags) {

    var summary = ControlHelpers.createSummaryBase(tags);
    summary.type = "BooleanCheckbox";

    var cbox = tags.INPUTcheckbox[0];
    summary.text = "";
    if (cbox.checked) {
        summary.OID = cbox.value;
        summary.valOID = [cbox.value];
        summary.valText = [""];
        summary.selected.push(0);
    }

    summary.widgets.push(cbox);
    summary.rangeText = ["", ""];
    summary.rangeOID = [null, cbox.value];

    return summary;
}


//GOLD
ControlHelpers.entityCheckbox = function () {

    var summary = ControlHelpers.createSummaryBase(tags);
    summary.type = "EntityCheckbox";

    for (var i = 0; i < tags.INPUTcheckbox.length; i++) {
        var cbox = tags.INPUTcheckbox[i];
        var text = ControlHelpers.rowToValue(cbox.parentNode.parentNode.parentNode);
        text = ControlHelpers.trim(text);

        var OID = cbox.value;
        var isChecked = cbox.checked;

        summary.rangeText.push(text);
        summary.rangeOID.push(OID);

        if (isChecked) {
            summary.valText.push(text);
            summary.valOID.push(OID);
            summary.selected.push(i);
        }
    }
    summary.OID = null;
    summary.text = summary.valText.join("\r\n");

    summary.widgets = tags.INPUTcheckbox;

    return summary;
}


//GOLD
ControlHelpers.booleanSelect = function (tags) {
    var summary = ControlHelpers.entitySelect(tags)
    summary.type = "BooleanSelect";
    return summary;
}

//GOLD
ControlHelpers.entitySelect = function (tags) {

    var summary = ControlHelpers.createSummaryBase(tags);
    summary.type = "EntitySelect";

    var idx = tags.SELECT[0].selectedIndex;
    var sel = tags.OPTION[idx];

    summary.widgets = summary.widgets.concat(tags.OPTION, tags.SELECT[0]);
    summary.valText.push(sel.innerText);
    summary.valOID.push(sel.value);
    summary.selected.push(idx);
    summary.text = ControlHelpers.trim(sel.innerText);
    summary.OID = sel.value;

    for (var i = 0; i < tags.OPTION.length; i++) {
        var thisOPTION = tags.OPTION[i];
        var text = ControlHelpers.trim(thisOPTION.innerText);
        if (!thisOPTION.value) text = '';
        summary.rangeText.push(text);
        summary.rangeOID.push(thisOPTION.value);
    }


    return summary
}

ControlHelpers.textarea = function (tags) {
    var summary = ControlHelpers.textControlHelper(tags, tags.TEXTAREA[0]);
    summary.type = "Textarea";
    return summary;
}


ControlHelpers.textControlHelper = function (tags, widget) {
    var summary = ControlHelpers.createSummaryBase(tags);
    var text = ControlHelpers.trim(widget.value);

    if (text) {
        summary.text = text;
        summary.valText = [text];
        summary.valOID = [null];
    }

    summary.widgets = [widget];
    return summary;
}

ControlHelpers.datePicker = function () {
    var summary = ControlHelpers.textControlHelper(tags, tags.INPUT[0]);
    summary.type = "DatePicker";
    return summary;
}

ControlHelpers.readOnly = function () {
    var summary = ControlHelpers.createSummaryBase(tags);
    var text = "";
    summary.type = "readOnly";
    summary.valOID = [null];
    var spanElems = tags.SPAN;
    if (spanElems && spanElems.length > 0) {
        for (var i = 0; i < spanElems.length; i++) {
            var elem = spanElems[i];
            if (elem.className == "PrintAnswer") {
                text = ControlHelpers.trim(elem.textContent);
                break;
            }
        }
    }
    summary.text = text;
    summary.valText = [text];
    return summary;
}


ControlHelpers.chooser = function () {
    var summary = ControlHelpers.createSummaryBase(tags);
    summary.type = "Chooser";

    var clearButton = null;
    var selectButton = null;
    var addButton = null;
    // Portal 8
    if (tags.BUTTONbutton != null && tags.BUTTONbutton != undefined) {
        for (var i = 0; i < tags.BUTTONbutton.length; i++) {
            var button = tags.BUTTONbutton[i];
            if (button.id && button.style.display != "none") {
                if (ControlHelpers.endsWith(button.id, "_clearBtn")) clearButton = button;
                if (ControlHelpers.endsWith(button.id, "_selectBtn")) selectButton = button;
                if (ControlHelpers.endsWith(button.id, "_addBtn")) addButton = button;
            }
        }
    }

    // Portal 6
    if (tags.INPUTbutton != null && tags.INPUTbutton != undefined) {
        for (var i = 0; i < tags.INPUTbutton.length; i++) {
            var button = tags.INPUTbutton[i];
            if (button.id && button.style.display != "none") {
                if (ControlHelpers.endsWith(button.id, "_clearBtn")) clearButton = button;
                if (ControlHelpers.endsWith(button.id, "_selectBtn")) selectButton = button;
                if (ControlHelpers.endsWith(button.id, "_addBtn")) addButton = button;

            }
        }
    }
    // When control for a Set of _Sel CDT property is Entity Table: Selection
    if (!selectButton) {
        selectButton = addButton;
    }

    if (!clearButton) {//is clear
        summary.OID = null;
        summary.text = "";
        summary.widgets = [tags.INPUT[0], selectButton]
    } else {
        var displayElem = null
        for (var i = 0; i < tags.SPAN.length; i++) {
            displayElem = tags.SPAN[i];
            if (displayElem.id && ControlHelpers.endsWith(displayElem.id, "_display")) break;
        }
        summary.text = ControlHelpers.trim(displayElem.innerText);
        summary.OID = tags.INPUThidden[0].value;
        summary.valOID = [summary.OID];
        summary.valText = [summary.text];
        summary.widgets = [selectButton, clearButton];
    }
    return summary;
}



ControlHelpers.file = function () {
    return "file"
}

ControlHelpers.input = function () {
    var summary = ControlHelpers.textControlHelper(tags, tags.INPUT[0]);
    summary.type = "Input";
    return summary;
}


ControlHelpers.selectEntityButton = function () {
    return "selectEntityButton"
}

ControlHelpers.addEntityButton = function () {
    return "addEntityButton"
}

ControlHelpers.editEntityButton = function () {
    return "editEntityButton"
}




ControlHelpers.nextElementSibling = function (el) {
    if (el.nextElementSibling)
        return el.nextElementSibling;
    do {
        el = el.nextSibling
    } while (el && el.nodeType !== 1);
    return el;
}

ControlHelpers.firstElementChild = function (el) {
    if (el.firstElementChild)
        return el.firstElementChild;
    var el = el.firstChild;
    if (!el)
        return null;
    return this.nextElementSibling(el);
}

portalNormalizeSpace = function (text) {
    return text.replace(/[^\S]+/g, ' ').replace(/^\s+|\s+$/g, '');
}


portalFindElementsByCaption = function (byText) {

    var prints = [];
    if (document.getElementsByClassName)
        prints = document.getElementsByClassName('PrintQuestion');
    else
        prints = document.querySelectorAll('.PrintQuestion');

    byText = portalNormalizeSpace(byText);
    var matches = [];
    for (var i = 0; i < prints.length; i++) {
        var elem = prints[i];
        if (elem.id == '_span_ALD_caption') {
            var innerText = portalNormalizeSpace(elem.innerText)
            if (innerText.indexOf(byText) != -1) matches.push(elem.parentElement);
        }
    }
    return matches;
}



portalFindElementsByAttributeTail = function (tailText) {
    // Readonly controls don't have _clientSideError elements, need to look for _container directly
    var container = document.querySelector("[id$='" + tailText + "_container']");
    if (container != null && container != undefined) {
        console.log("TERRY-inside2")
        return [container];
    }
    var errorElems = document.getElementsByClassName("Error");
    var upperTailText = (tailText + "_clientSideError").toUpperCase()
    for (var i = 0; i < errorElems.length; i++) {
        var elem = errorElems[i];
        if (!elem.id) continue;
        console.log("elem.id:" + elem.id)
        if (!ControlHelpers.endsWith(elem.id, "_clientSideError")) continue;
        var upperId = elem.id.toUpperCase()
        if (ControlHelpers.endsWith(upperId, upperTailText)) return [elem.parentElement];
    }
    return null;
}



ControlHelpers.forward = function (elem, callback) {

    for (var idx = 0, depth = 0; elem; idx++) {
        //if node has children, get the first child
        //var nextElem = nextElementSibling(ch);
        if (callback.call(elem, idx, depth) === false)
            return; //early break

        var child = ControlHelpers.firstElementChild(elem);
        if (child) {
            elem = child;
            depth++;
            continue;
        }

        var next = ControlHelpers.nextElementSibling(elem);
        if (next) {
            elem = next;
            continue;
        }

        //up the tree until a sibling is located
        do {
            elem = elem.parentNode;
            depth--;
            //if we are back at document.body, return!
            if (elem === document.body)
                return;
            next = ControlHelpers.nextElementSibling(elem);
        } while (!next)
        elem = next;
    }
}


ControlHelpers.seleniumControl = function (elem) {
    var summary = ControlHelpers.collect(elem);
    console.log(summary);
    if (summary.rangeText) {


        var out = [
            summary.type,
            summary.OID,
            summary.text,
            summary.captionText,
            summary.attribute,
            summary.widgets,
            summary.rangeText,
            summary.rangeOID,
            summary.valText,
            summary.valOID,
            summary.selected,
            summary.container,
            summary.caption
        ]

        for (var i = 5; i < out.length; i++) {
            if (!out[i].length) out[i] = null;
        }
        return out;
    }
    return summary + "";
}