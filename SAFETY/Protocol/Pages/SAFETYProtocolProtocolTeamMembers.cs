﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.SAFETY.Protocol.Pages
{
    public class SAFETYProtocolProtocolTeamMembers : SAFETYProtocolFormPage
    {
        public SAFETYProtocolProtocolTeamMembers(SAFETYProtocol protocol)
            : base(protocol)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[09539336FE4E034283E4DFF1D41D0B43]]");
        }
    }
}
