﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClickPortal.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClickPortal.IACUC
{
    public class MeetingSettings : PageController
    {
        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?container=com.webridge.entity.Entity%5BOID%5B8FB35316C0396A498D2A7B69390FB8A4%5D%5D");
        }

        public class Options : PageController
        {
            public override void Navigate()
            {
                base.Navigate(TabPath("8FB35316C0396A498D2A7B69390FB8A4", "E7EA5A1E66C9F3418FED8EDF217FF4FC"));
            }
        }

        public class NotificationSettings : PageController
        {
            public string FromAddress
            {
                get
                {
                    return
                        driver.FindElement(By.Name("_ClickCommitteeMeetingSettings.customAttributes.notificationFromAddress")).GetAttribute("value");
                }
            }

            public string ReplyToAddress
            {
                get
                {
                    return
                        driver.FindElement(By.Name("_ClickCommitteeMeetingSettings.customAttributes.notificationReplyToAddress")).GetAttribute("value");
                }
            }

            public override void Navigate()
            {
                base.Navigate(TabPath("8FB35316C0396A498D2A7B69390FB8A4", "1E17ED2F265A6A4784502F3196B13F1A"));
            }
        }

        public class WorkspaceTemplates : PageController
        {
            public string MeetingCompleteTemplate
            {
                get
                {
                    SelectElement select = new SelectElement(driver.FindElement(By.Name("com.webridge.entity.Entity[OID[B6D169B6AA9B3C46B7E2076BF02CC62F]]:customAttributes.templateMeetingComplete")));

                    return select.SelectedOption.Text;
                }
            }

            public string MeetingScheduledTemplate
            {
                get
                {
                    SelectElement select = new SelectElement(driver.FindElement(By.Name("com.webridge.entity.Entity[OID[B6D169B6AA9B3C46B7E2076BF02CC62F]]:customAttributes.templateMeetingScheduled")));

                    return select.SelectedOption.Text;
                }
            }

            public override void Navigate()
            {
                base.Navigate(TabPath("8FB35316C0396A498D2A7B69390FB8A4", "38425DF302F8C746AE1AF52633CD4C05"));
            }
        }
    }
}
