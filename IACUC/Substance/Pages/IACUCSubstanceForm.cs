﻿using ClickPortal.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Substance.Pages
{
    public class IACUCSubstanceForm : SmartFormPage
    {
        public IACUCSubstance Substance;

        public IACUCSubstanceInformation SubstanceInformationForm
        {
            get { return new IACUCSubstanceInformation(this.Substance); }
        }

        public IACUCSubstanceForm(IACUCSubstance substance)
        {
            this.Substance = substance;
        }
        public override void FillOutUsingTestData()
        {
            throw new NotImplementedException();
        }
    }
}
