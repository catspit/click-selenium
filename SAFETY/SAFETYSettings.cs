﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ClickPortal.Pages;
using OpenQA.Selenium;

namespace ClickPortal.SAFETY
{
    public class SmartformStepsSettings : PageController
    {
        public override void Navigate()
        {
            driver.FindElement(By.LinkText("Settings")).Click();
            driver.FindElement(By.LinkText("Safety Settings")).Click();
            IWebElement tab = driver.FindElement(By.XPath("//a[.='\u00A0SmartForm\u00A0']"));
            tab.Click();
        }

        public IWebElement CustomStepsTable
        {
            get { return driver.FindElement(By.Id("_webrRSV_DIV_0")).FindElement(By.TagName("table")); }
        }

        public IWebElement CustomViewsTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='SubmissionTab']/table[2]")); }
        }
    }
}
