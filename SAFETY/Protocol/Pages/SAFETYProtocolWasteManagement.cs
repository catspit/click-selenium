﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.SAFETY.Protocol.Pages
{
    public class SAFETYProtocolWasteManagement : SAFETYProtocolFormPage
    {
        public SAFETYProtocolWasteManagement(SAFETYProtocol protocol)
            : base(protocol)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _protocol.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[98A071F57C1006418826C4E2E007857A]]");
        }
    }
}
