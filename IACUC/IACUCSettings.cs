﻿using ClickPortal.Pages;
using OpenQA.Selenium;
using System;

namespace ClickPortal.IACUC
{
    public class GeneralSettingsPage : PageController
    {
        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[5940605F92A57242896131B1528FD165]]");
        }
        public bool AutoAssignCoordCheckbox
        {
            get { return driver.FindElement(By.Name("_IACUCSettings.customAttributes.assignCoordinatorToNewMods")).Selected; }
        }

        public bool CoordinatorCanEditCheckbox
        {
            get { return driver.FindElement(By.Name("_IACUCSettings.customAttributes.enableCoordinatorToEditStudy")).Selected; }
        }

        public bool PIProxiesCheckbox
        {
            get { return driver.FindElement(By.Name("_IACUCSettings.customAttributes.piProxy")).Selected; }
        }

        public bool VAResearchCheckbox
        {
            get { return driver.FindElement(By.Name("_IACUCSettings.customAttributes.vaSupport")).Selected; }
        }

        public bool PICanSelectIRBCheckbox
        {
            get { return driver.FindElement(By.Name("_IACUCSettings.customAttributes.showIRBOffice")).Selected; }
        }

        public bool CoordCanChangeOfficeCheckbox
        {
            get { return driver.FindElement(By.Name("_IACUCSettings.customAttributes.showAssignIRB")).Selected; }
        }

        public String SSRSReportServerURL
        {
            get { return driver.FindElement(By.Name("_IACUCSettings.customAttributes.reportServerUrl")).Text; }
        }

        public bool LockSystemCheckbox
        {
            get { return driver.FindElement(By.Name("_IACUCSettings.customAttributes.lockIRB")).Selected; }
        }

        public string CommonRuleEffectiveDate
        {
            get { return driver.FindElement(By.Name("_IACUCSettings.customAttributes.commonRuleEffectiveDate")).GetAttribute("value"); }
        }
    }
    public class SmartformStepsSettings : PageController
    {
        private string subTabName;
        private string subTabId;
        string DEFAULT_TAB_NAME = "SUBMISSION";
        public SmartformStepsSettings()
        {
            this.subTabName = DEFAULT_TAB_NAME;
            this.subTabId = GetSubTabId();
        }
        public SmartformStepsSettings(string tabName)
        {
            this.subTabName = tabName;
            this.subTabId = GetSubTabId();
        }
        private string GetSubTabId()
        {
            return this.subTabName.ToUpper().ToCharArray()[0] + this.subTabName.ToLower().Substring(1) + "Tab";
        }

        public override void Navigate()
        {
            base.Navigate("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[5940605F92A57242896131B1528FD165]]");
            IWebElement tab = driver.FindElement(By.XPath("//nav[contains(@id,'Tab')]/a[contains(text(),'SmartForm')]"));
            tab.Click();
            IWebElement subtab = driver.FindElement(By.XPath("//a[@href='#" + this.subTabId + "']"));
            subtab.Click();
        }

        public IWebElement CustomStepsTable
        {        
            get { return driver.FindElement(By.XPath("//*[@id='" + this.subTabId + "']/table[1]//div[contains(@id,'_webrRSV_DIV_')]/table")); }
        }

        public IWebElement CustomViewsTable
        {
            get { return driver.FindElement(By.XPath("//*[@id='" + this.subTabId + "']/table[2]")); }
        }
    }

}
