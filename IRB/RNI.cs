﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IRB
{
    public class RNI : IRBStudy
    {
        public List<String> RelatedStudies;
        public new string ResourceContainerTemplateID = "TMPL4DD28F7EDFC00";

        public RNI()
        {
            this.ID = "RNI-" + studyNumber;
            this.Name = this.Description = "Test RNI " + studyNumber;
            this.SubmissionType = "RNI";
            this.RelatedStudies = new List<string>();
        }

        public RNI(IRBStudy source) : base(source)
        {
            ;
        }

        public override string CreateScript
        {
            get
            {
                String create = base.CreateScript;
                for (int index = 0; index < this.RelatedStudies.Count; index++)
                {
                    create += @"var related" + index + @" = getResultSet(""_IRBSubmission"").query(""ID='" + this.RelatedStudies[index] + @"'"").elements()(1);
etype.setQualifiedAttribute(""customAttributes.reportableNewInformation.customAttributes.relatedStudies"", related" + index + @", ""add"");";
                    create += @"etype.setQualifiedAttribute(""customAttributes.readers"", related" + index + @".customAttributes.investigator.customAttributes.studyTeamMember, ""add"");";
                }
                return create;
            }
        }
    }
}
