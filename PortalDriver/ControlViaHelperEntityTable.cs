﻿using System;
using System.Collections;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver
{
    internal class ControlViaHelperEntityTable : IControlFactoryFromHelper
    {
        public IPortalControl create(ControlHelper helper, IPortalDriver driver)
        {
            if (helper.type == "EntityTable") return new ControlEntityTable(helper, driver);
            return null;
        }

        internal class ControlEntityTable : ControlFromHelper
        {

            internal ControlEntityTable(ControlHelper helper, IPortalDriver driver):base(helper,driver)
            {
            }

            public override bool Is(IPortalControlValue value)
            {
                throw new NotImplementedException();
            }

            public override bool Is(string textOrOID)
            {
                var text = textOrOID ?? "";
                if (text == "" && Helper.text == "") return true;
                return Helper.text.Trim().Contains(text.Trim());
            }

            public override string OID
            {
                get { throw new NotImplementedException(); }
            }

            public override string Text
            {
                get { throw new NotImplementedException(); }
            }

            public override bool IsClear
            {
                get { throw new NotImplementedException(); }
            }

            public override IEnumerator<IPortalControlValue> GetEnumerator()
            {
                throw new NotImplementedException();
            }

            public override IPortalControlValue this[int index]
            {
                get { throw new NotImplementedException(); }
            }

            public override int Count
            {
                get { throw new NotImplementedException(); }
            }

            public override IPortalControlValue Clear()
            {
                throw new NotImplementedException();
            }

            public override IPortalControlValue Val()
            {
                throw new NotImplementedException();
            }

            public override IPortalControlValue Val(string TextOrOID)
            {
                throw new NotImplementedException();
            }

            public override IPortalControlValue Val(DateTime date)
            {
                throw new NotImplementedException();
            }
            public override IWebElement SelectButton
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public override IWebElement AddButton
            {
                get
                {
                    var widgets = Helper.widgets;
                    if (widgets != null)
                    {
                        for (var i = 0; i < widgets.Count; i++)
                        {
                            var elem = widgets[i];
                            var elemId = elem.GetAttribute("id");
                            if (elemId != null && elemId.EndsWith("_addBtn"))
                            {
                                return elem;
                            }
                        }
                    }
                    return null;
                }
            }
        }
    }
}
