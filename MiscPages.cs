﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Collections.ObjectModel;
using System.Linq;
using ClickPortal.Pages;

namespace ClickPortal
{
    public class BulkImportPage : PageController
    {
        public override void Navigate()
        {
            base.Navigate("/CommonAdministration/BulkImport/ImportFile");
        }

        public void Upload(String filepath, Boolean multiple = false)
        {
            driver.FindElement(By.Id("fileupload")).SendKeys(filepath);
            if (multiple)
            {
                var checkbox = driver.FindElement(By.Name("processMultipleRange"));
                string checkboxID = checkbox.GetAttribute("id");
                IWebElement label = checkbox.FindElement(By.XPath("following-sibling::label[@for='" + checkboxID + "']"));
                if (label != null)
                {
                    label.Click();
                }
                else
                {
                    checkbox.Click();
                }
            }
            driver.FindElement(By.Name("ImportForm")).Submit();

            // Check the completed tab and wait for an Import Complete message
            base.Navigate("/CommonAdministration/BulkImport/ImportFile?tab=Completed");
            while (driver.FindElements(By.XPath("//*[@id='_webrRSV_DIV_0']/table/tbody/tr[2]/td[6]/a")).Count == 0)
            {
                try
                {
                    IWebElement cell = driver.FindElement(By.XPath("//*[@id='_webrRSV_DIV_0']/table/tbody/tr[2]/td[6]/span"));
                    if (cell.Text.StartsWith("Job Aborted."))
                    {
                        throw new Exception("Bulk import failed: " + cell.Text);
                    }
                } 
                catch (NoSuchElementException)
                {
                    Thread.Sleep(1000);
                    driver.Navigate().Refresh();
                }
            }

            String status = driver.FindElement(By.XPath("//*[@id='_webrRSV_DIV_0']/table/tbody/tr[2]/td[6]/a")).Text;
            if (status != "Import Completed.")
            {
                throw new Exception("Bulk Import did not complete successfully.");
            }
        }
    }

    public class AttachmentUploadForm : PageController
    {
        public AttachmentUploadForm(IWebElement launcher)
        {
            this.OpenPopup(launcher, "Add Attachment");
        }

        public void UploadAttachment(String filepath, String category = null)
        {
            IWebElement fileField = new WebDriverWait(driver, TimeSpan.FromSeconds(3)).Until(ExpectedConditions.ElementExists(By.Name("_Attachment.customAttributes.draftVersion.targetURL")));
            fileField.SendKeys(filepath);
            if (this.HasCategoryField())
            {
                if (category == null)
                {
                    throw new Exception("No category passed and this attachment form requires it.");
                }

                this.CategorySelect.SelectByText(category);
            }
            else
            {
                if (category != null)
                {
                    throw new Exception("Category passed but category field does not exist.");
                }
            }

            driver.Window().Close(this.OKButton.Click);
        }

        public IWebElement OKButton
        {
            get
            {
                return driver.FindElement(By.XPath("//input[@type='submit'][@name='ok_btnName']"));
            }
        }

        public SelectElement CategorySelect
        {
            get
            {
                return new SelectElement(driver.FindElement(By.Name("_Attachment.customAttributes.category")));
            }
        }

        public Boolean HasCategoryField()
        {
            return driver.FindElements(By.Name("_Attachment.customAttributes.category")).Count > 0;
        }
    }

    public class BaseChooser : PageController
    {
        public BaseChooser(IWebElement launcher)
        {
            driver.Window().New(launcher.Click);
        }

        public ReadOnlyCollection<IWebElement> Rows
        {
            get
            {
                return new ReadOnlyCollection<IWebElement>(driver.FindElements(By.XPath("//div[@id='_webrRSV_DIV_0']/table/tbody/tr")).Skip(1).ToList());
            }
        }

        public void Filter(String by, String searchValue)
        {
            new SelectElement(driver.FindElement(By.Id("_webrRSV_FilterField_0_0"))).SelectByText(by);
            driver.FindElement(By.Id("_webrRSV_FilterValue_0_0")).SendKeys(searchValue);
            driver.FindElement(By.Name("webrRSV__FilterGo_0")).Click();

            // Wait for the frame to reload.
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until((d) => { return ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"); });
        }

        public void SelectByIndex(int index, bool autoSubmit = true)
        {
            // Increment index by one to compensate for misrepresented header row.
            this.Rows[index].FindElement(By.TagName("input")).Click();

            if (autoSubmit) this.Submit();
        }

        public void SelectByInputText(String name, bool autoSubmit = true)
        {
            bool textFound = false;
            foreach (IWebElement row in this.Rows)
            {
                String rowInputText = row.FindElement(By.XPath(".//td[2]")).Text;
                if (rowInputText == name)
                {
                    row.FindElement(By.TagName("input")).Click();
                    textFound = true;
                    break;
                }
            }

            if (!textFound) throw new Exception("Failed to find input text in chooser table.");
            if (autoSubmit) this.Submit();
        }

        public void Submit()
        {
            driver.Window().Close(driver.FindElement(By.Id("btnOk")).Click);
        }

        ~BaseChooser()
        {
            driver.Window().Close(driver.Close);
        }
    }

    public class PersonChooser : BaseChooser
    {
        public PersonChooser(IWebElement launcher) : base(launcher)
        {
            ;
        }
    }

    public class OrganizationChooser : BaseChooser
    {
        public OrganizationChooser(IWebElement launcher) : base(launcher)
        {
            ;
        }
    }

    public class CommandConsole : PageController
    {
        public override void Navigate()
        {
            base.Navigate("/DebugConsole/CommandWindow/command");
        }

        public void CreateNewScript(string name)
        {
            driver.FindElement(By.Id("New")).Click();
            IWebElement nameBox = new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.Id("CommandScript.name")));
            nameBox.SendKeys(name);
            this.SaveCurrentScript();
        }

        public void SaveCurrentScript()
        {
            driver.FindElement(By.Id("Save")).Click();
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.Id("CommandScript.name")));
        }

        public void DeleteCurrentScript()
        {
            driver.FindElement(By.Id("Delete")).Click();
            driver.SwitchTo().Alert().Accept();
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.Id("CommandScript.name")));
        }

        public void RunCurrentScript()
        {
            driver.FindElement(By.Id("Run")).Click();
            new WebDriverWait(driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementExists(By.Id("CommandScript.name")));
        }

        public string RunScript(string script)
        {
            this.CreateNewScript("TEMP");
            this.CurrentScript = script;
            this.RunCurrentScript();
            String output = this.CurrentOutput;
            this.DeleteCurrentScript();
            return output;
        }

        public string CurrentScript
        {
            get
            {
                return driver.FindElement(By.Id("CommandScript.script")).Text;
            }
            set
            {
                // Use Javascript to set the script value to save time over using SendKeys with long scripts.
                ((IJavaScriptExecutor)driver).ExecuteScript(@"document.getElementById('CommandScript.script').value = arguments[0];", value);
            }
        }

        public string CurrentOutput
        {
            get
            {
                return driver.FindElement(By.Id("OutputScript")).Text;
            }
        }
    }
}