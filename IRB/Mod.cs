﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClickPortal.PortalDriver;
using System.Threading;

namespace ClickPortal.IRB
{
    public class MOD : IRBStudy
    {
        public String ParentStudy;

        public MOD() : base()
        {
            this.SubmissionType = "MOD";
            this.ID = "MOD-" + this.studyNumber;
            this.ResourceContainerTemplateID = "TMPL4DC1411ED8C00";
        }

        public override string CreateScript
        {
            get
            {
                if (ParentStudy == null)
                {
                    throw new Exception("Can not create MOD without specifying the parent ID");
                }

                string script = base.CreateScript;
                script += @"
var parentStudy = getResultSet(""_IRBSubmission"").query(""ID='" + ParentStudy + @"'"");
if (parentStudy.count() != 1) {
    throw new Error(""Incorrect number of entities found when querying parent study."");
}
parentStudy = parentStudy.elements().item(1);
etype.setQualifiedAttribute(""parentProject"", parentStudy);
etype.setQualifiedAttribute(""customAttributes.parentStudy"", parentStudy);
etype.setQualifiedAttribute(""customAttributes.draftStudy"", parentStudy.customAttributes.draftStudy);
etype.resourceContainer.parent = parentStudy.resourceContainer;";

                return script;
            }
        }

        public override void CreateEntity(PortalDriver.PortalDriver Driver)
        {
            string script = @"
var parentStudy = getResultSet(""_IRBSubmission"").query(""ID='" + ParentStudy + @"'"");
if (parentStudy.count() != 1) {
    throw new Error(""Incorrect number of entities found when querying parent study."");
}
parentStudy = parentStudy.elements().item(1);
var draftStudy = parentStudy.getQualifiedAttribute(""customAttributes.draftStudy"");
if (draftStudy) {
    ? ""True"";
}
else {
    ? ""False"";
}";
            bool found = false;
            int numChecks = 0;
            do
            {
                Thread.Sleep(1000);
                found = (Driver.Store().Execute(script) == "True");
            } while (numChecks < 30 && !found);

            if (!found)
            {
                throw new Exception("Unable to find Draft Study after 30s.");
            }

            base.CreateEntity(Driver);
        }
    }
}