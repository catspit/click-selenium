﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Internal;

namespace ClickPortal.PortalDriver
{

    internal class ComponentNotFoundException : Exception
    {
        public ComponentNotFoundException(string specifics) : base(specifics){}

        public ComponentNotFoundException() : base("An attempt was made to use a component not found in the current context.  Are you on the wrong page?") { }
    }

    public interface IPortalDriver : IWebDriver, IJavaScriptExecutor, IHasInputDevices, IActionExecutor 
    {

        IPortalControl Control(); // last control successfully found

        IPortalControl  Control(String locator);

        IPortalControl  Control(PortalBy @by);

        IPortalControl  Control(By @by);

        IPortalStore    Store(String configIdOrUrl);

        IPortalStore    Store(); //default store

        IPortalConsole  Console();  //

        IPortalConsole  Console(String configIdOrUrl);  

        IPortalProjectCreator ProjectCreator(String title);
        
        IPortalProjectCreator ProjectCreator(By @by);

        IPortalActivity Activity();

        IPortalActivity Activity(string locator);

        IPortalChooser Chooser();

        IPortalJob Job();

        IPortalJob Job(string name);

        IPortalProjectEditor ProjectEditor(String altText);

        IPortalProjectEditor ProjectEditor(By @by);

        IPortalSmartform Smartform();

        IPortalTab Tab(String tabTitle);

        IPortalTab Tab(By @by);

        IPortalWindow Window();

        String StoreName { get; } //use config or URL (possibly root folder)

        String ResourceDirectory { get; }

        String UrlForFileResource(String resourceFileName);

        void NavigateToRelativePath(String path);

        void NavigateToRelativePath(String path, Dictionary<String, String> query);

        void InFrame(By @by, Action<IPortalDriver> p);
        void WaitUntilPageLoaded();
    }

    public interface IPortalActivity : IPortalComponentExists
    {
        void Open();
        void Submit();
        void Submit(string username, string password);
        void Cancel();
        void OpenAndSubmit();
    }

    public interface IPortalActivityLog
    {
        bool HasActivity(string activityName);
    }

    public interface IPortalJob : IPortalComponentExists
    {
        void Open();
        void Preview();
    }

    public interface IPortalSmartform
    {
        void Continue();
        string Title();
        void Back();
        void Save();
        void Exit();
        void JumpTo(string sfName);
        void Finish();
    }

    public interface IPortalChooser
    {
        int ResultCount { get; }
        void Select(IEnumerable<IDictionary<string, string>> options);
        bool ContainsOptions(IEnumerable<IDictionary<string, string>> options); 
        void OK();
        void Cancel();
        void Next();
        void Filter(string filterBy, string text);
    }


    public interface IPortalControl : IPortalControlValues, IPortalComponent, ISetPortalValue, IPortalControlWidgets
    //public interface IPortalControl
    {
        string Type { get; }
        string Caption { get; }
        bool HasHelpBubble { get; }
        IWebElement Container { get; }
    }
    
    public interface IPortalProjectCreator : IPortalComponent
    {
        void Click();
    }

    public interface IPortalProjectEditor : IPortalComponent
    {
        void Click();
    }

    public interface IPortalConsole : IPortalComponentExists
    {
        Boolean Go();
        string Execute(string script);
    }

    public interface IPortalStore
    {
        void Login(); //admin
        void Login(String user); //user + config password
        void Login(String user, String password);
        void LoginAdmin();
        void Logoff();
        
        string Execute(String script);
        string Execute(string script, string user, string pass);

        void Import(); //stuff for bulk import

        string CurrentUser { get; }
        int portalVersionMajor { get; }
    }


    public interface IPortalControlValue
    {
        bool Is(IPortalControlValue value);
        bool Is(string textOrOID);
        String OID { get;}
        String Text { get; }
        bool IsClear { get; }

    }

    public interface IPortalControlValues : IPortalControlValue, IEnumerable<IPortalControlValue>
    {
        IPortalControlValue this[int index] {
            get;
        }

        int Count { get; }
    }

    public interface IPortalComponentExists
    {
        bool Is();
    }

    public interface IPortalComponent : IPortalComponentExists
    {
    }


    public interface ISetPortalValue
    {
        IPortalControlValue Clear();
        IPortalControlValue Val();
        IPortalControlValue Val(String TextOrOID);
        IPortalControlValue Val(DateTime date);
    }

    public interface IPortalControlWidgets
    {
        IWebElement AddButton { get; }
        IWebElement SelectButton { get; }
    }

    public interface IPortalTab : IPortalComponent
    {
        void Click();
        IWebElement GetElement();
        bool IsVisible();
    }

    public interface IPortalTopNavTab : IPortalComponent
    {
        void Click();
    }

    public interface IPortalProjectListing : IPortalComponent
    {
        void FilterBy(string queryText, string filterBy);
        void ClickProjectIdOrNameToWorkspace(string projectIdOrName);
    }

    public interface IPortalWindow : IPortalComponent
    {
        void New(Action act);
        void Close(Action act);
        void Close();
    }
}
