﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.IACUC.Procedure.Pages
{
    public class IACUCProcedureFoodOrFluidRestrictionForm : IACUCProcedureFormPage
    {
        public IACUCProcedureFoodOrFluidRestrictionForm(IACUCProcedure procedure) : base(procedure)
        {
            ;
        }

        public override void Navigate()
        {
            base.Navigate("/ResourceAdministration/Project/ProjectEditor?Project=" + _procedure.OID + "&Mode=smartform&WizardPageOID=com.webridge.entity.Entity[OID[35C9080DC70E0C4DB9391BF658314E66]]");
        }
    }
}
