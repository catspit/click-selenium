﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using OpenQA.Selenium;

namespace ClickPortal.PortalDriver {


    class ControlHelper {
        internal ReadOnlyCollection<object> stuff; 
        
        internal ControlHelper(ISearchContext context, IWebElement element)
        {
            var exec = (IJavaScriptExecutor) context;
            this.stuff = exec.ExecuteScript("return ControlHelpers.seleniumControl(arguments[0])", element) as ReadOnlyCollection<object> ??
                         new ReadOnlyCollection<object>(new object[]{"undefined",null, null, null, null, null,null,null,null,null,null,null,null});
        }
 

        //internal ReadOnlyCollection<string> rangeText(int index) {return toStringCollection(4);}

        internal string type         { get { return (string)stuff[0]; } }
        internal string OID          { get { return (string)stuff[1]; } }
        internal string text         { get { return (string)stuff[2]; } }
        internal string captionText  { get { return (string)stuff[3]; } }
        internal string attribute    { get { return (string)stuff[4]; } }
        internal ReadOnlyCollection<IWebElement> widgets { get { return toWebElementsCollection(5); } }
        internal ReadOnlyCollection<string> rangeText { get { return toStringCollection(6); } }
        internal ReadOnlyCollection<string> rangeOID  { get { return toStringCollection(7); } }
        internal ReadOnlyCollection<string> valText   { get { return toStringCollection(8); } }
        internal ReadOnlyCollection<string> valOID    { get { return toStringCollection(9); } }
        internal ReadOnlyCollection<IWebElement> caption   { get { return toWebElementsCollection(10); } }
        internal ReadOnlyCollection<IWebElement> container { get { return toWebElementsCollection(11); } }


        public ReadOnlyCollection<IPortalControlValue> Range
        {
            get { return toValueList(this.rangeText, this.rangeOID); }
        }

        public ReadOnlyCollection<IPortalControlValue> Values {
            get { return toValueList(this.valText, this.valOID); }
        }


        private ReadOnlyCollection<IPortalControlValue> toValueList(ReadOnlyCollection<string> text, ReadOnlyCollection<string> oid)
        {
            if(text == null && oid == null) return new ReadOnlyCollection<IPortalControlValue>(new IPortalControlValue[0]);
            if(text == null || oid == null) throw new ArgumentException("either OID or Text can be null, but not both");
            if(text.Count != oid.Count) throw new ArgumentException("the oid and text counts don't match for Val creation");

            //IPortalControlValue[] test = null;
            IPortalControlValue[] arrValues = new IPortalControlValue[text.Count];

            var i = 0;
            foreach (var pair in oid.Zip<string, string, Tuple<string, string>>(text, Tuple.Create))
            {
                var isClear = string.IsNullOrEmpty(pair.Item1) && pair.Item2.Trim() == "";
                arrValues[i++] = new PortalControlValue(pair.Item1, pair.Item2.Trim(), isClear);
            }
            return new ReadOnlyCollection<IPortalControlValue>(arrValues);
        }


        internal ReadOnlyCollection<string> toStringCollection(int index)
        {
            var possibleStrings = stuff[index] as ReadOnlyCollection<object>;
            if (possibleStrings == null) return new ReadOnlyCollection<string>(new string[0]);

            var stringList = new string[possibleStrings.Count];

            var i = 0;
            foreach (var s in possibleStrings)
            {
                stringList[i] = (string) s;
                i++;
            }
            return new ReadOnlyCollection<string>(stringList);
        }

        internal ReadOnlyCollection<IWebElement> toWebElementsCollection(int index)
        {
            var possibleElems = stuff[index] as ReadOnlyCollection<IWebElement>;
            if (possibleElems == null) return new ReadOnlyCollection<IWebElement>(new IWebElement[0]);
            return possibleElems;
        }
    }
}
