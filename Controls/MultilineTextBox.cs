﻿using ClickPortal.Pages;
using OpenQA.Selenium;

namespace ClickPortal.Controls
{
    public class MultilineTextBox : ClickControl
    {
        public MultilineTextBox(PageController currentPage, string propertyLabel) : base(currentPage, propertyLabel)
        {
        }

        public override void SetValue(string value)
        {
            var textControl = Container.FindElement(By.ClassName("textAreaControl"));
            WaitForElementToBeVisible(textControl);
            textControl.Clear();
            textControl.SendKeys(value);
        }
    }
}
