﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ClickPortal
{
    public abstract class ProjectType : ClickEType
    {
        protected abstract string ResourceContainerTemplateID { get; }
        protected abstract string ResourceContainerParent { get; }
        protected abstract string InitialSmartformView { get; }

        [ETypeProperty("ID")]
        public string ID;
        [ETypeProperty("name")]
        public string Name;
        [ETypeProperty("description")]
        public string Description;
        [ETypeProperty("company", RelatedEType = "Company", RelatedKeyName = "name")]
        public string Company;
        [ETypeProperty("status", RelatedEType = "ProjectStatus")]
        public string Status;
        [ETypeProperty("createdBy", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string creatorUserID;
        [ETypeProperty("owner", RelatedEType = "Person", RelatedKeyName = "userId")]
        public string Owner;
        [ETypeProperty("dateEnteredState")]
        public string DateEnteredState;

        public ProjectType(string internalTypeName) : base(internalTypeName)
        {
            this.ID = "PROJECT-" + this.InstanceNumber;
            this.Name = this.Description = "Test Project " + this.InstanceNumber;
            this.Company = "Test Company";
            this.creatorUserID = this.Owner = "administrator";
        }

        /**
         * Copy constructor
         */
        public ProjectType(ProjectType source) : this(source.ETypeName)
        {
            this.OID = source.OID;
            this.ID = source.ID;
            this.Name = source.Name;
            this.Description = source.Description;
            this.Company = source.Company;
            this.Status = source.Status;
            this.creatorUserID = source.creatorUserID;
            this.Owner = source.Owner;
        }

        public override string CreateScript
        {
            get
            {
                string script = @"var etype = wom.createTransientEntity(""" + this.ETypeName + @""");
etype.registerEntity();
etype.initialize();";
                string scriptThatCreatesID = "";
                List<string> optionalParams = new List<string>();

                int relatedCount = 0;
                foreach (FieldInfo field in this.GetType().GetFields())
                {
                    foreach (object attr in field.GetCustomAttributes(true))
                    {
                        if (attr is ETypeProperty && field.GetValue(this) != null)
                        {
                            if (((ETypeProperty)attr).RelatedEType != null)
                            {
                                string fieldname = ((ETypeProperty)attr).RelatedKeyName == null ? "ID" : ((ETypeProperty)attr).RelatedKeyName;
                                switch (((ETypeProperty)attr).PropertyName)
                                {
                                    case "owner":
                                        script += "\nvar entOwnerPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""");
if (entOwnerPerson == null || entOwnerPerson.count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }
entOwnerPerson = entOwnerPerson.elements()(1);";
                                        break;
                                    case "company":
                                        script += "\nvar entCompany = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""");
if (entCompany == null || entCompany.count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }
entCompany = entCompany.elements()(1);";
                                        break;
                                    case "createdBy":
                                        script += "\nvar entCreatedByPerson = getResultSet(\"" + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                            + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""");
if (entCreatedByPerson == null || entCreatedByPerson.count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }
entCreatedByPerson = entCreatedByPerson.elements()(1);";
                                        break;
                                    default:
                                        optionalParams.Add("\nvar related" + ++relatedCount + " = getResultSet(\""
                                    + ((ETypeProperty)attr).RelatedEType + "\").query(\""
                                    + fieldname + "=" + JsonConvert.ToString(((string)field.GetValue(this)), '\'')
                                    + @""");
if (related" + relatedCount + @".count() != 1) { throw new Error(""Found incorrect number of related entities for field '" + field.Name
                                    + @"'.""); }" + "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + "related" + relatedCount + ".elements()(1)" + ");");
                                        break;
                                }
                            }
                            else
                            {
                                if (((ETypeProperty)attr).PropertyName == "ID")
                                {
                                    scriptThatCreatesID = "\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");";
                                }
                                else
                                {
                                    optionalParams.Add("\netype.setQualifiedAttribute(\"" + ((ETypeProperty)attr).PropertyName + "\", " + JsonConvert.SerializeObject(field.GetValue(this)) + ");");
                                }
                            }
                        }
                    }
                }

                script += "\netype.setRequiredFields(entOwnerPerson, entCompany, entCreatedByPerson);";
                foreach (string snip in optionalParams)
                {
                    script += snip;
                }
                script += @"
var rc = ResourceContainer.createFromResource(etype, getElements(""ContainerTemplateForID"", ""ID"", """ + ResourceContainerTemplateID + @""")(1));
rc.setQualifiedAttribute(""parent"", getElements(""ContainerBaseForID"", ""ID"", """ + ResourceContainerParent + @""")(1));
etype.setQualifiedAttribute(""resourceContainer"", rc);
var sch = ShadowSCH.getSCH();
var view = wom.getEntityFromString(""" + InitialSmartformView + @""");
wom.putContext(""currentView"", view, true);
//ProjectStateTransition.processProject(sch, etype, etype, null);
" + scriptThatCreatesID + @"
?etype;";
                return script;
            }
        }
    }
}
